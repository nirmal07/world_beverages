<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


/* Web */
Route::get('/', 'HomeController@index')->name('home.index');
Route::get('/search', 'HomeController@search')->name('home.search');


/* Front */ 
Route::namespace('Front')->group(function () {
    Route::get('front/login','LoginController@login')->name('front.login');
    Route::post('front/logindata','LoginController@logindata')->name('front.logindata');
    Route::get('front/logout','LoginController@logout')->name('front.logout');
    Route::get('front/profile','ProfileController@create')->name('front.profile');
    Route::get('front/create','RegisterController@create')->name('front.create');
    Route::post('front/store','RegisterController@store')->name('front.store');
	Route::get('/digital-directory', 'DigitalDirectoryController@index')->name('front.digitaldirectory');
	Route::get('/product/{id}', 'ProductController@index')->name('front.product_details');
    Route::get('/wine-importers-in-usa', 'DigitalDirectoryController@WineUsa')->name('front.wine_imports_usa');
    Route::get('/product-description/{id}','ProductController@view')->name('front.product_description');
    Route::get('/news-details/{id}','ProductController@newsDetail')->name('front.news_details');
    Route::get('/event-details/{id}','ProductController@eventDetail')->name('front.event_details');
    Route::get('/wine-importer-detail/{id}','DigitalDirectoryController@wineDetail')->name('front.wine_importer_details');
});
/* Buyer */

/* Seller */

/* Admin */
Route::namespace('Admin')->group(function () {
    Route::get('admin/login', 'LoginController@showLoginForm')->name('admin.login');
    Route::post('admin/login', 'LoginController@login')->name('admin.login');
    Route::get('admin/logout', 'LoginController@logout')->name('admin.logout');
});
Route::group(['prefix' => 'admin', 'middleware' => ['employee'], 'as' => 'admin.' ], function () {
    Route::namespace('Admin')->group(function () {
		Route::get('dashboard', 'DashboardController@index')->name('dashboard');
		Route::namespace('Customers')->group(function () {
        	Route::resource('customers', 'CustomerController');
            Route::post('is_active/{id}','CustomerController@isActive')->name('customers.is_active');
       	});
		Route::namespace('Products')->group(function () {
            Route::resource('products', 'ProductController');
            Route::get('remove-image-thumb', 'ProductController@removeThumbnail')->name('product.remove.thumb');
        });
        Route::namespace('Categories')->group(function () {
            Route::resource('categories', 'CategoryController');
            Route::get('categories/subcat/{id}','CategoryController@getSubcategory')->name('getsubcat');
        });
        Route::namespace('News')->group(function () {
            Route::get('news','NewsController@create')->name('news.create');
            Route::post('news-store','NewsController@store')->name('news.store');
            Route::get('news-list','NewsController@index')->name('news.list');
            Route::get('news/delete/{id}','NewsController@destroy')->name('news.destroy'); 
            Route::get('news/edit/{id}','NewsController@edit')->name('news.edit');
            Route::post('news/status/{id}', 'NewsController@status')->name('news.status');
        });
        Route::namespace('Events')->group(function () {
            Route::get('events','EventsController@create')->name('events.create');
            Route::post('events-store','EventsController@store')->name('events.store');
            Route::get('events-list','EventsController@index')->name('events.list');
            Route::get('events/delete/{id}','EventsController@destroy')->name('events.destroy'); 
            Route::get('events/edit/{id}','EventsController@edit')->name('events.edit');
            Route::post('events/status/{id}', 'EventsController@status')->name('events.status');
        });
        Route::namespace('DigitalDirectory')->group(function () {
            Route::get('digitaldirectory','DigitalDirectoryController@create')->name('digitaldirectory.create');
            Route::post('digitaldirectory-store','DigitalDirectoryController@store')->name('digitaldirectory.store');
            Route::get('digitaldirectory-list','DigitalDirectoryController@index')->name('digitaldirectory.list');
            Route::get('digitaldirectory/delete/{id}','DigitalDirectoryController@destroy')->name('digitaldirectory.destroy'); 
            Route::get('digitaldirectory/edit/{id}','DigitalDirectoryController@edit')->name('digitaldirectory.edit');
            Route::post('digitaldirectory/status/{id}', 'DigitalDirectoryController@status')->name('digitaldirectory.status');
            Route::get('digitaldirectory/subcat/{id}','DigitalDirectoryController@getSubdigicategory')->name('getsubcatdigital');
            // None Listed Consultan
            Route::get('digitaldirectoryconsultant/list','ConsultantController@index')->name('digitaldirectory.none_listed_consultant');
            Route::get('digitaldirectoryconsultant/create','ConsultantController@create')->name('digitaldirectory.none_listed_consultant_create');
            Route::post('digitaldirectoryconsultant-store','ConsultantController@store')->name('digitaldirectory.none_listed_consultant_store');
            Route::get('digitaldirectoryconsultant/delete/{id}','ConsultantController@destroy')->name('digitaldirectory.none_listed_consultant_destroy'); 
            Route::get('digitaldirectoryconsultant/edit/{id}','ConsultantController@edit')->name('digitaldirectory.none_listed_consultant_edit');
            Route::post('digitaldirectoryconsultant/status/{id}', 'ConsultantController@status')->name('digitaldirectory.none_listed_consultant_status');
        });
        Route::namespace('Academys')->group(function(){
            // Author
            Route::get('author/list','AuthorController@index')->name('academys.list_author');
            Route::get('author/create','AuthorController@create')->name('academys.create_author');
            Route::get('author/edit/{id}','AuthorController@edit')->name('academys.edit_author');
            Route::get('author/delete/{id}','AuthorController@destroy')->name('academys.destroy_author');
            Route::post('author/store','AuthorController@store')->name('academys.store_author');
            //Route::post('academystore','AcademyController@create');

            //Contents
            Route::get('content/list','ContentController@index')->name('academys.list_content');
            Route::get('content/create','ContentController@create')->name('academys.create_content');
            Route::get('content/delete/{id}','ContentController@destroy')->name('academys.destroy_content');
            Route::get('content/edit/{id}','ContentController@edit')->name('academys.edit_content');
            Route::post('content/store','ContentController@store')->name('academys.store_content');

            //Webinar
            Route::get('webinar/list','WebinarController@index')->name('academys.list_webinar');
            Route::get('webinar/create','WebinarController@create')->name('academys.create_webinar');
            Route::get('webinar/edit/{id}','WebinarController@edit')->name('academys.edit_webinar');
            Route::get('webinar/delete/{id}','WebinarController@destroy')->name('academys.destroy_webinar');
            Route::post('webinar/store','WebinarController@store')->name('academys.store_webinar');

            //Articles
            Route::get('article/list','ArticleController@index')->name('academys.list_article');
            Route::get('article/create','ArticleController@create')->name('academys.create_article');
             Route::get('article/edit/{id}','ArticleController@edit')->name('academys.edit_article');
            Route::get('article/delete/{id}','ArticleController@destroy')->name('academys.destroy_article');
            Route::post('article/store','ArticleController@store')->name('academys.store_article');
        });
	});
});

// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
