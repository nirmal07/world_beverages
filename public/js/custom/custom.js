
$(document).ready(function(){
	/* variable */
	
	/* function */
	$(".number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $('#image').change(function(){
        readURL(this);
    });
    
    $('.select2').select2({
        tags: true
    });
});
CKEDITOR.replace( 'content1' );
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#noFile').empty();
            $('#edit_image').remove();
            $('#image_beside').empty();
            $("#image_beside").append('<div class="row">'+
                '<img src ='+e.target.result+' width="250px" height="240px" >'+
                    '</div>'+
            '</div>');
        }
        reader.readAsDataURL(input.files[0]);
    }
}