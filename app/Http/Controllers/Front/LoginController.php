<?php
namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use Auth;
use Log;

class LoginController extends Controller
{

	public function login()
	{
		try{
			return view('auth.front.login');
		}
		catch(Exception $e){
			log::error($e);
		}
	}
	public function logindata(Request $request)
	{
		try{
			 $rules = [
			 	'username' => 'required|max:50',
			 	'password' => 'required|max:15'
			 ];
			 $customMessage = [
			 	'username.required' => 'Please enter username',
			 	'password.required' => 'Please enter password'
			 ];
			 // $valid = Validator::make($request->all(),$rules,$customMessage);
			 /*if($valid->fails())
			 {
			 	return back()->withInput()->withErrors($valid->errors());
			 }*/
			if($request->username != "" && $request->password != ""){
				if(Auth::guard('customer')->attempt(['username'=>$request->username,'password'=>$request->password])){
                	return redirect('/');
            	} else {
                	return redirect('front/login')->with('error','Your email and password are wrong');
            	}
            	return back()->with('error', 'Something wrong please try again latter');
			} else{
				return back()->with('error', 'Please enter Username and Password.');
			}

        } catch (Exception $e) {
            log::error($e);
        } 
	}
	public function logout(Request $request)
	{
		
		Auth::guard('customer')->logout();
		return redirect('/');
	}
}
?>