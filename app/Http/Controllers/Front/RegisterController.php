<?php
namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Front\Customer;
use App\Helpers\MailHelper;
use Hash;
use Log;
use Validator;
use Mail;

class RegisterController extends Controller
{	
	public function create()
	{
		return view('auth.admin.register');
	}
	public function store(Request $request)
	{
		 try{
            $rules = [
                'username'    => 'required|max:50|unique:customers,username',
                'password' => 'required|max:15|min:6|max:15',
                'name'    => 'required|max:255',
                'surname' => 'required|max:255',
                'phone'=> 'required|numeric|digits_between:7,15',
                'email'=> 'required|email|max:50|regex:/(.*)@gmail\.com/i|unique:customers,email',
            ];
            $customeMessage = [
                'username.required'    => 'Please enter user name.',
                'username.unique' => 'Username has already been taken',
                'name.required'    => 'Please enter name.',
                'surname.required' => 'Please enter surname.',
                'email.required'   => 'Please enter email.',
                'email.email' => 'Please enter valid email address',
                'email.unique' =>'Email address already in use',
                'password.required'   => 'Please enter password.',
                'password.min'   => 'Please enter minimum 6 character or number.',
                'password.max'   => 'Please enter maximum 15 character or number.',
                'password.digits_between'   => 'Please enter number between 0 to 15.',
                'password.required'   => 'Please enter password.',
                'phone.required'   => 'Please enter phone number.',
                'phone.number' =>'Please enter number',
            ];
            $Validator = Validator::make($request->all(),$rules,$customeMessage);
            if ($Validator->fails()) {
                return back()->withInput()->withErrors($Validator->errors());
            } else {
                $id = trim(strip_tags($request->id));
                $username = trim(strip_tags($request->username));
                $password = trim(strip_tags($request->password));
                $name = trim(strip_tags($request->name));
                $surname = trim(strip_tags($request->surname));
                $phone = trim(strip_tags($request->phone));
                $email = trim(strip_tags($request->email));
                if($request->id!=""){
                    $customer=Customer::find($request->id);
                }else{
                    $customer=New Customer;
                }
                $customer->username = $username;
                $customer->name = $name;
                $password_hash = Hash::make($password);
                $customer->password = $password_hash;
                $customer->surname = $surname;
                $customer->phone = $phone;
                $customer->email = $email;
                // dd($customer);
                if($customer->save()) {
            		// MailHelper::sendEmail($customer);
                    return redirect()->route('front.login')->with('message','Registartion Successfully');
	            } else{
	            	return redirect()->route('front.register')->with('message','Registartion Not Successfully');
	            }
                return back()->with('error', 'Something wrong please try again latter');
            }
        }
        catch(Exception $e){
            log::error($e);
        }
	}
}
?>