<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Country;
use App\Model\Front\Digitaldirectory;	
use App\Model\Front\Companytype;	
use App\Model\Front\Categories;
use DB;
use Carbon\Carbon;

class DigitalDirectoryController extends Controller
{
	public function index(Request $request){
		try {
			$directory_limit = 3;
			$page = trim($request->page);
			$directorys = DB::table('digital_directory')
							->select('digital_directory.*','categories.name as company_types','countries.country as countries_name')
							->leftjoin('categories','categories.id','digital_directory.company_type')
							->leftjoin('countries','countries.id','digital_directory.country')
							->where('categories.parent_id',35);
			if (count($request->all()) != 0) {
                if (isset($request->company_type) && !empty($request->company_type))
                {
                    $company_type = strip_tags(trim($request->company_type));
                    $directorys->where(function ($query) use ($company_type) {
                        $query->where('digital_directory.company_type', 'LIKE', '%' . $company_type . '%');
                    });
                }
                if (isset($request->country_name) && !empty($request->country_name))
                {
                    $country_name = strip_tags(trim($request->country_name));
                    $directorys->where(function ($query) use ($country_name) {
                        $query->where('digital_directory.country', 'LIKE', '%' . $country_name . '%');
                    });
                }

			}
			if($page!=""){
                $data['directorys'] = $directorys->paginate($directory_limit,['*'],'page',$page);
             }else{
                $data['directorys'] = $directorys->paginate($directory_limit);
            }
			$data['company_types'] = Categories::where('parent_id',35)->get();
			$data['countrys'] = Country::get();
			return view('web.digital_directory')->with($data);
		} catch (Exception $e) {
			log::error($e);
		}
	}
	public function WineUsa(Request $request){
		try {
			$data['directorys_usa'] = DB::table('digital_directory')
							->select('digital_directory.*','categories.name as company_types','countries.country as countries_name')
							->leftjoin('categories','categories.id','digital_directory.company_type')
							->leftjoin('countries','countries.id','digital_directory.country')
							->where('categories.parent_id',35)
							->where('digital_directory.non_listed_consultant','Yes')
							->paginate(3);
			return view('web.wine_importer_usa')->with($data);
		} catch (Exception $e) {
			log::error($e);
		}
	}
	public function wineDetail($id)
	{
		try{
			$data['wine_usa'] = Digitaldirectory::where('id',$id)->get();
			$data['company'] = Companytype::where('id',$id)->get();
			$data['countries'] = Country::where('id',$id)->get();
			return view('web.wine_importer_details')->with($data);
		} catch(Exception $e) {
			log::error($e);
		}
	}
	
}
