<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Front\Products;
use App\Model\Front\News;
use App\Model\Front\Events;
use DB;

class ProductController extends Controller
{
    public function index($id){
    	try {
    		$data['product'] = Products::where('id',$id)->first();
    		return view('web.product_detail')->with($data);
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
    public function view($id)
    {
    	try{
    		$data['details'] = Products::where('id',$id)->get();
    		return view('web.product_description')->with($data);
    	}
    	catch(Exception $e){
            log::error($e);
    	}
    }
    public function newsDetail($id)
    {
        try {
            $data['news'] = News::where('id',$id)->get();
            return view('web.news_description')->with($data);
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function eventDetail($id)
    {
        try {
            $data['events'] = Events::where('id',$id)->get();
            // dd($data);
            return view('web.event_description')->with($data);
            
        } catch (Exception $e) {
            log::error($e);
        }
    }
}
