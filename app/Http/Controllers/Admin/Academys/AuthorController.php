<?php

namespace App\Http\Controllers\Admin\Academys;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Front\Authors;
use App\Model\Front\Seo;
use Log;
use Validator;

class AuthorController extends Controller
{
    public function index()
	{
		try {
			$data['authors'] = Authors::get();
			return view('admin.academy.list_author')->with($data);
		} catch (Exception $e) {
			log::error($e);
		}
	}
	public function create()
	{
		try {
			return view('admin.academy.create_author');
		} catch (Exception $e) {
			log::error($e);	
		}
	}
	public function store(Request $request){
		try {
			$rules = [
				'name' => 'required',
				'surname' => 'required',
				'author_website_url' => 'required',
				'twitter' => 'required',
				'facebook' => 'required',
				'linkdin' => 'required',
				'author_blog' => 'required',
				'email' => 'required|email|max:50|regex:/(.*)@gmail\.com/i|unique:author,email',
				'place' => 'required',
				'author_tagline_english' => 'required',
				'full_bio_english' => 'required',
				'seo_title' => 'required',
				'seo_page_description' => 'required',
				'author_photo' => 'required'
			];
			$customessage = [
				'name.required' => 'Please enter name',
				'surname.required' => 'Please enter surname',
				'author_website_url.required' => 'Please enter author website url',
				'twitter.required' => 'Please enter twitter',
				'twitter.twitter' => 'Please enter valid twitter',
				'facebook.required' => 'Please enter facebook',
				'linkdin.required' => 'Please enter linkdin',
				'author_blog.required' => 'Please enter author blog',
				'email.required' => 'Please enter email',
				'email.email' => 'Please enter valid email address',
                'email.unique' =>'Email address already in use',
				'place.required' => 'Please enter place',
				'author_tagline_english.required' => 'Please enter tagline english',
				'full_bio_english.required' => 'Please enter bio english',
				'seo_title.required' => 'Please enter seo_title',
				'seo_page_description.required' => 'Please enter seo page description',
				'author_photo.required' => 'Please select photo',
			];
			$valid = Validator::make($request->all(),$rules,$customessage);
			if($valid->fails()){
				return back()->withInput()->withErrors($valid->errors());
			} else{
				$id = trim(strip_tags($request->id));
				$name = trim(strip_tags($request->name));
				$surname = trim(strip_tags($request->surname));
				$author_website_url = trim(strip_tags($request->author_website_url));
				$twitter = trim(strip_tags($request->twitter));
				$facebook = trim(strip_tags($request->facebook));
				$linkdin = trim(strip_tags($request->linkdin));
				$author_blog = trim(strip_tags($request->author_blog));
				$email = trim(strip_tags($request->email));
				$place = trim(strip_tags($request->place));
				$author_tagline_english = trim(strip_tags($request->author_tagline_english));
				$full_bio_english = trim(strip_tags($request->full_bio_english));
				$seo_title = trim(strip_tags($request->seo_title));
                $seo_page_description = trim(strip_tags($request->seo_page_description));

				if($id!=""){
					$author = Authors::find($id);
				} else{
					$author = New Authors();
				}
				if (isset($request->seo_id) && $request->seo_id!=0) {
                    $seo = Seo::find($request->seo_id);
                } else{
                    $seo = New Seo;
                }
				/* SEO */
				$seo->page_title = $seo_title;
                $seo->page_description = $seo_page_description;
                if (isset($request->seo_page_keywords)) {
                    $seo_page_keyword = implode(',', $request->seo_page_keywords);
                    $seo->page_keywords = $seo_page_keyword;
                }
                $seo->save();
				/* Author */
				if (isset($request->record_tags)) {
                    $record_tags = implode(',', $request->record_tags);
                    $author->record_tags = $record_tags;
                }
				$author->name = $name;
				$author->surname = $surname;
				$author->author_website_url = $author_website_url;
				$author->twitter = $twitter;
				$author->facebook = $facebook;
				$author->linkdin = $linkdin;
				$author->author_blog = $author_blog;
				$author->email = $email;
				$author->place = $place;
				$author->author_tagline_english = $author_tagline_english;
				$author->full_bio_english = $full_bio_english;
				$author->seo_id = $seo->id;
				if($author->save()){
					if($request->has('author_photo')) {
						$time      = md5(time());
                        $file      = $request->author_photo;
                        $extension = $file->getClientOriginalExtension();
                        $logo   = $time . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('img/author/logo/'), $logo);
                        $image = 'img/author/logo/' . $logo;
                        $image_update          = Authors::find($author->id);
                        $image_update->author_photo = $image;
                        $image_update->save();
					}
				}
			}
			if($request->id!=""){
				return redirect()->route('admin.academys.list_author')->with('message','Author updated succesfully');
			}
			else
			{
				return redirect()->route('admin.academys.list_author')->with('message','Author created succesfully');
			}
		} 
		catch (Exception $e) {
			Log::error($e);
		}
	}
	public function edit($id)
	{
		try{
			if(Authors::where('id',$id)->exists()){
				$data['author'] = Authors::where('id',$id)->first();
				$data['seo'] = Seo::where('id',$data['author']->seo_id)->first();
                return view('admin.academy.create_author')->with($data);    
			}
		}
		catch(Exception $e){
			Log::error($e);
		}
	}
	public function destroy($id)
	{
		try{
			if (Authors::where('id',$id)->exists()) {
                $digitaldirectory = Authors::where('id',$id)->delete();
                return redirect()->route('admin.academys.list_author')->with('message', 'Author delete successfully.');
            }
		}
		catch(Exception $e){
			Log::error($e);
		}
	}
}
