<?php
namespace App\Http\Controllers\Admin\Academys;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Front\Contents;
use App\Model\Front\Authors;
use App\Model\Front\Seo;
use Validator;
use Log;

class ContentController extends Controller
{
	public function index()
	{
		try{
			$data['contents'] = Contents::get();
			return view('admin.academy.list_content')->with($data);
		}
		catch(Exception $e){
			log::error($e);
		}
	}
	public function create()
	{
		try{
			$data['authors'] = Authors::get();
			return view('admin.academy.create_content')->with($data);
		}
		catch(Exception $e){
			log::error($e);
		}
	}
	public function store(Request $request)
	{
		try{
			$rules = [
				'content_type' => 'required',
				'document_type' => 'required',
				'modreator' => 'required',
				'section_tags' => 'required',
				'tags_english' => 'required',
				'webinars_youtube_video' => 'required',
			];
			$custommessage = [
				'content_type.required' => 'Please content type',
				'document_type.required' => 'Please document type',
				'modreator.required' => 'Please moderator',
				'section_tags.required' => 'Please select section tags',
				'tags_english.required' => 'Please select tags english',
				'webinars_youtube_video.required' => 'Please enter webinar youtube video',
			];
			$valid = Validator::make($request->all(),$rules,$custommessage);
			if($valid->fails()){
				return back()->withInput()->withErrors($valid->errors());
			} else{
				$id = trim(strip_tags($request->id));
				$content_type = trim(strip_tags($request->content_type));
				$document_type = trim(strip_tags($request->document_type));
				$author_one = trim(strip_tags($request->author_one));
				$author_two = trim(strip_tags($request->author_two));
				$modreator = trim(strip_tags($request->modreator));
				$publication_date = trim(strip_tags($request->publication_date));
				$article_availability = trim(strip_tags($request->article_availability));
				$layout_photo = trim(strip_tags($request->layout_photo));
				$related_content_one = trim(strip_tags($request->related_content_one));
				$related_content_two = trim(strip_tags($request->related_content_two));
				$related_content_three = trim(strip_tags($request->related_content_three));
				$related_content_four = trim(strip_tags($request->related_content_four));
				$webinar_status = trim(strip_tags($request->webinar_status));
				$broadcast_day = trim(strip_tags($request->broadcast_day));
				$broadcast_month = trim(strip_tags($request->broadcast_month));
				$broadcast_year = trim(strip_tags($request->broadcast_year));
				$broadcast_hour = trim(strip_tags($request->broadcast_hour));
				$broadcast_minute = trim(strip_tags($request->broadcast_minute));
				$webinars_registration_link = trim(strip_tags($request->webinars_registration_link));
				$webinars_youtube_video = trim(strip_tags($request->webinars_youtube_video));
				$webinar_slideshare_embed_code = trim(strip_tags($request->webinar_slideshare_embed_code));
				$title_english = trim(strip_tags($request->title_english));
				$sub_title_english = trim(strip_tags($request->sub_title_english));
				$teaser_english = trim(strip_tags($request->teaser_english));
				$body_english = trim(strip_tags($request->body_english));
				$text_box_english = trim(strip_tags($request->text_box_english));
				$audio_description_english = trim(strip_tags($request->audio_description_english));
				$video_index_english = trim(strip_tags($request->video_index_english));

				$seo_title = trim(strip_tags($request->seo_title));
                $seo_page_description = trim(strip_tags($request->seo_page_description));

				if($id!=""){
					$content = Contents::find($id);
				} else{
					$content = New Contents();
				}
				if (isset($request->seo_id) && $request->seo_id!=0) {
                    $seo = Seo::find($request->seo_id);
                } else{
                    $seo = New Seo;
                }
				
				$seo->page_title = $seo_title;
                $seo->page_description = $seo_page_description;
                if (isset($request->seo_page_keywords)) {
                    $seo_page_keyword = implode(',', $request->seo_page_keywords);
                    $seo->page_keywords = $seo_page_keyword;
                }
                $seo->save();

                
                $content->content_type = $content_type;
				$content->document_type = $document_type;
				$content->author_one = $author_one;
				$content->author_two = $author_two;
				$content->modreator = $modreator;
				$content->publication_date = $publication_date;
				if (isset($request->section_tags)) {
                    $section_tags = implode(',', $request->section_tags);
                    $content->section_tags = $section_tags;
                }
                if (isset($request->tags_english)) {
                    $tags_english = implode(',', $request->tags_english);
                    $content->tags_english = $tags_english;
                }
				if (isset($request->category_association)) {
                    $category_association = implode(',', $request->category_association);
                    $content->category_association = $category_association;
                }
                if (isset($request->expertise_association)) {
                    $expertise_association = implode(',', $request->expertise_association);
                    $content->expertise_association = $expertise_association;
                }
                if (isset($request->btn_use_association)) {
                    $btn_use_association = implode(',', $request->btn_use_association);
                    $content->btn_use_association = $btn_use_association;
                }
				$content->article_availability = $article_availability;
				$content->layout_photo = $layout_photo;
				$content->related_content_one = $related_content_one;
				$content->related_content_two = $related_content_two;
				$content->related_content_three = $related_content_three;
				$content->related_content_four = $related_content_four;
				$content->webinar_status = $webinar_status;
				$content->broadcast_day = $broadcast_day;
				$content->broadcast_month = $broadcast_month;
				$content->broadcast_year = $broadcast_year;
				$content->broadcast_hour = $broadcast_hour;
				$content->broadcast_minute = $broadcast_minute;
				$content->webinars_registration_link = $webinars_registration_link;
				$content->webinars_youtube_video = $webinars_youtube_video;
				$content->webinar_slideshare_embed_code = $webinar_slideshare_embed_code;
				$content->title_english = $title_english;
				$content->sub_title_english = $sub_title_english;
				$content->teaser_english = $teaser_english;
				$content->body_english = $body_english;
				$content->text_box_english = $text_box_english;
				$content->audio_description_english = $audio_description_english;
				$content->video_index_english = $video_index_english;
				$content->created_at = date('Y-m-d',strtotime($publication_date));

				$content->seo_id = $seo->id;
				if($content->save()){
					if($request->has('main_image')) {
						$time      = md5(time());
                        $file      = $request->main_image;
                        $extension = $file->getClientOriginalExtension();
                        $logo   = $time . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('img/content/image/'), $logo);
                        $image = 'img/content/image/' . $logo;
                        $image_update          = Contents::find($content->id);
                        $image_update->main_image = $image;
                        $image_update->save();
					}
					if($request->has('webinar_poster')) {
						$time      = md5(time());
                        $file      = $request->webinar_poster;
                        $extension = $file->getClientOriginalExtension();
                        $logo   = $time . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('img/content/poster/'), $logo);
                        $image = 'img/content/poster/' . $logo;
                        $image_update          = Contents::find($content->id);
                        $image_update->webinar_poster = $image;
                        $image_update->save();
					}
					if($request->has('audio_file')) {
						$time      = md5(time());
                        $file      = $request->audio_file;
                        $extension = $file->getClientOriginalExtension();
                        $logo   = $time . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('img/content/audio/'), $logo);
                        $image = 'img/content/audio/' . $logo;
                        $image_update          = Contents::find($content->id);
                        $image_update->audio_file = $image;
                        $image_update->save();
					}
				}
			}
			if($request->id!=""){
				return redirect()->route('admin.academys.list_content')->with('message','Content updated succesfully');	
			}
			else
			{
				return redirect()->route('admin.academys.list_content')->with('message','Content created succesfully');
				/*return redirect()->route('admin.academys.list_author')->with('message','Author created succesfully');*/
			}
		}
		catch(Exception $e){
			log::error($e);
		}
	}
	public function edit($id)
	{
		try{
			if(Contents::where('id',$id)->exists()){
				$data['content'] = Contents::where('id',$id)->first();
				$data['authors'] = Authors::get();
				$data['seo'] = Seo::where('id',$data['content']->seo_id)->first();
                return view('admin.academy.create_content')->with($data);    
			}
		}
		catch(Exception $e){
			log::error($e);
		}
	}
	public function destroy($id)
	{
		try{
			if (Contents::where('id',$id)->exists()) {
                $digitaldirectory = Contents::where('id',$id)->delete();
                return redirect()->route('admin.academys.list_content')->with('message', 'Content delete successfully.');
            }
		}
		catch(Exception $e){
			log::error($e);
		}
	}
}
