<?php

namespace App\Http\Controllers\Admin\Customers;

use App\Model\Front\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Helpers\MailHelper;
use App\Model\Country;
use Auth;
use redirect;
use Validator;
use Hash;

class CustomerController extends Controller
{
    public $rules = [
        'username'    => 'required|max:50',
        'password' => 'max:15|min:6|max:15',
        'name'    => 'required|max:255',
        'surname' => 'required|max:255',
        'phone'=> 'required|numeric|digits_between:0,15',
        'email'=> 'required|email|max:50',
        'country' => 'required'
    ];

    public $customeMessage = [
        'username.required'    => 'Please enter User name.',
        'name.required'    => 'Please enter Name.',
        'surname.required' => 'Please enter Surname.',
        'email.required'   => 'Please enter Email.',
        'email.email' => 'Please enter valid Email address',
        'email.unique' =>'Email address already in use',
        'password.required'   => 'Please enter Password.',
        'password.min'   => 'Please enter minimum 6 character or number.',
        'password.max'   => 'Please enter maximum 15 character or number.',
        'password.digits_between'   => 'Please enter number between 0 to 15.',
        'password.required'   => 'Please enter Password.',
        'phone.required'   => 'Please enter Phone number.',
        'phone.number' =>'Please enter number',
        'country.required'   => 'Please enter Country.'
    ];
    public function index(Request $request)
    {
        try{
            $user = Auth::guard('employee')->user();
            $customer_limit = 10;
            $page=trim($request->page);
            $data['customers'] = DB::table('customers')
                    ->select('customers.*','countries.country as country_name')
                    ->leftjoin('countries','countries.id','customers.country')
                    ->OrderBy('customers.id','desc')
                    ->where('customers.deleted_at','=',null)
                    ->paginate($customer_limit);
            return view('admin.customers.list')->with($data);
        }
        catch(Exception $e){
            Log::error($e); 
        }
    }
    public function create()
    {
        try{
            $data['countrys'] = Country::get();
            return view('admin.customers.create')->with($data);
        }
        catch(Exception $e){
            Log::error($e);  
        }
    }
    public function store(Request $request){
        try {
            if (isset($request->id) && !empty($request->id)) {
                if (Customer::where('id', $request->id)->first()->email != $request->email) {
                    $this->rules['email'] = 'required|email|unique:customers,email';
                }
                if (Customer::where('id', $request->id)->first()->username != $request->username) {
                    $this->rules['username'] = 'required|unique:customers,username';
                }
            } else {
                $this->rules['email']      = 'required|email|unique:customers,email';
                $this->rules['username']      = 'required|unique:customers,username';
                $this->rules['password'] = 'required|min:6|max:15';
            }
            $Validator = Validator::make($request->all(), $this->rules, $this->customeMessage);
            if ($Validator->fails()) {
                return back()->withInput()->withErrors($Validator->errors());
            } else {
                $id = trim(strip_tags($request->id));
                $username = trim(strip_tags($request->username));
                $password = trim(strip_tags($request->password));
                $name = trim(strip_tags($request->name));
                $surname = trim(strip_tags($request->surname));
                $country = trim(strip_tags($request->country));
                $phone = trim(strip_tags($request->phone));
                $email = trim(strip_tags($request->email));
                $comments = trim(strip_tags($request->comments));
                $status = trim(strip_tags($request->status));
                if($request->id!=""){
                    $customer=Customer::find($request->id);
                }else{
                    $customer=New Customer;
                }
                if ($request->id!="") {
                    $new_password = trim(strip_tags($request->new_password));
                    $customer->password = Hash::make($new_password);
                }
                $customer->username = $username;
                $customer->name = $name;
                $password_hash = Hash::make($password);
                $customer->password = $password_hash;
                $customer->surname = $surname;
                $customer->country = $country;
                $customer->phone = $phone;
                $customer->email = $email;
                $customer->comments = $comments;
               
                $customer->status = $status;
                $customer->save();
                if ($id!="") {
                    return redirect()->route('admin.customers.index')->with('message', 'User Succesfully updated');
                } else {
                    return redirect()->route('admin.customers.index')->with('message', 'User Succesfully Added');
                }
                return back()->with('error', 'Something wrong please try again latter');

            }
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function destroy($id){
        try{
            if (Customer::where('id',$id)->exists()) {
                Customer::where('id',$id)->delete();
            }
            return redirect()->route('admin.customers.index')->with('message', 'User delete successfully.');
        }
        catch(Exception $e){
            Log::error($e); 
        }
    }
    public function edit($id){
        try {
            $data['customer'] = Customer::where('id',$id)->first();
            $data['countrys'] = Country::get();
            return view('admin.customers.create')->with($data);
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function check_email(Request $request) {
        $email = Input::post('email');
        $id = Input::post('id');
        if ($email != '') {
            if (!empty($id) && is_numeric($id)) {
                $edit_user =  DB::table(TBL_CUSTOMERS)->where('id', '=', $id)->first();
                if ($email == $edit_user->email) {
                    return 'true';
                }
            }
            $users = DB::table(TBL_CUSTOMERS)->where('email', '=', $email)->first();
            if ($users) {
                return 'false';
            } else {
                return 'true';
            }
        } else {
            return 'false';
        }
    }
    public function isActive($id){
        try{
            $customer = Customer::where('id',$id)->first();
            if ($customer) {
                if ($customer->status== 1) {
                    $customer->status = "0";
                    MailHelper::statusActive($customer);
                } else{
                    $customer->status = "1";
                    MailHelper::statusInActive($customer);
                }
                $customer->update();
                return back()->with('message', 'Status updated successfully');
            }
        }
        catch(Exception $e){
           Log::error($e); 
        }
    }
    
}
