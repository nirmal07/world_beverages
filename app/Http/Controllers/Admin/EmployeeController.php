<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Shop\Roles\Role;
use App\Shop\Employees\Employee;
use Illuminate\Http\Request;
use Log;
use Validator;
use Carbon\Carbon;
use App\Helpers\MailHelper;
use App\Shop\Admins\Requests\UpdateEmployeeRequest;
use App\Shop\Roles\RoleUser;
use Auth;

class EmployeeController extends Controller
{
    public $rules = [
        'first_name'    => 'required',
        'last_name' => 'required',
        'role' => 'required',
    ];

    public $customeMessage = [
        'first_name.required'    => 'Please enter First name.',
        'last_name.required' => 'Please enter Last name.',
        'email.required'   => 'Please enter Email.',
        'email.email' => 'Please enter valid Email address',
        'email.unique' =>'Email address already in use',
        'password.required'   => 'Please enter Password.',
        'password.min'   => 'Please enter minimum 6 character or number.',
        'role.required'   => 'Please select Role.',
    ];
    public function index()
    {
        try{
            $user = Auth::guard('employee')->user();
            if ($user->hasPermission('manage-roles')) {
                $data['employees'] = Employee::where('deleted_at',null)->OrderBy('id','ASC')->get();
                return view('admin.employees.list')->with($data);
            } else{
                return view('layouts.errors.403');
            }
        } catch ( Exception $e) {
            Log::debug($e);
        }
    }
    public function create()
    {
        try{
            $user = Auth::guard('employee')->user();
            if ($user->hasPermission('manage-roles')) {
                $data['roles'] = Role::where('deleted_at',null)->get();
                return view('admin.employees.create')->with($data);
            } else{
                return view('layouts.errors.403');    
            }
        } catch ( Exception $e) {
            Log::debug($e);
        }
    }
    public function store(Request $request)
    {
        try{
            if (isset($request->id) && !empty($request->id)) {
                if (Employee::where('id', $request->id)->first()->email != $request->email) {
                    $this->rules['email'] = 'required|email|unique:employees,email';
                }
            } else {
                $this->rules['email']      = 'required|email|unique:employees,email';
                $this->rules['password']   = 'required|min:6';
            }
            $Validator = Validator::make($request->all(), $this->rules, $this->customeMessage);
            if ($Validator->fails()) {
                return back()->withInput()->withErrors($Validator->errors());
            } else {
                $id = $request->id;
                $first_name = $request->first_name;
                $last_name = $request->last_name;
                $email = $request->email;
                $status = $request->status;
                $role = $request->role;
                if ($id!="") {
                    $employee = Employee::find($id);
                    $role_user = RoleUser::where('user_id',$id)->first();
                } else{
                    $employee = new Employee();
                    $role_user = new RoleUser();
                }
                $employee->first_name = $first_name;
                $employee->last_name = $last_name;
                $employee->email = $email;
                $employee->role = $role;
                if ($request->has('password')) {
                    $password = $request->password;
                    $password_hash = Hash::make($password);
                    $employee->password = $password_hash;
                }
                if ($request->has('new_password') && $request->new_password!="") {
                    $new_password = $request->new_password;
                    $new_password_hash = Hash::make($new_password);
                    $employee->password = $new_password_hash;
                }
                if ($employee->status != $status) {
                    if ($status==1) {
                        MailHelper::statusActive($employee);
                    } elseif($status==0){
                        MailHelper::statusInActive($employee);
                    }
                }
                $employee->status = $status;
                $employee->save();
                $role_user->user_id = $employee->id;
                $role_user->role_id = $role;
                $role_user->user_type = 'App\Shop\Employees\Employee';
                $role_user->save(); 
                if ($id!="") {
                    if ($request->new_password!="") {
                        MailHelper::EmployeePasswordChange($employee);    
                    }
                    return redirect()->route('admin.employees.index')->with('message','Vender update successfully');
                }
                else{
                    MailHelper::EmployeeCreate($employee);
                    return redirect()->route('admin.employees.index')->with('message','Vender create successfully');
                }
            }
        } catch ( Exception $e) {
            Log::debug($e);
        }
    }
    // public function show(int $id)
    // {
    //     $employee = $this->employeeRepo->findEmployeeById($id);
    //     return view('admin.employees.show', ['employee' => $employee]);
    // }
    public function edit($id)
    {
        try{
            $user = Auth::guard('employee')->user();
            if ($user->hasPermission('manage-roles')) {
                $data['roles'] = Role::where('deleted_at',null)->get();
                $data['employees'] = Employee::where('deleted_at',null)->where('id',$id)->first();
                return view('admin.employees.create')->with($data);
            }else{
                return view('layouts.errors.403');    
            }
        } catch ( Exception $e) {
            Log::debug($e);
        }
    }
    public function destroy(int $id)
    {
        try{
            $user = Auth::guard('employee')->user();
            if ($user->hasPermission('manage-roles')) {
                $time = Carbon::now();
                $time_string = $time->toDateTimeString();
                if (Employee::where('id',$id)->exists()) {
                    $employee = Employee::where('id',$id)->first();
                    $employee->deleted_at = $time_string;
                    $employee->save();
                    return redirect()->route('admin.employees.index')->with('message', 'Vender delete successfully');
                }
            } else{
                return view('layouts.errors.403');
            }
        } catch ( Exception $e) {
            Log::debug($e);
        }

    }
    public function is_active($id)
    {
        try{
            $user = Auth::guard('employee')->user();
            if ($user->hasPermission('manage-roles')) {
                if (Employee::where('id',$id)->exists()) {
                    $employee = Employee::where('id',$id)->first();
                    if ($employee->status==1) {
                        $employee->status = 0;
                        MailHelper::statusInActive($employee);
                    } elseif($employee->status==0){
                        $employee->status = 1;
                        MailHelper::statusActive($employee);
                    }
                    $employee->save();
                    return redirect()->route('admin.employees.index')->with('message', 'Vender status updated successfully');
                }
            } else{
                return view('layouts.errors.403');
            }    
        } catch ( Exception $e) {
            Log::debug($e);
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProfile($id)
    {
        try{
            $data['employee'] = Employee::where('id',$id)->first();
            return view('admin.employees.profile')->with($data);
        } catch ( Exception $e) {
            Log::debug($e);
        }
    }
    public function updateProfile(UpdateEmployeeRequest $request, $id)
    {
        try{
            $first_name = $request->first_name;
            $last_name = $request->last_name;
            $email = $request->email;
            $username = $request->username;
            $old_password = $request->old_password;
            $password = $request->password;
            $password_confirmation = $request->password_confirmation;
            $employee = Employee::where('id',$id)->first();
            if ($request->has('password') && $request->input('password') != '') {
                if(!Hash::check($old_password, $employee->password)){
                    return back()->with('error','The specified password does not match the database password');
                } else{
                    $password_hash = Hash::make($password);
                    $employee->password = $password_hash;        
                }
            }
            $employee->first_name = $first_name;
            $employee->last_name = $last_name;
            $employee->email = $email;
            $employee->username = $username;
            if($employee->save()){
                if ($request->has('profile_image')) {
                    $time      = md5(time());
                    $file      = $request->profile_image;
                    $extension = $file->getClientOriginalExtension();
                    $profile   = $time . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path('employee/profile/'), $profile);
                    $profile                 = 'employee/profile/' . $profile;
                    $profile_update          = Employee::find($employee->id);
                    $profile_update->profile_image = $profile;
                    $profile_update->save();
                }
            }
            return redirect()->route('admin.employee.profile', $id)
                ->with('message', 'Profile update  successfully.');
        } catch ( Exception $e) {
            Log::debug($e);
        }
    }
}
