<?php

namespace App\Http\Controllers\Admin\News;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Front\Categories;
use App\Model\Front\News;
use App\Model\Front\Seo;
use Validator;
use DB;
use Auth;

class NewsController extends Controller
{
   	public function index(Request $request){
   		try{
            
            $data['news'] = DB::table('news')
                    ->select('news.*','categories.name as category_name')
                    ->leftjoin('categories','categories.id','=','news.category')
                    ->OrderBy('news.id','desc')
                    ->get();
   			return view('admin.news.list')->with($data);
	   	}
	   	catch(Exception $e){
            log::error($e);
        }
	}
   	public function create(){
	   	try{   
	   		$data['categories'] = Categories::select('id','name')->get();
            return view('admin.news.create')->with($data);
	   	}
	   	catch(Exception $e){
            log::error($e);
        }
   	}
	public function store(Request $request){
	   	try{
	   		$rules =[
            	'title' => 'required',
                'sub_title' => 'required|string|max:255',
                'content' => 'required',
                'publication_date' => 'required',
                'category_id' => 'required',
                'section_tags' => ' required',
                'layouts_image' => 'required',
                'status' => 'required'
            ];
            $customeMessage = [
                'title.required' => 'Please enter Title.',
                'sub_title.required' => 'Please enter Sub Title.',
                'content.required' => 'Please enter Content.',
                'image.required' => 'Please select background image.',
                'publication_date.required' => 'Please enter Publication Date.',
                'section_tags.required' => 'Please enter Section Tag.',
                'layouts_image.required' => 'Please enter layouts.',
                'image.image' => 'Please select valid image',
                'status.required' => 'Please select Status.'
            ];
            if($request->id==""){
        		$rules['image'] ='required|image';
            }
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            if( $validator->fails() ){
                return back()->withInput()->withErrors($validator->errors());
            } else{
                /* SEO */ 
                $seo_page_title = trim(strip_tags($request->seo_page_title));
                $seo_page_description = trim(strip_tags($request->seo_page_description));
                $seo_page_keyword = $request->seo_page_keyword;
                $public_metatags = $request->public_metatags;
                /* News */ 
                $title = trim(strip_tags($request->title));
                $sub_title = trim(strip_tags($request->sub_title));
                $content = trim(strip_tags($request->content));
                $publication_date = date('Y-m-d',strtotime($request->publication_date));
                $category_id = trim(strip_tags($request->category_id));
                $section_tags = implode(',', $request->section_tags);
                $layouts_image = trim(strip_tags($request->layouts_image));
                $status = $request->status;
                if($request->id!=""){
                    $news=News::find($request->id);
                }else{
                    $news=New News;
                }
                if (isset($request->seo_id) && $request->seo_id!=0) {
                    $seo = Seo::find($request->seo_id);
                } else{
                    $seo = New Seo;
                }
                
                /* SEO */ 
                $seo->page_title = $seo_page_title;
                $seo->page_description = $seo_page_description;
                if (isset($request->seo_page_keyword)) {
                    $seo_page_keyword = implode(',', $seo_page_keyword);
                }
                if (isset($request->public_metatags)) {
                    $public_metatags = implode(',', $public_metatags);
                }
                $seo->page_keywords = $seo_page_keyword;
                $seo->public_metatags = $public_metatags;
                $seo->save();
                /* News */ 
                $news->category = $category_id;
                $news->title = $title;
                $news->status = $status;
                $news->sub_title = $sub_title;
                $news->body = $content;
                $news->publication_date = $publication_date;
                $news->layouts = $layouts_image;
                $news->section_tags = $section_tags;
                $news->seo_id = $seo->id;
                if($news->save()){
                    if ($request->has('image')) {
                        $time      = md5(time());
                        $file      = $request->image;
                        $extension = $file->getClientOriginalExtension();
                        $image   = $time . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('news/image/'), $image);
                        $image                 = 'news/image/' . $image;
                        $image_update          = News::find($news->id);
                        $image_update->image = $image;
                        $image_update->save();
                    }
                }
                if($request->id!=""){
                	return redirect()->route('admin.news.list')->with('message', 'News updated successfully.');
                }
                else{
                	return redirect()->route('admin.news.list')->with('message', 'News create successfully.');
            	}
            }
   	    }
	   	catch(Exception $e){
	       log::error($e);
        }
   	}
   	public function destroy($id)
	{
		try{
            if (News::where('id',$id)->exists()) {
                $news_delete = News::where('id',$id);
                $news_delete->delete();
                return redirect()->route('admin.news.list')->with('message', 'News delete successfully.');
            }
        }
        catch (Exception $e) {
            log::error($e);
        }
	}
	public function edit($id)
	{
		try{
            if(News::where('id',$id)->exists()){  
            	$data['categories'] = Categories::select('id','name')->get();
                $data['news'] = News::where('id',$id)->first();
                $data['seo'] = Seo::where('id',$data['news']->seo_id)->first();
                return view('admin.news.create')->with($data);    
            }
        }
        catch(Exception $e){
            log::error($e);
        }
	} 
    public function status($id)
    {
        try{
            if (News::where('id',$id)->exists()) {
                $news = News::where('id',$id)->first();
                if ($news->status==1) {
                    $news->status = 0;
                } elseif($news->status==0){
                    $news->status = 1;
                }
                $news->save();
                return redirect()->route('admin.news.list')->with('message', 'News status updated successfully');
            }
        } catch ( Exception $e) {
            log::error($e);
        }
    }
}
