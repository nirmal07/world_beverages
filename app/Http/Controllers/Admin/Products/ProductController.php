<?php

namespace App\Http\Controllers\Admin\Products;

use App\Http\Controllers\Controller;
use App\Model\Front\Categories;
use App\Model\Country;
use Illuminate\Http\Request;
use App\Model\Front\DigitalDirectory;
use App\Model\Front\Customer;
use App\Model\Front\Products;
use App\Model\Front\ProductsImage;
use App\Model\Front\Seo;
use Validator;
use DB;
use Session;
use Auth;

class ProductController extends Controller
{
	public function index(Request $request)
	{
		try{
            $user = Auth::guard('employee')->user();
            $page   = trim($request->page);
            $product_limit =10;
            $data['products'] = DB::table('products')
                    ->select('products.*','categories.name as  categories_name')
                    ->leftjoin('categories','categories.id','=','products.category_id')
                    ->orderBy('products.id','desc')
                    ->get();
			return view('admin.products.list')->with($data);
		}
		catch (Exception $e) {
            Log::debug($e);
        }
	}
	public function create(Request $request)
	{
		try{
            $data['categories'] = Categories::where('parent_id',32)->select('id','name')->get();
            $data['users'] = Customer::get();
            $data['companys'] = DigitalDirectory::get();
            $data['countrys'] = Country::get();
			return view('admin.products.create')->with($data);
		}
		catch (Exception $e) {
            Log::error($e);
        }
	}
	public function store(Request $request)
	{
		try{
            $rules =[
            	'user' => 'required',
                'company_id' => 'required',
                'name' => 'required',
                'category_id' => 'required',
                'country' => 'required',
                // 'image' => 'image',
                // 'additional_image' => 'image',
                // 'brand_header' => 'image',
                // 'size' => 'required|max:50',
                // 'design_no' => 'required|numeric|digits_between:1,50',
            ];
            $customeMessage = [
                'user.required' => 'Please select User Id.',
                'company_id.required' => 'Please enter Company Id.',
                'name.required' => 'Please enter Name.',
                'category_id.required' => 'Please enter Category Id.',
                'country.required' => 'Please enter Country.',
                'image.required' => 'Please select Image.',
                'image.image' => 'Please select valid image',
                'additional_image.image' => 'Please select valid image',
                'brand_header.image' => 'Please select valid image',
                // 'size.required' => 'Please enter size.',
                // 'design_no.required' => 'Please enter design no.',
                // 'cover.required' => 'Please select product image.',
                // 'background_image.required' => 'Please select background image.',
            ];
            if($request->id==""){
                $rules['image'] ='required|image';
            }
            $validator = Validator::make($request->all(),$rules,$customeMessage);
                if( $validator->fails() ){
                    return back()->withInput()->withErrors($validator->errors());
                } else{  
                	$user = trim(strip_tags($request->user));
                    $company_id = trim(strip_tags($request->company_id));
                    $create_date = trim(strip_tags($request->create_date));
                    $edit_date = trim(strip_tags($request->edit_date));
                    $name = trim(strip_tags($request->name));
                    $status = trim(strip_tags($request->status));
                    $priority = trim(strip_tags($request->priority));
                    $category_id = trim(strip_tags($request->category_id));
                    $create_date = trim(strip_tags($request->create_date));
                    $country = trim(strip_tags($request->country));
                    $quantity = trim(strip_tags($request->quantity));
                    $price_unit_usd = trim(strip_tags($request->price_unit_usd));
                    $price_unit_eur = trim(strip_tags($request->price_unit_eur));
                    $production_year = trim(strip_tags($request->production_year));
                    $product_ac_vol = trim(strip_tags($request->product_ac_vol));
                    $productformat = trim(strip_tags($request->productformat));
                    $current_world_distribution = trim(strip_tags($request->current_world_distribution));
                    $desired_world_distribution = trim(strip_tags($request->desired_world_distribution));
                    $current_us_distribution = trim(strip_tags($request->current_us_distribution));
                    $desired_us_distribution = trim(strip_tags($request->desired_us_distribution));
                    $pick_up_location = trim(strip_tags($request->pick_up_location));
                    $pick_lat = trim(strip_tags($request->pick_lat));
                    $pic_lon = trim(strip_tags($request->pic_lon));
                    $seo_title = trim(strip_tags($request->seo_title));
                    $seo_page_description = trim(strip_tags($request->seo_page_description));
                    $product_appellation = trim(strip_tags($request->product_appellation));
                    $product_description_short = trim(strip_tags($request->product_description_short));
                    $product_description_long = trim(strip_tags($request->product_description_long));
                    
                }
                if($request->id!=""){
                    $product=Products::find($request->id);
                }else{
                    $product=New Products;
                }
                if (isset($request->seo_id) && $request->seo_id!=0) {
                    $seo = Seo::find($request->seo_id);
                } else{
                    $seo = New Seo;
                }
                if (isset($request->brand_tag)) {
                    $brand_tag = implode(',', $request->brand_tag);
                    $product->brand_tag = $brand_tag;
                }
                /*SEO*/ 
                $seo->page_title = $seo_title;
                $seo->page_description = $seo_page_description;
                if (isset($request->seo_page_keywords)) {
                    $seo_page_keywords = implode(',', $request->seo_page_keywords);
                    $seo->page_keywords = $seo_page_keywords;
                }
                $seo->save();
                /* Product */ 
                $product->user = $user;
                $product->company_id = $company_id;
                
                $product->name = $name;
                $product->status = $status;
                $product->priority = $priority;
                $product->category_id = $category_id;
                $product->country = $country;
                $product->quantity = $quantity;
                $product->price_unit_usd = $price_unit_usd;
                $product->price_unit_eur = $price_unit_eur;
                $product->production_year = $production_year;
                $product->product_ac_vol = $product_ac_vol;
                $product->productformat = $productformat;
                $product->current_world_distribution = $current_world_distribution;
                $product->desired_world_distribution = $desired_world_distribution;
                $product->current_us_distribution = $current_us_distribution;
                $product->desired_us_distribution = $desired_us_distribution;
                $product->pick_up_location = $pick_up_location;
                $product->pick_lat = $pick_lat;
                $product->pic_lon = $pic_lon;
                $product->product_appellation = $product_appellation;
                $product->product_description_short = $product_description_short; 
                $product->description = $product_description_long;
                $product->product_description_short = $product_description_short; 
                $product->seo_id = $seo->id;
                $product->created_at = date('Y-m-d',strtotime($create_date));
                $product->updated_at = date('Y-m-d',strtotime($edit_date));
                if($product->save()){
                    if ($request->has('additional_image')) {
                        $image_array = $request->file('additional_image');
                        $array_len = count($image_array);
                        for ($i=0; $i <$array_len ; $i++) {
                            $time      = md5(time()); 
                            $img_ext = $image_array[$i]->getClientOriginalExtension();
                            $cover   = $time . '.' . $image_array[$i]->getClientOriginalExtension();
                            $new_image_name   = 'product/additional_image/_' . $i. $cover;
                            $destination_path = public_path('product/additional_image/');
                            $image_array[$i]->move($destination_path,$new_image_name);
                            $cover                 = 'product/additional_image/' .$cover;
                            // $cover_update          = Product::find($product->id);
                            // $cover_update->cover = $cover;
                            // $cover_update->save();
                            $product_id = $product->id;
                            $product_image=New ProductsImage;
                            $product_image->product_id = $product_id;
                            $product_image->src = $new_image_name; 
                            $product_image->save();
                        }
                    }
                    if ($request->has('header_image')) {
                        $time      = md5(time());
                        $file      = $request->header_image;
                        $extension = $file->getClientOriginalExtension();
                        $header_image   = $time . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('product/header_image/'), $header_image);
                        $header_image                 = 'product/header_image/' . $header_image;
                        $header_image_update          = Products::find($product->id);
                        $header_image_update->header_image = $header_image;
                        $header_image_update->save();
                    }
                    if ($request->has('image')) {
                        $time      = md5(time());
                        $file      = $request->image;
                        $extension = $file->getClientOriginalExtension();
                        $image   = $time . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('product/image/'), $image);
                        $image                 = 'product/image/' . $image;
                        $image_update          = Products::find($product->id);
                        $image_update->img = $image;
                        $image_update->save();
                    }
                }
                if($request->id!=""){
                	return redirect()->route('admin.products.index')->with('message', 'Product updated successfully.');
                }
                else{
                	return redirect()->route('admin.products.index')->with('message', 'Product create successfully.');
            	}
            }
        catch (Exception $e) {
            log::error($e);
        }
    }
	public function edit($id)
	{
		try{
            if(Products::where('id',$id)->exists()){  
            	$data['categories'] = Categories::where('parent_id',32)->select('id','name')->get();
                $data['users'] = Customer::get();
                $data['companys'] = DigitalDirectory::get();
                $data['countrys'] = Country::get();
                $data['product_images'] = ProductsImage::where('product_id',$id)->get();
                $data['product'] = Products::where('id',$id)->first();
                $data['seo'] = Seo::where('id',$data['product']->seo_id)->first();
                return view('admin.products.create')->with($data);    
            }else{
                return view('layouts.errors.403');
            }
        }
        catch(Exception $e){
            log::error($e);
        }
	}
	public function destroy($id)
	{
		try{
            if (Products::where('id',$id)->exists()) {
                $product_delete = Products::where('id',$id)->delete();
                return redirect()->route('admin.products.index')->with('message', 'Product delete successfully.');
            }
        }
        catch (Exception $e) {
            Exceptions::exception($e);
        }
		
	}
    // public function removeimage($id)
    // {
    //     try{

    //         if(Product_image::where('id',$id)->exists()){
    //             $image_delete = Product_image::where('id',$id)->delete();
    //             return "true";
    //         }
    //         else{
    //             return "false";
    //         }
    //     }
    //     catch(Exception $e){
    //         $Exceptions = New Exceptions;
    //         $Exceptions->sendException($e);  
    //     }
    // }
    public function removeThumbnail(Request $request)
    {
        try{
            $id = $request->id;
            if(Product_image::where('id',$id)->exists()){
                $image_delete = Product_image::where('id',$id)->delete();
                return redirect()->back()->with('message', 'Image remove successfully.');
            }
            else{
                return back();
            }
        }
        catch(Exception $e){
            $Exceptions = New Exceptions;
            $Exceptions->sendException($e);  
        }
    }
}
