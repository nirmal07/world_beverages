<?php

namespace App\Http\Controllers\Admin\DigitalDirectory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Front\Categories;
use App\Model\Country;
use App\Model\State;
use App\Model\Front\DigitalDirectory;
use App\Model\Front\Seo;
use Validator;
use Log;


class DigitalDirectoryController extends Controller
{
    public function index(){
    	try {
            $data['digitaldirectorys'] = DigitalDirectory::where('non_listed_consultant','')->get();
    		return view('admin.digital_directory.list')->with($data);
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
    public function create(){
    	try {
            $data['countries'] = Country::get();
            $data['us_state'] = State::where('country_id',231)->get();
    		$data['categories'] = Categories::where('parent_id','35')->get();
    		return view('admin.digital_directory.create')->with($data);
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
    public function store(Request $request)
    {
        try{
            $rules =[
                'company_name' => 'required|string|max:255',
                'phone'=> 'required',
                'contact_person_name'=>'required',
                'contact_person_title' =>'required',
                'email'=>'required|email',
                'category_id'=>'required',
                'company_location'=>'required',
                // 'com_website'=>'nullable|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
                // 'twitter_url'=>'nullable|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
                // 'facebook_url'=>'nullable|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
                // 'linkedin_url'=>'nullable|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',

            ];
            $customeMessage = [
                'company_name.required' => 'Please enter Name.',
                'phone.required' => 'Please enter Phone Number.',
                'contact_person_name.required' => 'Please enter Contact Person Name.',
                'contact_person_title.required' => 'Please enter Contact Person Title.',
                'email.required' => 'Please enter Email.',
                'email.email' => 'Please enter valid Email',
                'category_id.required' => 'Please enter Category.',
                'company_location.required' => 'Please enter Company Location.',
                'com_website.regex' => 'Please enter valid Url.',
                'twitter_url.regex' => 'Please enter valid Url.',
                'facebook_url.regex' => 'Please enter valid Url.',
                'linkedin_url.regex' => 'Please enter valid Url.',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            if( $validator->fails() ){
                return back()->withInput()->withErrors($validator->errors());
            } else{ 
                $id = trim(strip_tags($request->id));
                $directory_request = trim(strip_tags($request->directory_request));
                $directory_approve = trim(strip_tags($request->directory_approve));
                $directory_featured = trim(strip_tags($request->directory_featured));
                $consultant_listing_request = trim(strip_tags($request->consultant_listing_request));
                $consultant_listing_approve = trim(strip_tags($request->consultant_listing_approve));
                $listing_priority = trim(strip_tags($request->listing_priority));
                $company_name = trim(strip_tags($request->company_name));
                $category_id = trim(strip_tags($request->category_id));
                $subcat_id = trim(strip_tags($request->sub_cat));
                $company_location = trim(strip_tags($request->company_location));
                $country = trim(strip_tags($request->country));
                if ($country!="") {
                    $country = Country::where('country',$country)->pluck('id')->first();
                }
                $state = trim(strip_tags($request->state));
                $city = trim(strip_tags($request->city));
                $zip_code = trim(strip_tags($request->zip_code));
                $phone = trim(strip_tags($request->phone));
                $fax = trim(strip_tags($request->fax));
                $contact_person_name = trim(strip_tags($request->contact_person_name));
                $contact_person_title = trim(strip_tags($request->contact_person_title));
                $email = trim(strip_tags($request->email));
                $com_website = trim(strip_tags($request->com_website));
                $twitter_url = trim(strip_tags($request->twitter_url));
                $facebook_url = trim(strip_tags($request->facebook_url));
                $linkedin_url = trim(strip_tags($request->linkedin_url));
                $annual_case_production = trim(strip_tags($request->annual_case_production));
                $avg_bottle_price = trim(strip_tags($request->avg_bottle_price));
                $vineyard_acres = trim(strip_tags($request->vineyard_acres));
                $grows_grapes = trim(strip_tags($request->grows_grapes));
                $sells_grapes = trim(strip_tags($request->sells_grapes));
                $custom_crush = trim(strip_tags($request->custom_crush));
                $aternating_propietorship = trim(strip_tags($request->aternating_propietorship));

                $featured_broker = trim(strip_tags($request->featured_broker));
                $is_pro = trim(strip_tags($request->is_pro));
                
                $company_experience = trim(strip_tags($request->company_experience));
                $country_coverd = trim(strip_tags($request->country_coverd));
                $us_state_covered = trim(strip_tags($request->us_state_covered));
                $compensation_method = trim(strip_tags($request->compensation_method));
                $seo_title = trim(strip_tags($request->seo_title));
                $seo_page_description = trim(strip_tags($request->seo_page_description));
                $section_tags = trim(strip_tags($request->section_tags));
                $short_description = trim(strip_tags($request->short_description));
                $description = trim(strip_tags($request->description));
                  
                if($id!=""){
                    $digital_directory=DigitalDirectory::find($id);
                }else{
                    $digital_directory=New DigitalDirectory();
                }
                if (isset($request->seo_id) && $request->seo_id!=0) {
                    $seo = Seo::find($request->seo_id);
                } else{
                    $seo = New Seo;
                }
                /* SEO */ 
                $seo->page_title = $seo_title;
                $seo->page_description = $seo_page_description;
                if (isset($request->seo_page_keywords)) {
                    $seo_page_keyword = implode(',', $request->seo_page_keywords);
                    $seo->page_keywords = $seo_page_keyword;
                }
                $seo->save();
                /* Digital Directory */ 
                $digital_directory->directory_request = $directory_request;
                $digital_directory->directory_approve = $directory_approve;
                $digital_directory->directory_feature = $directory_featured;
                $digital_directory->consultant_listing_request = $consultant_listing_request;
                $digital_directory->consultant_listing_approve = $consultant_listing_approve;
                $digital_directory->listing_priority = $listing_priority;
                $digital_directory->name = $company_name;
                $digital_directory->company_type = $category_id;
                $digital_directory->sub_cat = $subcat_id;
                $digital_directory->company_location = $company_location;
                $digital_directory->country = $country;
                $digital_directory->state = $state;
                $digital_directory->city = $city;
                $digital_directory->zip_code = $zip_code;
                $digital_directory->phone = $phone;
                $digital_directory->fax = $fax;
                $digital_directory->contact_person_name = $contact_person_name;
                $digital_directory->contact_person_title = $contact_person_title;
                $digital_directory->email = $email;

                $digital_directory->twitter_url = $twitter_url;
                $digital_directory->facebook_url = $facebook_url;
                $digital_directory->linkedin_url = $linkedin_url;
                $digital_directory->annual_case_production = $annual_case_production;
                $digital_directory->avg_bottle_price = $avg_bottle_price;
                $digital_directory->vineyard_acres = $vineyard_acres;
                $digital_directory->grows_grapes = $grows_grapes;
                $digital_directory->sells_grapes = $sells_grapes;
                $digital_directory->custom_crush = $custom_crush;
                $digital_directory->aternating_propietorship = $aternating_propietorship;
                $digital_directory->featured_broker = $featured_broker;
                $digital_directory->is_pro = $is_pro;
                if (isset($request->company_skills)) {
                    $company_skills = implode(',', $request->company_skills);
                    $digital_directory->company_skills = $company_skills;
                }
                $digital_directory->website = $com_website;
                if (isset($request->currentProductLine)) {
                    $currentProductLine = implode(',', $request->currentProductLine);
                    $digital_directory->currentProductLine = $currentProductLine;
                }
                if (isset($request->productlooking)) {
                    $productlooking = implode(',', $request->productlooking);
                    $digital_directory->productlooking = $productlooking;
                }
                
                if (isset($request->varietals_produced)) {
                    $varietals_produced = implode(',', $request->varietals_produced);
                    $digital_directory->varietals_produced = $varietals_produced;
                }
                if (isset($request->primary_appellations)) {
                    $primary_appellations = implode(',', $request->primary_appellations);
                    $digital_directory->primary_appellations = $primary_appellations;
                }
                $digital_directory->company_experience = $company_experience;
                $digital_directory->country_coverd = $country_coverd;
                $digital_directory->us_state_covered = $us_state_covered;
                $digital_directory->compensation_method = $compensation_method;
                $digital_directory->short_description = $short_description;
                $digital_directory->description = $description;
                $digital_directory->seo_id = $seo->id;
                if($digital_directory->save()){
                    if ($request->has('image')) {
                        $time      = md5(time());
                        $file      = $request->image;
                        $extension = $file->getClientOriginalExtension();
                        $logo   = $time . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('digital_directory/logo/'), $logo);
                        $image                 = 'digital_directory/logo/' . $logo;
                        $image_update          = DigitalDirectory::find($digital_directory->id);
                        $image_update->logo = $image;
                        $image_update->save();
                    }
                }
            }
            if($request->id!=""){    
                return redirect()->route('admin.digitaldirectory.list')->with('message', 'DigitalDirectory Update Succesfully.');
            }
            else{
                return redirect()->route('admin.digitaldirectory.list')->with('message', 'DigitalDirectory Create Succesfully.');
            }
        }
        catch (Exception $e) {
            log::error($e);
        }
    }
    public function edit($id)
    {
        try{
            if(DigitalDirectory::where('id',$id)->exists()){  
                $data['digitaldirectory'] =  DigitalDirectory::where('id',$id)->first();
                $data['categories'] = Categories::where('parent_id','35')->get();
                $data['countries'] = Country::get();
                $data['us_state'] = State::where('country_id',231)->get();
                $data['seo'] = Seo::where('id',$data['digitaldirectory']->seo_id)->first();
                return view('admin.digital_directory.create')->with($data);    
            }
        }
        catch(\Exception $e){
            log::error($e);
        }
    }
    public function destroy(int $id)
    {
        try{
            if (DigitalDirectory::where('id',$id)->exists()) {
                $digitaldirectory = DigitalDirectory::where('id',$id)->delete();
                return redirect()->route('admin.digitaldirectory.list')->with('message', 'Digital Directory delete successfully.');
            }
        }
        catch (Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function getSubdigicategory($id)
    {
        try {
            $get_subcat = Categories::where('subcat_id',$id)->pluck('id','name');
            return $get_subcat;
        } catch (Exception $e) {
            log::error($e);
        }
    }
}
