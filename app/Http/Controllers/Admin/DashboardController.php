<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Shop\Employees\AvailableEmployee;
use App\Shop\Employees\Employee;
use App\Shop\Discount\Discount;
use App\Shop\AddToCart\Po;
use App\Shop\News\News;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        
        $breadcumb = [
            ["name" => "Dashboard", "url" => route("admin.dashboard"), "icon" => "fa fa-dashboard"],
            ["name" => "Home", "url" => route("admin.dashboard"), "icon" => "fa fa-home"],

        ];
        // populate_breadcumb($breadcumb);
        return view('admin.dashboard', [
                        "breadcumbs" => $breadcumb
                    ]);
    }
}
