<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use redirect;
use Validator;
use Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public $rules=[
        'email' => 'required|email',
        'password' => 'required'
    ];

    public $customeMessage=[
        'email.required' => 'Please enter email',
        'password.required' =>'Please enter password',
    ];

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';


    /**
     * Shows the admin login form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        try{
            if (auth()->guard('employee')->check()) {
                return redirect()->route('admin.dashboard');
            }
            return view('auth.admin.login');
        } catch(exception $e){
            Log::error($e);
        }
    }

    /**
     * Login the employee
     *
     * @param LoginRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),$this->rules,$this->customeMessage);
            if( $validator->fails() ){
                return back()->withInput()->withErrors($validator->errors());
            } else{
                $this->validateLogin($request);
                // If the class is using the ThrottlesLogins trait, we can automatically throttle
                // the login attempts for this application. We'll key this by the username and
                // the IP address of the client making these requests into this application.
                if ($this->hasTooManyLoginAttempts($request)) {
                    $this->fireLockoutEvent($request);
                    return $this->sendLockoutResponse($request);
                }
                $details = $request->only('email', 'password');
                $details['status'] = 1;

                if(Auth::guard('employee')->attempt(['email'=>$request->email,'password'=>$request->password,'status'=>1])){
                    return redirect()->route('admin.dashboard');
                }
                // if (auth()->guard('employee')->attempt($details)) {
                //     return $this->sendLoginResponse($request);
                // }
                // If the login attempt was unsuccessful we will increment the number of attempts
                // to login and redirect the user back to the login form. Of course, when this
                // user surpasses their maximum number of attempts they will get locked out.
                $this->incrementLoginAttempts($request);

                return $this->sendFailedLoginResponse($request);
            }
        } catch (Exception $e) {
            Log::error($e);
        }

    }
}
