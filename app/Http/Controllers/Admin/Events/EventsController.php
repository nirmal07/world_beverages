<?php

namespace App\Http\Controllers\Admin\Events;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Front\Categories;
use App\Model\Front\Events;
use App\Model\Front\Seo;
use Validator;

class EventsController extends Controller
{
    public function index(Request $request){
    	try {
    		$events_limit = 10;
    		$data['events'] = Events::paginate($events_limit);
    		return view('admin.events.list')->with($data);
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
    public function create(Request $request){
    	try {
    		$data['categories'] = Categories::select('id','name')->get();
    		return view('admin.events.create')->with($data);
    	} catch (Exception $e) {
    		log::error($e);	
    	}
    }
    public function store(Request $request){
	   	try{
	   		$rules =[
            	'title' => 'required|max:500',
                'description' => 'required',
                'category_id' => 'required',
                'priority' => 'required',
                'start_date'=> 'required',
                'end_date'=> 'required',
                'section_tags' => ' required',
                'layouts_image' => 'required',
                'status' => 'required',
                // 'further_info_url' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
                // 'further_info_email' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/'

            ];
            $customeMessage = [
                'title.required' => 'Please enter Title.',
                'priority.required' => 'Please enter Priority.',
                'description.required' => 'Please enter Description.',
                'end_date.required' => 'Please enter Event End Date.',
                'image.required' => 'Please select image.',
                'start_date.required' => 'Please enter Event Start Date.',
                'section_tags.required' => 'Please enter Section Tag.',
                'layouts_image.required' => 'Please enter layouts.',
                'status.required' => 'Please Select Status.',
                'image.image' => 'Please select valid image',
                'further_info_url.regex' => 'Please enter valid Url.',
                'further_info_email.regex' => 'Please enter valid Url.',
            ];
            if($request->id==""){
        		$rules['image'] ='required|image';
            }
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            if( $validator->fails() ){
                return back()->withInput()->withErrors($validator->errors());
            } else{
                /* SEO */ 
                $seo_page_title = trim(strip_tags($request->seo_page_title));
                $seo_page_description = trim(strip_tags($request->seo_page_description));
                $seo_page_keyword = $request->seo_page_keyword;
                $public_metatags = $request->public_metatags;
                /* Events */ 
                $title = trim(strip_tags($request->title));
                $priority = trim(strip_tags($request->priority));
                $description = trim(strip_tags($request->description));
                $start_date = date('Y-m-d',strtotime($request->start_date));
                $end_date = date('Y-m-d',strtotime($request->end_date));
                $category_id = trim(strip_tags($request->category_id));
                $section_tags = implode(',', $request->section_tags);
                $layouts_image = trim(strip_tags($request->layouts_image));
                $event_map = trim(strip_tags($request->event_map));
                $event_location = trim(strip_tags($request->event_location));
                $event_opening_hour = trim(strip_tags($request->event_opening_hour));
                $organizer_profile = trim(strip_tags($request->organizer_profile));
                $further_info_email = trim(strip_tags($request->further_info_email));
                $further_info_url = trim(strip_tags($request->further_info_url));
                $registration_url = trim(strip_tags($request->registration_url));
                $cost = trim(strip_tags($request->cost));
                $status = $request->status;
                if($request->id!=""){
                    $events = Events::find($request->id);
                }else{
                    $events = New Events;
                }
                if (isset($request->seo_id) && $request->seo_id!=0) {
                    $seo = Seo::find($request->seo_id);
                } else{
                    $seo = New Seo;
                }
                /* SEO */ 
                $seo->page_title = $seo_page_title;
                $seo->page_description = $seo_page_description;
                if (isset($request->seo_page_keyword)) {
                    $seo_page_keyword = implode(',', $seo_page_keyword);
                }
                if (isset($request->public_metatags)) {
                    $public_metatags = implode(',', $public_metatags);
                }
                $seo->page_keywords = $seo_page_keyword;
                $seo->public_metatags = $public_metatags;
                $seo->save();
                /* News */ 
                $events->category = $category_id;
                $events->title = $title;
                $events->status = $status;
                $events->priority = $priority;
                $events->description = $description;
                $events->start_date = $start_date;
                $events->end_date = $end_date;
                $events->event_map = $event_map;
                $events->location = $event_location;
                $events->event_opening_hour = $event_opening_hour;
                $events->organizer_profile = $organizer_profile;
                $events->further_info_email = $further_info_email;
                $events->further_info_url = $further_info_url;
                $events->registration_url = $registration_url;
                $events->cost = $cost;
                $events->layout_image = $layouts_image;
                $events->section_tags = $section_tags;
                $events->seo_id = $seo->id;
                if($events->save()){
                    if ($request->has('image')) {
                        $time      = md5(time());
                        $file      = $request->image;
                        $extension = $file->getClientOriginalExtension();
                        $image   = $time . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('events/image/'), $image);
                        $image                 = 'events/image/' . $image;
                        $image_update          = Events::find($events->id);
                        $image_update->image = $image;
                        $image_update->save();
                    }
                }
                if($request->id!=""){
                	return redirect()->route('admin.events.list')->with('message', 'Events updated successfully.');
                }
                else{
                	return redirect()->route('admin.events.list')->with('message', 'Events create successfully.');
            	}
            }
   	    }
	   	catch(Exception $e){
	       log::error($e);
        }
   	}
   	public function edit($id)
	{
		try{
            if(Events::where('id',$id)->exists()){  
            	$data['categories'] = Categories::select('id','name')->get();
                $data['event'] = Events::where('id',$id)->first();
                $data['seo'] = Seo::where('id',$data['event']->seo_id)->first();
                return view('admin.events.create')->with($data);    
            }
        }
        catch(Exception $e){
            log::error($e);
        }
	} 
    public function status($id)
    {
        try{
            if (Events::where('id',$id)->exists()) {
                $events = Events::where('id',$id)->first();
                if ($events->status==1) {
                    $events->status = 0;
                } elseif($events->status==0){
                    $events->status = 1;
                }
                $events->save();
                return redirect()->route('admin.events.list')->with('message', 'Event status updated successfully');
            }
        } catch ( Exception $e) {
            Log::error($e);
        }
    }
    public function destroy($id)
	{
		try{
            if (Events::where('id',$id)->exists()) {
                $events_delete = Events::where('id',$id);
                $events_delete->delete();
                return redirect()->route('admin.events.list')->with('message', 'Events delete successfully.');
            }
        }
        catch (Exception $e) {
            log::error($e);
        }
	}
}
