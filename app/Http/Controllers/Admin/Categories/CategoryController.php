<?php

namespace App\Http\Controllers\Admin\Categories;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Front\Categories;
use Validator;
use Auth;
use Log;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        try{
            $data['categories'] = Categories::get();
            return view('admin.categories.list')->with($data);
        }
        catch (Exception $e) {
            log::error($e);
        }
    }
    public function create()
    {
        try{
            $data['category_availabilitys'] = Categories::orderBy('id','desc')->where('subcat_id',null)->where('parent_id',null)->get();
            return view('admin.categories.create')->with($data);
        }
        catch (Exception $e) {
            log::error($e);
        }
    }
    public function store(Request $request)
    {
        try{
            $rules =[
                'name' => 'required|string|max:255',
            ];
            $customeMessage = [
                'name.required' => 'Please enter name.',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            if( $validator->fails() ){
                return back()->withInput()->withErrors($validator->errors());
            } else{ 
                $id = trim(strip_tags($request->id)); 
                $name = trim(strip_tags($request->name));
                $category_availability = trim(strip_tags($request->category_availability));
                $subcat_id = trim(strip_tags($request->subcat_id));
                if($id!=""){
                    $category=Categories::find($id);
                }else{
                    $category=New Categories();
                }
                if ($category_availability!="") {
                    $category->parent_id = $category_availability;
                }
                if ($subcat_id!="") {
                    $category->subcat_id = $subcat_id;
                }
                $category->name = $name;
                $category->save();
            }
            if($request->id!=""){    
                return redirect()->route('admin.categories.index')->with('message', 'Category Update Succesfully.');
            }
            else{
                return redirect()->route('admin.categories.index')->with('message', 'Category Create Succesfully.');
            }
        }
        catch (Exception $e) {
            log::error($e);
        }
    }
    public function edit($id)
    {
        try{
            if(Categories::where('id',$id)->exists()){  
                $data['category_availabilitys'] = Categories::orderBy('id','desc')->where('subcat_id',null)->where('parent_id',null)->get();
                $data['category'] = Categories::where('id',$id)->first();
                $data['parent_id'] = Categories::where('id',$data['category']->parent_id)->first();                
                return view('admin.categories.create')->with($data);    
            }
        }
        catch(\Exception $e){
            $Exceptions = New Exceptions;
            $Exceptions->sendException($e);  
        }
    }
    public function destroy(int $id)
    {
        try{
            if (Categories::where('id',$id)->exists()) {
                $cat_delete = Categories::where('id',$id)->delete();
                return redirect()->route('admin.categories.index')->with('message', 'Category delete successfully.');
            }
        }
        catch (Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function getSubcategory($id){
        try {
            $subcat_id = Categories::where('parent_id',$id)->where('subcat_id',null)->pluck('id','name');
            return $subcat_id;
        } catch (Exception $e) {
            Log::error($e);
        }
    }
}

    
