<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Model\Front\Customer;
use Log;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    /*public function showRegistrationForm()
    {
        return view('auth.admin.register');
    }*/
    /*public function register(Request $request)
    {
        try{
            $rules = [
                'username'    => 'required|max:50',
                'password' => 'max:15|min:6|max:15',
                'name'    => 'required|max:255',
                'surname' => 'required|max:255',
                'phone'=> 'required|numeric|digits_between:0,15',
                'email'=> 'required|email|max:50'
            ];
            $customeMessage = [
                'username.required'    => 'Please enter User name.',
                'name.required'    => 'Please enter Name.',
                'surname.required' => 'Please enter Surname.',
                'email.required'   => 'Please enter Email.',
                'email.email' => 'Please enter valid Email address',
                'email.unique' =>'Email address already in use',
                'password.required'   => 'Please enter Password.',
                'password.min'   => 'Please enter minimum 6 character or number.',
                'password.max'   => 'Please enter maximum 15 character or number.',
                'password.digits_between'   => 'Please enter number between 0 to 15.',
                'password.required'   => 'Please enter Password.',
                'phone.required'   => 'Please enter Phone number.',
                'phone.number' =>'Please enter number',
            ];
            if (isset($request->id) && !empty($request->id)) {
                if (Customer::where('id', $request->id)->first()->email != $request->email) {
                    $this->rules['email'] = 'required|email|unique:customers,email';
                }
                if (Customer::where('id', $request->id)->first()->username != $request->username) {
                    $this->rules['username'] = 'required|unique:customers,username';
                }
            } else {
                $this->rules['email']      = 'required|email|unique:customers,email';
                $this->rules['username']      = 'required|unique:customers,username';
                $this->rules['password'] = 'required|min:6|max:15';
            }
            $Validator = Validator::make($request->all(),$rules,$customeMessage);
            if ($Validator->fails()) {
                return back()->withInput()->withErrors($Validator->errors());
            } else {
                $id = trim(strip_tags($request->id));
                $username = trim(strip_tags($request->username));
                $password = trim(strip_tags($request->password));
                $name = trim(strip_tags($request->name));
                $surname = trim(strip_tags($request->surname));
                $phone = trim(strip_tags($request->phone));
                $email = trim(strip_tags($request->email));
                if($request->id!=""){
                    $customer=Customer::find($request->id);
                }else{
                    $customer=New Customer;
                }
                $customer->username = $username;
                $customer->name = $name;
                $password_hash = Hash::make($password);
                $customer->password = $password_hash;
                $customer->surname = $surname;
                $customer->phone = $phone;
                $customer->email = $email;
                dd($customer);
                $customer->save();
                if ($id) {
                    return redirect()->route('digital-directory')->with('message', 'User Succesfully added');
                } else {
                    return "Not registered";
                }
                return back()->with('error', 'Something wrong please try again latter');
            }
        }
        catch(Exception $e){
            log::error($e);
        }
    }*/
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware('guest');
    }*/

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
   /* protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }*/

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    /*protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }*/
}
