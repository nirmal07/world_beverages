<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Front\Categories;
use App\Model\Front\Products;
use App\Model\Front\News;
use App\Model\Front\Events;
use App\Model\Front\Article;
use DB;
use Log;

class HomeController extends Controller
{
    public function index(Request $request){
        try{
            $news_limit = 3;
            $events_limit = 3;
            $data['categories'] = Categories::where('parent_id',32)->get();
            $data['news_all'] = News::where('status',1)->OrderBy('created_at','desc')->limit(3)->get(); 
            $data['events'] = Events::where('status',1)->OrderBy('created_at','desc')->limit(3)->get();
            $data['articles'] = Article::limit(1)->get();
            /*$data['news_all'] = News::paginate($news_limit,['*'],'news_page');
            $data['events'] = Events::paginate($events_limit,['*'],'event_page');*/
            return view('web.home')->with($data);
        } catch(Exception $e){
            Log::error($e);
        }
    }
    public function search(Request $request){
        try{
            $products_limit = 9;
            $page = trim($request->page);
            $products = DB::table('products')
                                ->select('products.*','categories.name as category_name')
                                ->leftjoin('categories','categories.id','products.category_id');
            if(count((array)$request->all())!=0)
            {
                if (isset($request->product_category) && !empty($request->product_category))
                {
                    $product_category = strip_tags(trim($request->product_category));
                    $products->where(function ($query) use ($product_category) {
                        $query->where('products.category_id', $product_category);
                    });
                }
                if (isset($request->keyword) && !empty($request->keyword))
                {
                    $keyword = strip_tags(trim($request->keyword));
                    $products->where(function ($query) use ($keyword) {
                        $query->where('products.name', 'LIKE', '%' . $keyword . '%');
                    });
                }
                if (isset($request->price_range) && !empty($request->price_range))
                {
                    $price_range = explode(',',$request->price_range);
                    $min_price = $price_range[0];
                    $max_price = $price_range[1];
                    if($min_price!="" && $max_price!=""){
                        $products->whereBetween('products.price_unit_usd', [$min_price, $max_price]);
                    }
                }
            }
            if($page!=""){
                $data['products'] = $products->paginate($products_limit,['*'],'page',$page);
             }else{
                $data['products'] = $products->paginate($products_limit);
            }                               
            $data['categories'] = Categories::where('parent_id','32')->get();
            return view('web.home_search')->with($data);
        } catch(Exception $e){
            Log::error($e);
        }
    }
    
}
