<?php

namespace App\Http\Middleware;

use Closure;
use Request;

class RedirectIfNotEmployee
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param string $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'employee')
    {
        if (!auth()->guard($guard)->check()) {
            if(Request::segment(1) == "admin" ){
            } else{
                $request->session()->flash('error', 'You must be an employee to see this page');
            }
            return redirect(route('admin.login'));
        }

        return $next($request);
    }
}
