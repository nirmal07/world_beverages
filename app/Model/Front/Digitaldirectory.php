<?php

namespace App\Model\Front;

use Illuminate\Database\Eloquent\Model;

class DigitalDirectory extends Model
{
    public $table = "digital_directory";
    protected $primarykey = 'id';
}
