<?php

namespace App\Model\Front;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
	public $table = "seo";
	public $timestamps = false;
}
