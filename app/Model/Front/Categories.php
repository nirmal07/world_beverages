<?php

namespace App\Model\Front;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    public $table = "categories";
    public $timestamps = false;
}
