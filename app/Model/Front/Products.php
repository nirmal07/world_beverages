<?php

namespace App\Model\Front;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    public $table = "products";
}
