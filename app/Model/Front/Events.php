<?php

namespace App\Model\Front;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
	public $table = "events";
	public $timestamps = false;
}
