<?php

namespace App\Model\Front;

use Illuminate\Database\Eloquent\Model;

class Companytype extends Model
{
    public $table = "company_types";
}
