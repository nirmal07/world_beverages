<?php

namespace App\Model\Front;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
	public $table= "news";
	public $timestamps = false;
}
