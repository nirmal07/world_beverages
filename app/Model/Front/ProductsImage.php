<?php

namespace App\Model\Front;

use Illuminate\Database\Eloquent\Model;

class ProductsImage extends Model
{
    public $table = "product_image";
}
