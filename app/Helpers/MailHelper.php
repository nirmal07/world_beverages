<?php
namespace App\Helpers;

use Illuminate\Database\Eloquent\Helper;
use Mail;
use App\Helpers\Exceptions;
use Log;

class MailHelper
{
	public static function sendEmail($data="")
	{
		try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('auth.front.mail',['data'=>$data] , function($message) use ($data) {
			        $message->to($data['email'], $data['name'])->subject('World Beverages');
			        $message->from('shineinfosoft23@gmail.com','beverages');
			    });
			    if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch(Exception $e) {
            // Exceptions::exception($e);
        }
	}
	public static function statusActive($data="")
	{
		try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('auth.admin.userstatus_Active',['data'=>$data] , function($message) use ($data) {
			        $message->to($data['email'], $data['name'])->subject('Beverages Employer Account Active');
			        $message->from('shineinfosoft23@gmail.com','Beverages');
			    });
			    if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch(Exception $e) {
            Log::debug($e);
        }
	}
	public static function statusInActive($data="")
	{
		try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('auth.admin.userstatus_InActive',['data'=>$data] , function($message) use ($data) {
			        $message->to($data['email'], $data['name'])->subject('Beverages Employer Account InActive');
			        $message->from('shineinfosoft23@gmail.com','Beverages');
			    });
			    if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch(Exception $e) {
            Log::debug($e);
        }
	}
}
?>