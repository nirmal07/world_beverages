@extends('layouts.admin.layout')

@section('content')
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Dashboard</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                @foreach($breadcumbs as $breadcumb)
                    @if($loop->last)
                        <li><a href="javascript:;" class="active">{{$breadcumb["name"]}}</a></li>
                    @else
                      <li><a href="{{ $breadcumb['url'] }}">{{$breadcumb["name"]}}</a></li>
                    @endif
                @endforeach
            </ol>
        </div>
    </div>
</div>
@endsection
