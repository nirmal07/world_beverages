@extends('layouts.admin.layout')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage Digital Directory</h4> </div>
		</div>
		@include('layouts.errors-and-messages')
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-listing">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							<li><a href="{{ route('admin.digitaldirectory.create') }}">Add Digital Directory</a></li>
							<li class="active"><a>Digital Directory Listing</a></li>
					  	</ul>
					  
					  	<div class="tab-content">
							<div id="ProductListing" class="tab-pane fade in active">
								<div class="product-listing">
									<div class="table-responsive">
										<table id="events" class="events table border-b1px" style="width:100%">
											<thead>
												<tr>
													<th>Creation Date</th>
													<th>Edition Date</th>
													<th>Name</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
												@forelse ($digitaldirectorys as $digitaldirectory)
												<tr>
													<td>{{ date('d-m-Y',strtotime($digitaldirectory->created_at)) }}</td>
													<td>{{ date('d-m-Y',strtotime($digitaldirectory->updated_at)) }}</td>
													<td>{{$digitaldirectory->name}}</td>
													<td>

														<div class="action-btn">
															<form action="{{ route('admin.digitaldirectory.destroy', $digitaldirectory->id) }}" method="get" class="form-horizontal">
																{{ csrf_field() }}
																<input type="hidden" name="_method" value="delete">
																<div class="btn-group">
																	<a href="{{ route('admin.digitaldirectory.edit', $digitaldirectory->id) }}" class="btn btn-edit"><i class="mdi mdi-pencil"></i></a>
																	<button onclick="return confirm('Are you sure you want to delete?')" type="submit" class="btn btn-delete"><i class="mdi mdi-close"></i></button>
																</div>
															</form>
														</div>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
<style type="text/css">
	.btn-info, .btn-info.disabled {
        color: black!important;
        background: #e4e7ea!important;
    }
    .btn-info:hover{
        border:1px solid!important;
        border-color: none!important;
    }
</style>
@push('js')
<script>
    $(document).ready(function() {
        init_datatable();
    } );
    
    function init_datatable(){
        $('#events').DataTable().destroy();
        setTimeout(function(){
            var table = $('#events').DataTable({
                deferRender: true,
                scrollX: true
            });
        },300);
    }
</script>
@endpush
