@extends('layouts.admin.layout')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage Digital Directory</h4> 
			</div>
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-tab">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							@if(isset($digitaldirectory->id))
								<li class="active"><a>Edit Digital Directory</a></li>
							@else
								<li class="active"><a>Add Digital Directory</a></li>
							@endif
							<li><a href="{{ route('admin.digitaldirectory.list') }}">Digital Directory Listing</a></li>
					  	</ul>
					  	<div class="tab-content">
							<div id="AddProcucts" class="tab-pane fade in active">
								<div class="add-product">
								   	<form id="create_product" action="{{ route('admin.digitaldirectory.store') }}" method="post" class="form" enctype="multipart/form-data">
									{{ csrf_field() }}
									@if(isset($digitaldirectory->id))
				            			<input type="hidden" name="id" value="{{$digitaldirectory->id}}">
				            			<input type="hidden" name="seo_id" value="{{$digitaldirectory->seo_id}}">
		            				@else
				            			<input type="hidden" name="id" value="">
			            			@endif
			            				<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Digital Directory request</label>
													<select name="directory_request" id="directory_request" tabindex="1" class="form-control" style="width: 100%; height:36px;">
														<option value="0"@if(old('directory_request')=="0") selected="true" @elseif(isset($digitaldirectory->directory_request) && $digitaldirectory->directory_request=="0") selected="true" @endif>No</option>
														<option value="1"@if(old('directory_request')=="1") selected="true" @elseif(isset($digitaldirectory->directory_request) && $digitaldirectory->directory_request=="1") selected="true" @endif>Yes</option>
													</select>
													@if($errors->has('directory_request'))
		                                      			<span class="text-danger">{{$errors->first('directory_request')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Digital Directory approval</label>
													<select name="directory_approve" class="form-control" tabindex="2">
														<option value="1"@if(old('directory_approve')=="1") selected="true" @elseif(isset($digitaldirectory->directory_approve) && $digitaldirectory->directory_approve=="1") selected="true" @endif>Yes</option>
														<option value="0"@if(old('directory_approve')=="0") selected="true" @elseif(isset($digitaldirectory->directory_approve) && $digitaldirectory->directory_approve=="0") selected="true" @endif>No</option>
													</select>
													@if($errors->has('directory_approve'))">
		                                      			<span class="text-danger">{{$errors->first('directory_approve')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Digital Directory featured</label>
													<select name="directory_featured" id="directory_featured" tabindex="3" class="form-control" style="width: 100%; height:36px;">
														<option value="1"@if(old('directory_featured')=="1") selected="true" @elseif(isset($digitaldirectory->directory_featured) && $digitaldirectory->directory_featured=="1") selected="true" @endif>Yes</option>
														<option value="0"@if(old('directory_featured')=="0") selected="true" @elseif(isset($digitaldirectory->directory_featured) && $digitaldirectory->directory_featured=="0") selected="true" @endif>No</option>
													</select>
													@if($errors->has('directory_featured'))
		                                      			<span class="text-danger">{{$errors->first('directory_featured')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Consultant Listing request</label>
													<select name="consultant_listing_request" class="form-control" tabindex="4">
														<option value="0"@if(old('consultant_listing_request')=="0") selected="true" @elseif(isset($digitaldirectory->consultant_listing_request) && $digitaldirectory->consultant_listing_request=="0") selected="true" @endif>No</option>
														<option value="1"@if(old('consultant_listing_request')=="1") selected="true" @elseif(isset($digitaldirectory->consultant_listing_request) && $digitaldirectory->consultant_listing_request=="1") selected="true" @endif>Yes</option>
													</select>
													@if($errors->has('consultant_listing_request'))">
		                                      			<span class="text-danger">{{$errors->first('consultant_listing_request')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Consultant Listing approval </label>
													<select name="consultant_listing_approve" id="consultant_listing_approve" tabindex="5" class="form-control" style="width: 100%; height:36px;">
														<option value="0"@if(old('consultant_listing_approve')=="0") selected="true" @elseif(isset($digitaldirectory->consultant_listing_approve) && $digitaldirectory->consultant_listing_approve=="0") selected="true" @endif>No</option>
														<option value="1"@if(old('consultant_listing_approve')=="1") selected="true" @elseif(isset($digitaldirectory->consultant_listing_approve) && $digitaldirectory->consultant_listing_approve=="1") selected="true" @endif>Yes</option>
													</select>
													@if($errors->has('consultant_listing_approve'))
		                                      			<span class="text-danger">{{$errors->first('consultant_listing_approve')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Listing Priority</label>
													<select name="listing_priority" class="form-control"tabindex="6">
														<option value="1"@if(old('listing_priority')=="1") selected="true" @elseif(isset($digitaldirectory->listing_priority) && $digitaldirectory->listing_priority=="1") selected="true" @endif>Normal</option>
														<option value="2"@if(old('listing_priority')=="2") selected="true" @elseif(isset($digitaldirectory->listing_priority) && $digitaldirectory->listing_priority=="2") selected="true" @endif>High</option>
														<option value="3"@if(old('listing_priority')=="3") selected="true" @elseif(isset($digitaldirectory->listing_priority) && $digitaldirectory->listing_priority=="3") selected="true" @endif>Medium</option>
													</select>
													@if($errors->has('listing_priority'))">
		                                      			<span class="text-danger">{{$errors->first('listing_priority')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
			            				<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Company Name<span class="red">*</span></label>
													<input type="text" tabindex="7" name="company_name" class="form-control" placeholder="Company Name" maxlength="500" value="@if(isset($digitaldirectory->name)){{ $digitaldirectory->name }}@elseif(old('company_name')!=''){{ old('company_name') }} @endif">
													@if($errors->has('company_name'))
		                                      			<span class="text-danger">{{$errors->first('company_name')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Main Activity/Category <span class="red">*</span></label>
													<select name="category_id" id="category_id" tabindex="8" class="form-control">
														<option value="">Select Category</option>
														@foreach($categories as $data)
															<option value="{{ $data->id }}"
															@if(old('category_id') == $data->id)
	                                						selected="true"
	                                						@endif
					                                        @if(isset($digitaldirectory->company_type) && $data->id== $digitaldirectory->company_type))
					                                            selected="true"
					                                        @endif
					                                        >{{ $data->name }}</option>
					                                    @endforeach
													</select>
													@if($errors->has('category_id'))
		                                      			<span class="text-danger">{{$errors->first('category_id')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Sub Activity/Category</label>
													<select name="sub_cat" class="form-control" tabindex="9" id="sub_cat">
														<option value="">Select Sub Activity/Category</option>
													</select>
													<input type="hidden" name="selected_subcat" id="selected_subcat" value="@if(isset($digitaldirectory->sub_cat)){{ $digitaldirectory->sub_cat }}@elseif(old('selected_subcat')!=''){{ old('selected_subcat') }} @endif">
													@if($errors->has('sub_cat'))">
		                                      			<span class="text-danger">{{$errors->first('sub_cat')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Company GEO Location <span class="red">*</span></label>
													<div id="locationField">
												        <input id="autocomplete" placeholder="Enter your address" name="company_location" tabindex="10" class="form-control" onFocus="geolocate()" type="text" value="@if(isset($digitaldirectory->company_location)){{ $digitaldirectory->company_location }}@elseif(old('company_location')!=''){{ old('company_location') }} @endif"></input>
														@if($errors->has('company_location'))
			                                      			<span class="text-danger">{{$errors->first('company_location')}}</span>
			                                    		@endif
												    </div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-sm-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Country</label>
													<input class="field form-control" name="country" id="country" tabindex="11" placeholder="Country" value="@if(isset($digitaldirectory->country)){{ $digitaldirectory->country }}@elseif(old('country')!=''){{ old('country') }} @endif">
												</div>
											</div>
											<div class="col-sm-4 col-sm-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>State</label>
													<td class="slimField"><input class="field form-control" name="state" tabindex="12" id="administrative_area_level_1" placeholder="State" value="@if(isset($digitaldirectory->state)){{ $digitaldirectory->state }}@elseif(old('company_email')!=''){{ old('company_email') }} @endif"></input></td>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>City</label>
													<td class="wideField" colspan="3"><input class="field form-control" tabindex="13" name="city" id="locality" placeholder="City" value="@if(isset($digitaldirectory->city)){{ $digitaldirectory->city }}@elseif(old('city')!=''){{ old('city') }} @endif"></input></td>
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Zip Code</label>
													<td class="wideField"><input class="field form-control" id="postal_code" tabindex="14" name="zip_code" placeholder="Zip Code" value="@if(isset($digitaldirectory->zip_code)){{ $digitaldirectory->zip_code }}@elseif(old('zip_code')!=''){{ old('zip_code') }} @endif"></input></td>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Company Phone <span class="red">*</span></label>
													<input type="text" class="number form-control" name="phone" placeholder="Company Phone" maxlength="15" tabindex="15" value="@if(isset($digitaldirectory->phone)){{ $digitaldirectory->phone }}@elseif(old('phone')!=''){{ old('phone') }} @endif">
													@if($errors->has('phone'))
		                                      			<span class="text-danger">{{$errors->first('phone')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Company Fax</label>
													<input type="text" name="fax"  class="form-control" placeholder="Company Fax" maxlength="20" tabindex="16" value="@if(isset($digitaldirectory->fax)){{ $digitaldirectory->fax }}@elseif(old('fax')!=''){{ old('fax') }} @endif">
													@if($errors->has('fax'))
		                                      			<span class="text-danger">{{$errors->first('fax')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Company contact person name<span class="red">*</span></label>
													<input type="text" class=" form-control" name="contact_person_name" placeholder="Company Contact Person Name" tabindex="17" maxlength="100" value="@if(isset($digitaldirectory->contact_person_name)){{ $digitaldirectory->contact_person_name }}@elseif(old('contact_person_name')!=''){{ old('contact_person_name') }} @endif">
													@if($errors->has('contact_person_name'))
		                                      			<span class="text-danger">{{$errors->first('contact_person_name')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Company contact person title <span class="red">*</span></label>
													<input type="text" name="contact_person_title"  class="form-control" placeholder="Company Contact Person Title" maxlength="100" tabindex="18" value="@if(isset($digitaldirectory->contact_person_title)){{ $digitaldirectory->contact_person_title }}@elseif(old('contact_person_title')!=''){{ old('contact_person_title') }} @endif">
													@if($errors->has('contact_person_title'))
		                                      			<span class="text-danger">{{$errors->first('contact_person_title')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Company contact email <span class="red">*</span></label>
													<input type="text" class=" form-control" name="email" placeholder="Company Contact Email" maxlength="50" tabindex="19" value="@if(isset($digitaldirectory->email)){{ $digitaldirectory->email }}@elseif(old('email')!=''){{ old('email') }} @endif">
													@if($errors->has('email'))
		                                      			<span class="text-danger">{{$errors->first('email')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Company Website</label>
													<input type="text" name="com_website"  class="form-control" placeholder="Company Website" tabindex="20" maxlength="200" value="@if(isset($digitaldirectory->com_website)){{ $digitaldirectory->com_website }}@elseif(old('com_website')!=''){{ old('com_website') }} @endif">
													@if($errors->has('com_website'))
		                                      			<span class="text-danger">{{$errors->first('com_website')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Company Twitter URL</label>
													<input type="text" name="twitter_url" placeholder="Twitter URL" class="form-control" tabindex="21" maxlength="100" value="@if(isset($digitaldirectory->twitter_url)){{ $digitaldirectory->twitter_url }}@elseif(old('twitter_url')!=''){{ old('twitter_url') }} @endif">
													@if($errors->has('twitter_url'))
		                                      			<span class="text-danger">{{$errors->first('twitter_url')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Company Facebook URL</label>
													<input type="text" name="facebook_url" placeholder="Facebook URL" class="form-control" tabindex="22" maxlength="100" value="@if(isset($digitaldirectory->facebook_url)){{ $digitaldirectory->facebook_url }}@elseif(old('facebook_url')!=''){{ old('facebook_url') }} @endif">
													@if($errors->has('facebook_url'))
		                                      			<span class="text-danger">{{$errors->first('facebook_url')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Company LinkedIN URL</label>
													<input type="text" name="linkedin_url" placeholder="LinkedIN URL" tabindex="23" class="form-control" maxlength="100" value="@if(isset($digitaldirectory->linkedin_url)){{ $digitaldirectory->linkedin_url }}@elseif(old('linkedin_url')!=''){{ old('linkedin_url') }} @endif">
													@if($errors->has('linkedin_url'))
		                                      			<span class="text-danger">{{$errors->first('linkedin_url')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Annual Case Production</label>
													<input type="text" name="annual_case_production" placeholder="Annual Case Production" tabindex="24" class="form-control" maxlength="200" value="@if(isset($digitaldirectory->annual_case_production)){{ $digitaldirectory->annual_case_production }}@elseif(old('annual_case_production')!=''){{ old('annual_case_production') }} @endif">
													@if($errors->has('annual_case_production'))
		                                      			<span class="text-danger">{{$errors->first('annual_case_production')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Avg. Bottle Price</label>
													<input type="text" name="avg_bottle_price" tabindex="25" placeholder="Avg. Bottle Price" class="form-control" maxlength="200" value="@if(isset($digitaldirectory->avg_bottle_price)){{ $digitaldirectory->avg_bottle_price }}@elseif(old('avg_bottle_price')!=''){{ old('avg_bottle_price') }} @endif">
													@if($errors->has('avg_bottle_price'))
		                                      			<span class="text-danger">{{$errors->first('avg_bottle_price')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										@php
											if(isset($seo->page_keywords)){
												$page_keywords = explode(',',$seo->page_keywords);
											}
											if(isset($digitaldirectory->varietals_produced)){
												$varietals_produced = explode(',',$digitaldirectory->varietals_produced);
											}
											if(isset($digitaldirectory->primary_appellations)){
												$primary_appellations = explode(',',$digitaldirectory->primary_appellations);
											}
											
										@endphp
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Varietals Produced</label>
													<select name="varietals_produced[]" id="varietals_produced" tabindex="26" class="form-control select2" multiple="multiple">
														@if(isset($varietals_produced))
															@foreach($varietals_produced as $key => $value)
															<option
																 selected= "selected" value="{{ $value }}"
															>{{$value}}</option>
															@endforeach
														@else
															<option></option>	
														@endif
													</select>
													@if($errors->has('varietals_produced'))
		                                      			<span class="text-danger">{{$errors->first('varietals_produced')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Primary Appellations</label>
													<select name="primary_appellations[]" id="primary_appellations" tabindex="27" class="form-control select2" multiple="multiple">
														@if(isset($primary_appellations))
															@foreach($primary_appellations as $key => $value)
															<option
																 selected= "selected" value="{{ $value }}"
															>{{$value}}</option>
															@endforeach
														@else
															<option></option>	
														@endif
													</select>
													@if($errors->has('primary_appellations'))
		                                      			<span class="text-danger">{{$errors->first('primary_appellations')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Vineyard Acres</label>
													<input type="text" name="vineyard_acres" class="form-control" tabindex="28" placeholder="Vineyard Acres" value="@if(isset($digitaldirectory->vineyard_acres)){{ $digitaldirectory->vineyard_acres }}@elseif(old('vineyard_acres')!=''){{ old('vineyard_acres') }} @endif">
													@if($errors->has('vineyard_acres'))
		                                      			<span class="text-danger">{{$errors->first('vineyard_acres')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Grows Grapes</label>
													<select name="grows_grapes" class="form-control" tabindex="29">
														<option value="0" @if(old('grows_grapes')=="0") selected="true" @elseif(isset($digitaldirectory->grows_grapes) && $digitaldirectory->grows_grapes=="0") selected="true" @endif>No</option>
														<option value="1" @if(old('grows_grapes')=="1") selected="true" @elseif(isset($digitaldirectory->grows_grapes) && $digitaldirectory->grows_grapes=="1") selected="true" @endif>Yes</option>
													</select>
													@if($errors->has('grows_grapes'))
		                                      			<span class="text-danger">{{$errors->first('grows_grapes')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Sells Grapes</label>
													<select name="sells_grapes" class="form-control" tabindex="30" name="custom_crush">
														<option value="0" @if(old('sells_grapes')=="0") selected="true" @elseif(isset($digitaldirectory->sells_grapes) && $digitaldirectory->sells_grapes=="0") selected="true" @endif>No</option>
														<option value="1" @if(old('sells_grapes')=="1") selected="true" @elseif(isset($digitaldirectory->sells_grapes) && $digitaldirectory->sells_grapes=="1") selected="true" @endif>Yes</option>
													</select>
													@if($errors->has('sells_grapes'))
		                                      			<span class="text-danger">{{$errors->first('sells_grapes')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Custom Crush</label>
													<select name="custom_crush" class="form-control" tabindex="31">
														<option value="0" @if(old('custom_crush')=="0") selected="true" @elseif(isset($digitaldirectory->custom_crush) && $digitaldirectory->custom_crush=="0") selected="true" @endif>No</option>
														<option value="1" @if(old('custom_crush')=="1") selected="true" @elseif(isset($digitaldirectory->custom_crush) && $digitaldirectory->custom_crush=="1") selected="true" @endif>Yes</option>
													</select>
													@if($errors->has('custom_crush'))
		                                      			<span class="text-danger">{{$errors->first('custom_crush')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Aternating Propietorship</label>
													<select class="form-control" name="aternating_propietorship" tabindex="32">
														<option value="0" @if(old('aternating_propietorship')=="0") selected="true" @elseif(isset($digitaldirectory->aternating_propietorship) && $digitaldirectory->aternating_propietorship=="0") selected="true" @endif>No</option>
														<option value="1" @if(old('aternating_propietorship')=="1") selected="true" @elseif(isset($digitaldirectory->aternating_propietorship) && $digitaldirectory->aternating_propietorship=="1") selected="true" @endif>Yes</option>
													</select>
													@if($errors->has('aternating_propietorship'))
		                                      			<span class="text-danger">{{$errors->first('aternating_propietorship')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Featured Broker</label>
													<select name="featured_broker" class="form-control" tabindex="33">
														<option value="false" @if(old('featured_broker')=="false") selected="true" @elseif(isset($digitaldirectory->featured_broker) && $digitaldirectory->featured_broker=="false") selected="true" @endif >This Company is NOT listed as a pro Broker
														</option>
														<option value="true" @if(old('featured_broker')=="true") selected="true" @elseif(isset($digitaldirectory->featured_broker) && $digitaldirectory->featured_broker=="true") selected="true" @endif>This Company IS listed as a pro Broker
														</option>
													</select>
													@if($errors->has('featured_broker'))
		                                      			<span class="text-danger">{{$errors->first('featured_broker')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Is Pro</label>
													<select name="is_pro" class="form-control" tabindex="34">
														<option value="0" @if(old('is_pro')=="0") selected="true" @elseif(isset($digitaldirectory->is_pro) && $digitaldirectory->is_pro=="0") selected="true" @endif>No</option>
														<option value="1" @if(old('is_pro')=="1") selected="true" @elseif(isset($digitaldirectory->is_pro) && $digitaldirectory->is_pro=="1") selected="true" @endif>Yes</option>
													</select>
													@if($errors->has('is_pro'))
		                                      			<span class="text-danger">{{$errors->first('is_pro')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										@php
											if(isset($digitaldirectory->company_skills)){
												$company_skills = explode(',',$digitaldirectory->company_skills);
											}
											if(isset($digitaldirectory->currentProductLine)){
												$currentProductLine = explode(',',$digitaldirectory->currentProductLine);
											}
											if(isset($digitaldirectory->productlooking)){
												$productlooking = explode(',',$digitaldirectory->productlooking);
											}
											
										@endphp
											
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Company Skills</label>
													<br>
													<input type="checkbox" name="company_skills[]" tabindex="35" class="" value="distributor_sales"
													@if(isset($company_skills))
													@if (in_array("distributor_sales", $company_skills))
													checked="" 
													@endif
													@endif> Distributor Sales
													<br>
													<input type="checkbox" name="company_skills[]" value="retail_sales"
													@if(isset($company_skills))
											 		@if (in_array("retail_sales", $company_skills))
													checked="" 
													@endif
													@endif>Retail Sales
													<br>
													<input type="checkbox" name="company_skills[]" value="national_chain_account_sales"
													@if(isset($company_skills))
													@if (in_array("national_chain_account_sales", $company_skills))
													checked="" 
													@endif
													@endif>National Chain Account Sales
													<br>
													<input type="checkbox" name="company_skills[]" value="brand_development_and_consulting" 
													@if(isset($company_skills))
													@if (in_array("brand_development_and_consulting", $company_skills))
													checked="" 
													@endif
													@endif
													> Brand Development and Consulting
													<br>
													@if($errors->has('company_skills'))
		                                      			<span class="text-danger">{{$errors->first('company_skills')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Company Experience</label>
													<br>
													<input type="radio" name="company_experience" tabindex="36" value="0-2"
													@if(old('company_experience')=="0-2") checked="" @elseif(isset($digitaldirectory->company_experience) && $digitaldirectory->company_experience=="0-2") checked @endif>0-2 Years
													<br>
													<input type="radio" name="company_experience" value="3-5"@if(old('company_experience')=="3-5") checked="" @elseif(isset($digitaldirectory->company_experience) && $digitaldirectory->company_experience=="3-5") checked @endif>3-5 Years
													<br>
													<input type="radio" name="company_experience" value="6-10"@if(old('company_experience')=="6-10") checked="" @elseif(isset($digitaldirectory->company_experience) && $digitaldirectory->company_experience=="6-10") checked @endif>6-10 Years
													<br>
													<input type="radio" name="company_experience" value="11-20"@if(old('company_experience')=="11-20") checked="" @elseif(isset($digitaldirectory->company_experience) && $digitaldirectory->company_experience=="11-20") checked @endif>11-20 Years
													<br>
													<input type="radio" name="company_experience" value="20+"@if(old('company_experience')=="20+") checked="" @elseif(isset($digitaldirectory->company_experience) && $digitaldirectory->company_experience=="20+") checked @endif>20+ Years
													@if($errors->has('company_experience'))
		                                      			<span class="text-danger">{{$errors->first('company_experience')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Current Product Line</label>
													<br>
													<input type="checkbox" name="currentProductLine[]" tabindex="37" class="" value="wine"
													@if(isset($currentProductLine))
														@if (in_array("wine", $currentProductLine))
														checked="" 
														@endif
													@endif>Wine
													<br>
													<input type="checkbox" name="currentProductLine[]" value="spirits"
													@if(isset($currentProductLine))
													@if (in_array("spirits", $currentProductLine))
													checked="" 
													@endif
													@endif>Spirits 
													<br>
													<input type="checkbox" name="currentProductLine[]" value="beverages"
													@if(isset($currentProductLine))
													@if (in_array("beverages", $currentProductLine))
													checked="" 
													@endif
													@endif>Beverages
													<br>
													<input type="checkbox" name="currentProductLine[]" value="non_alc_drinks"
													@if(isset($currentProductLine))
													@if (in_array("non_alc_drinks", $currentProductLine))
													checked="" 
													@endif
													@endif>Non Alc Drinks
													<br>
													<input type="checkbox" name="currentProductLine[]" value="non_alc_drinks"
													@if(isset($currentProductLine))
													@if (in_array("non_alc_drinks", $currentProductLine))
													checked="" 
													@endif
													@endif>Non Alc Drinks
													<br>
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Looking for</label>
													<br>
													<input type="checkbox" name="productlooking[]" tabindex="38" class="" value="wine"
													@if(isset($productlooking))
													@if (in_array("wine", $productlooking))
													checked="" 
													@endif
													@endif>Wine
													<br>
													<input type="checkbox" name="productlooking[]" value="spirits"
													@if(isset($productlooking))
													@if (in_array("spirits", $productlooking))
													checked="" 
													@endif
													@endif>Spirits 
													<br>
													<input type="checkbox" name="productlooking[]" value="beverages"
													@if(isset($productlooking))
													@if (in_array("beverages", $productlooking))
													checked="" 
													@endif
													@endif>Beverages
													<br>
													<input type="checkbox" name="productlooking[]" value="non_alc_drinks"
													@if(isset($productlooking))
													@if (in_array("non_alc_drinks", $productlooking))
													checked="" 
													@endif
													@endif>Non Alc Drinks
													<br>
													<input type="checkbox" name="productlooking[]" value="non_alc_drinks"
													@if(isset($productlooking))
													@if (in_array("non_alc_drinks", $productlooking))
													checked="" 
													@endif
													@endif>Non Alc Drinks
													<br>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Countries covered</label>
													<select name="country_coverd" multiple="" class="form-control" tabindex="39">
														@foreach($countries as $country)
														<option value="{{ $country->id }}" @if(old('country_coverd')==$country->id) selected="true" @elseif(isset($digitaldirectory->country_coverd) && $digitaldirectory->country_coverd==$country->id) selected="true" @endif>{{ $country->country }}</option>
														@endforeach
													</select>
													@if($errors->has('country_coverd'))
		                                      			<span class="text-danger">{{$errors->first('country_coverd')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>US States covered</label>
													<select name="us_state_covered" multiple="" class="form-control" tabindex="40">
														@foreach($us_state as $state)
														
														<option value="{{ $state->state_id }}" @if(old('us_state_covered')==$state->id) selected="true" @elseif(isset($digitaldirectory->us_state_covered) && $digitaldirectory->us_state_covered==$state->id) selected="true" @endif>{{ $state->state }}</option>
														@endforeach
													</select>
													@if($errors->has('country_coverd'))
		                                      			<span class="text-danger">{{$errors->first('country_coverd')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Compensation method</label>
													<select name="compensation_method" class="form-control" tabindex="41">
														<option value="commision_based" @if(old('compensation_method')=="commision_based") selected="true" @elseif(isset($digitaldirectory->compensation_method) && $digitaldirectory->compensation_method=="commision_based") selected="true" @endif>Commision Based</option>
														<option value="base_salary"@if(old('compensation_method')=="base_salary") selected="true" @elseif(isset($digitaldirectory->compensation_method) && $digitaldirectory->compensation_method=="base_salary") selected="true" @endif>Base Salary</option>
														<option value="base_commission"@if(old('compensation_method')=="base_commission") selected="true" @elseif(isset($digitaldirectory->compensation_method) && $digitaldirectory->compensation_method=="base_commission") selected="true" @endif>Base + Commission</option>
													</select>
													@if($errors->has('compensation_method'))
		                                      			<span class="text-danger">{{$errors->first('compensation_method')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>SEO page title</label>
													<input type="text" tabindex="42" name="seo_title" class="form-control" placeholder="SEO page title" value="@if(isset($seo->page_title)){{ $seo->page_title }}@elseif(old('seo_title')!=''){{ old('seo_title') }} @endif">
													@if($errors->has('seo_title'))
		                                      			<span class="text-danger">{{$errors->first('seo_title')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>SEO page description</label>
													<textarea name="seo_page_description" placeholder="SEO page description" tabindex="43" class="form-control">@if(isset($seo->page_description)){{$seo->page_description}}@elseif(old('seo_page_description')!=''){{old('seo_page_description')}} @endif</textarea>
													@if($errors->has('seo_page_description'))
		                                      			<span class="text-danger">{{$errors->first('seo_page_description')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>SEO page keywords</label>
													<select name="seo_page_keywords[]" id="seo_page_keywords" tabindex="44" class="form-control select2" multiple="multiple">
														@if(isset($page_keywords))
															@foreach($page_keywords as $key => $value)
															<option
																 selected= "selected" value="{{ $value }}"
															>{{$value}}</option>
															@endforeach
														@else
															<option></option>	
														@endif
													</select>
													@if($errors->has('seo_page_keywords'))
		                                      			<span class="text-danger">{{$errors->first('seo_page_keywords')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Company short description</label>
													<textarea name="short_description" tabindex="45" placeholder="Company Short Description" class="form-control">@if(isset($digitaldirectory->short_description)){{$digitaldirectory->short_description}}@elseif(old('short_description')!=''){{old('short_description')}} @endif</textarea>
													@if($errors->has('short_description'))
		                                      			<span class="text-danger">{{$errors->first('short_description')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Company full description<span class="red">*</span></label>
													<textarea class="form-control" tabindex="46" id="content1" name="description">
														@if(isset($digitaldirectory->description)){{$digitaldirectory->description}}@elseif(old('description')!=''){{old('description')}} @endif
					                           		</textarea>
													@if($errors->has('description'))
		                                      			<span class="text-danger">{{$errors->first('description')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="file-upload">
													<div class="file-select">
														<div class="file-select-button" id="fileName">Company Logo<span class="red">*</span></div>
														<div class="file-select-name" id="noFile">No file chosen...</div>
														<input tabindex="47" type="file" name="image" id="image" accept="image/*" value="" >
													</div>
												</div>
												@if($errors->has('logo'))
	                                      			<span class="text-danger">{{$errors->first('logo')}}</span>
	                                    		@endif
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<div id="image_beside">
														@if(isset($digitaldirectory->logo))
														<img src="{{url($digitaldirectory->logo)}}" height="300px" class="image" id="edit_image" width="300px">
														@endif
													</div>
												</div>
											</div>
										</div>
										
										<div class="row" style="margin-top: 10px">
											<div class="col-sm-12">
												<div class="submit-btn">
													<button type="submit" tabindex="48" class="btn btn-submit">Submit</button>
												</div>
											</div>
										</div>
								 	</form>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@push('js')
<script type="text/javascript" src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script type="text/javascript" src="{{url('js/custom/custom.js')}}"></script>
<script type="text/javascript">
	var placeSearch, autocomplete;
    var componentForm = {
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };
    $('#start_date').datepicker({
        autoclose: true,  
        format: "dd-mm-yyyy",
        todayHighlight: true,

    });
    $('#end_date').datepicker({
        autoclose: true,  
        format: "dd-mm-yyyy",
        todayHighlight: true,

    });
    $("#category_id").on('change',function($e){
    	id = $("#category_id").val();
		getSubdirectory(id);
    });
    

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */
            (document.getElementById('autocomplete')), { types: ['geocode'] });

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
            document.getElementById(component).value = '';
            
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }
 	function getSubdirectory(id){
		$.ajax({
	        type : "GET",
	        url : "{{ url('admin/digitaldirectory/subcat/') }}/"+id,
	        success : function(response){
	        	$('#sub_cat').empty();
	        	selected_subcat = $("#selected_subcat").val();
	            $('#sub_cat').append('<option value="">None</option>');
	            $.each(response, function(key, value){
	                if(selected_subcat==value){
						$('#sub_cat').append('<option value="'+ value +'" selected="true">' + key + '</option>');
	                }else{
                    	$('#sub_cat').append('<option value="'+ value +'">' + key + '</option>');
                	}

	            });
	        },
	        error: function(error){
        		console.log(error);
        	}
	    });
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfDJaVdwAS-KTBw3WIyMPV2CcrTJAUKFI&libraries=places&callback=initAutocomplete" async defer></script>
@endpush