@extends('layouts.admin.layout')
@section('title','Product-create')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage Products</h4> </div>
			<!-- <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="#">Manage Products</a></li>
					<li class="active">Add Products</li>
				</ol>
			</div> -->
			<!-- /.col-lg-12 -->
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-tab">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							@if(isset($products->id))
								<li class="active"><a>Edit Product</a></li>
							@else
								<li class="active"><a>Add Product</a></li>
							@endif
							<li><a href="{{ route('admin.products.index') }}">Product Listing</a></li>
					  	</ul>

					  	<div class="tab-content">
							<div id="AddProcucts" class="tab-pane fade in active">
								<div class="add-product">
								   	<form id="create_product" action="{{ route('admin.products.store') }}" method="POST" class="form" enctype="multipart/form-data">
									{{ csrf_field() }}
									@if(isset($product->id))
				            			<input type="hidden" name="id" value="{{$product->id}}">
		            				@else
				            			<input type="hidden" name="id" value="">
			            			@endif

										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Owner User ID<span class="red">*</span></label>
													<!--  -->
													<select name="user" id="user" class="form-control" tabindex="1" style="width: 100%; height:36px;">
														<option value="">Select UserID</option>
														@foreach($users as $user)
															<option value="{{ $user->id }}"
															@if(old('user') == $user->id)
	                                						selected="true"
	                                						@endif
					                                        @if(isset($product->user) && $user->id== $product->user))
					                                            selected="true"
					                                        @endif
					                                        
					                                        >{{ $user->username }}</option>
					                                    @endforeach
													</select>
													@if($errors->has('user'))
		                                      			<span class="text-danger">{{$errors->first('user')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Owner Company ID<span class="red">*</span></label>
													<select name="company_id" class="form-control" tabindex="2">
														<option value="">Select Company</option>
														@foreach($companys as $company)
															<option value="{{ $company->id }}"
															@if(old('company_id') == $company->id)
	                                						selected="true"
	                                						@endif
					                                        @if(isset($product->company_id) && $company->id== $product->company_id))
					                                            selected="true"
					                                        @endif
					                                        >{{ $company->name }}</option>
														@endforeach
													</select>
													@if($errors->has('company_id'))
                                      					<span class="text-danger">{{$errors->first('company_id')}}</span>
                                    				@endif
												</div>
											</div>
										</div>
										@php
											$today_date = date('d-m-Y');
										@endphp
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Creation Date</label>
													<span> (dd/mm/yyyy)</span>
													<input type="text" name="create_date" tabindex="3" class="form-control" placeholder="dd-mm-yyyy" id="create_date" readonly="" value="@if(isset($product->created_at)){{date('d-m-Y',strtotime($product->created_at))}}@else{{$today_date}}@endif">
													@if($errors->has('create_date'))
		                                      			<span class="text-danger">{{$errors->first('create_date')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Edition Date </label>
													<span> (dd/mm/yyyy)</span>
													<input type="text" class="form-control" tabindex="4" name="edit_date" placeholder="dd-mm-yyyy" id="edit_date" readonly="" value="{{$today_date}}">
													@if($errors->has('edit_date'))
                                      					<span class="text-danger">{{$errors->first('edit_date')}}</span>
                                    				@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Status</label>
													<select name="status" id="status" tabindex="5" class="form-control" style="width: 100%; height:36px;">
														<option value="1"@if(old('status')=="0") selected="true" @elseif(isset($product->status) && $product->status=="0") selected="true" @endif>Approve</option>
														<option value="0" @if(old('status')=="1") selected="true" @elseif(isset($product->status) && $product->status=="1") selected="true" @endif>Non Approve</option>
													</select>
													@if($errors->has('status'))
		                                      			<span class="text-danger">{{$errors->first('status')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Listing Priority</label>
													<select name="priority" tabindex="6" class="form-control">
														<option value="1" @if(old('status')=="1") selected="true" @elseif(isset($product->priority) && $product->priority=="1") selected="true" @endif>Normal</option>
														<option value="2" @if(old('priority')=="2") selected="true" @elseif(isset($product->priority) && $product->priority=="2") selected="true" @endif>Medium</option>
														<option value="3" @if(old('priority')=="3") selected="true" @elseif(isset($product->priority) && $product->priority=="3") selected="true" @endif>High</option>
													</select>
													@if($errors->has('priority'))
                                      					<span class="text-danger">{{$errors->first('priority')}}</span>
                                    				@endif
												</div>
											</div>
										</div>
										@php
											if(isset($seo->page_keywords)){
												$page_keywords = explode(',',$seo->page_keywords);
											}
											if(isset($product->brand_tag)){
												$brand_tag = explode(',',$product->brand_tag);
											}
										@endphp	
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Brand Tags</label>
													<select name="brand_tag[]" id="brand_tag" tabindex="7" class="form-control select2" multiple="multiple">
														@if(isset($brand_tag))
															@foreach($brand_tag as $key => $value)
															<option
																 selected= "selected" value="{{ $value }}"
															>{{$value}}</option>
															@endforeach
														@else
															<option></option>	
														@endif
													</select>
													@if($errors->has('brand_tag'))
                                      					<span class="text-danger">{{$errors->first('brand_tag')}}</span>
                                    				@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Category<span class="red">*</span></label>
													<!--  -->
													<select name="category_id" id="category_id" tabindex="8" class="form-control" style="width: 100%; height:36px;">
														<option value="">Select Category</option>
														@foreach($categories as $data)
															<option value="{{ $data->id }}"
															@if(old('category_id') == $data->id)
	                                						selected="true"
	                                						@endif
					                                        @if(isset($product->category_id) && $data->id== $product->category_id))
					                                            selected="true"
					                                        @endif
					                                        
					                                        >{{ $data->name }}</option>
					                                    @endforeach
													</select>
													@if($errors->has('category_id'))
		                                      			<span class="text-danger">{{$errors->first('category_id')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Product Origin Country<span class="red">*</span></label>
													<select name="country" class="form-control" tabindex="9">
														<option value="">Select Country</option>
														@foreach($countrys as $country)
														<option value="{{ $country->id }}"
															@if(old('country') == $country->id)
	                                						selected="true"
	                                						@endif
					                                        @if(isset($product->country) && $country->id== $product->country))
					                                            selected="true"
					                                        @endif
					                                        
					                                        >{{ $country->country }}</option>
														
														@endforeach
													</select>
													@if($errors->has('country'))
                                      					<span class="text-danger">{{$errors->first('country')}}</span>
                                    				@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Product Name<span class="red">*</span></label>
													<input type="text" class="form-control" tabindex="10" name="name" placeholder="Enter Product Name" id="name" value="@if(isset($product->name)){{$product->name}}@elseif(old('name')!=''){{old('name')}} @endif" maxlength="50">
													@if($errors->has('name'))
                                      					<span class="text-danger">{{$errors->first('name')}}</span>
                                    				@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-4 col-xs-12">
												<div class="form-group">
													<label>Stock available (units)</label>
													<input type="text" name="quantity" tabindex="11" placeholder="Enter Stock available (units)" class="form-control" maxlength="50" value="@if(isset($product->quantity)){{$product->quantity}}@elseif(old('quantity')!=''){{old('quantity')}} @endif">
													@if($errors->has('quantity'))
		                                      			<span class="text-danger">{{$errors->first('quantity')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-4 col-xs-12">
												<div class="form-group">
													<label>Price per unit USD</label>
													<input type="text" name="price_unit_usd" tabindex="12" placeholder="Price Per Unit USD" class="form-control" maxlength="50" value="@if(isset($product->price_unit_usd)){{$product->price_unit_usd}}@elseif(old('price_unit_usd')!=''){{old('price_unit_usd')}} @endif">
													@if($errors->has('price_unit_usd'))
		                                      			<span class="text-danger">{{$errors->first('price_unit_usd')}}</span>
		                                    		@endif	
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-4 col-xs-12">
												<div class="form-group">
													<label>Price per unit EUR</label>
													<input type="text" name="price_unit_eur" tabindex="13" placeholder="Price Per Unit EUR" class="form-control" maxlength="50" value="@if(isset($product->price_unit_eur)){{$product->price_unit_eur}}@elseif(old('price_unit_eur')!=''){{old('price_unit_eur')}} @endif">
													@if($errors->has('price_unit_eur'))
		                                      			<span class="text-danger">{{$errors->first('price_unit_eur')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-4 col-xs-12">
												<div class="form-group">
													<label>Production Year</label>
													<input type="text" name="production_year" placeholder="Product Year" tabindex="14" class="form-control" maxlength="50" value="@if(isset($product->production_year)){{$product->production_year}}@elseif(old('production_year')!=''){{old('production_year')}} @endif">
													@if($errors->has('production_year'))
		                                      			<span class="text-danger">{{$errors->first('production_year')}}</span>
		                                    		@endif	
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-4 col-xs-12">
												<div class="form-group">
													<label>Product Alc. Vol</label>
													<input type="text" name="product_ac_vol" tabindex="15" placeholder="Product Alc. Vol" class="form-control" maxlength="50" value="@if(isset($product->product_ac_vol)){{$product->product_ac_vol}}@elseif(old('product_ac_vol')!=''){{old('product_ac_vol')}} @endif">
													@if($errors->has('product_ac_vol'))
		                                      			<span class="text-danger">{{$errors->first('product_ac_vol')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-4 col-xs-12">
												<div class="form-group">
													<label>Product Format</label>
													<select name="productformat" id="productformat" tabindex="16" class="form-control">
														<option value="beer-650" @if(old('productformat')=="beer-650") selected="true" @elseif(isset($product->productformat) && $product->productformat=="beer-650") selected="true" @endif> 22 fl oz (650 ml) BOTTLE BEER</option>
														<option value="100ml" @if(old('productformat')=="100ml") selected="true" @elseif(isset($product->productformat) && $product->productformat=="100ml") selected="true" @endif>100 ml</option>
														<option value="beer-12-OZ " @if(old('productformat')=="beer-12-OZ") selected="true" @elseif(isset($product->productformat) && $product->productformat=="beer-12-OZ") selected="true" @endif>12 OZ BOTTLE BEER</option>
														<option value="Wine-1500-" @if(old('productformat')=="Wine-1500-") selected="true" @elseif(isset($product->productformat) && $product->productformat=="Wine-1500-") selected="true" @endif>1500ml</option>
														<option value="018liter" @if(old('productformat')=="018liter") selected="true" @elseif(isset($product->productformat) && $product->productformat=="018liter") selected="true" @endif>187ml</option>
														<option value="MiniShots-" @if(old('productformat')=="MiniShots-") selected="true" @elseif(isset($product->productformat) && $product->productformat=="MiniShots-") selected="true" @endif>2 fl oz (59 ml)</option>
														<option value="Can-250" @if(old('productformat')=="Can-250") selected="true" @elseif(isset($product->productformat) && $product->productformat=="Can-250") selected="true" @endif>250 ml / 8.45 OZ CAN</option>
														<option value="32 fl oz" @if(old('productformat')=="32 fl oz") selected="true" @elseif(isset($product->productformat) && $product->productformat=="32 fl oz") selected="true" @endif>32 fl oz (946ml)</option>
														<option value="beer-330" @if(old('productformat')=="beer-330") selected="true" @elseif(isset($product->productformat) && $product->productformat=="beer-330") selected="true" @endif>330ml</option>
														<option value="Can-355" @if(old('productformat')=="Can-355") selected="true" @elseif(isset($product->productformat) && $product->productformat=="Can-355") selected="true" @endif>355 ml / 12 OZ CAN</option>
														<option value="Wine-375" @if(old('productformat')=="Wine-375") selected="true" @elseif(isset($product->productformat) && $product->productformat=="Wine-375") selected="true" @endif>375ml</option>
														<option value="Can-473" @if(old('productformat')=="Can-473") selected="true" @elseif(isset($product->productformat) && $product->productformat=="Can-473") selected="true" @endif>473 ml / 16 OZ CAN</option>
														<option value="50ml"@if(old('productformat')=="50ml") selected="true" @elseif(isset($product->productformat) && $product->productformat=="50ml") selected="true" @endif>50 ml</option>
														<option value="Wine-500"@if(old('productformat')=="Wine-500") selected="true" @elseif(isset($product->productformat) && $product->productformat=="Wine-500") selected="true" @endif>500ml</option>
														<option value="pet-600ml-"@if(old('productformat')=="pet-600ml-") selected="true" @elseif(isset($product->productformat) && $product->productformat=="pet-600ml-") selected="true" @endif>600 ml</option>
														<option value="700ml" @if(old('productformat')=="700ml") selected="true" @elseif(isset($product->productformat) && $product->productformat=="700ml") selected="true" @endif>700ml</option>
														<option value="Wine-750" @if(old('productformat')=="Wine-750") selected="true" @elseif(isset($product->productformat) && $product->productformat=="Wine-750") selected="true" @endif>750ml</option>
														<option value="liter" @if(old('productformat')=="liter") selected="true" @elseif(isset($product->productformat) && $product->productformat=="liter") selected="true" @endif>Liter</option>
													</select>
													@if($errors->has('productformat'))
		                                      			<span class="text-danger">{{$errors->first('productformat')}}</span>
		                                    		@endif	
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-4 col-xs-12">
												<div class="form-group">
													<label>Current World Distribution</label>
													<input type="text" name="current_world_distribution" placeholder="Current World Distribution" tabindex="17" class="form-control" maxlength="50" value="@if(isset($product->current_world_distribution)){{$product->current_world_distribution}}@elseif(old('current_world_distribution')!=''){{old('current_world_distribution')}} @endif">
													@if($errors->has('current_world_distribution'))
		                                      			<span class="text-danger">{{$errors->first('current_world_distribution')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-4 col-xs-12">
												<div class="form-group">
													<label>Desired World Distribution</label>
													<input type="text" name="desired_world_distribution" placeholder="Desired World Distribution" tabindex="18" class="form-control" maxlength="50" value="@if(isset($product->desired_world_distribution)){{$product->desired_world_distribution}}@elseif(old('desired_world_distribution')!=''){{old('desired_world_distribution')}} @endif">
													@if($errors->has('desired_world_distribution'))
		                                      			<span class="text-danger">{{$errors->first('desired_world_distribution')}}</span>
		                                    		@endif	
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-4 col-xs-12">
												<div class="form-group">
													<label>Current US Distribution</label>
													<input type="text" name="current_us_distribution" placeholder="Current US Distribution" tabindex="19" class="form-control" maxlength="50" value="@if(isset($product->current_us_distribution)){{$product->current_us_distribution}}@elseif(old('current_us_distribution')!=''){{old('current_us_distribution')}} @endif">
													@if($errors->has('current_us_distribution'))
		                                      			<span class="text-danger">{{$errors->first('current_us_distribution')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-4 col-xs-12">
												<div class="form-group">
													<label>Desired US Distribution </label>
													<input type="text" name="desired_us_distribution" placeholder="Desired US Distribution" tabindex="20" class="form-control" maxlength="50" value="@if(isset($product->desired_us_distribution)){{$product->desired_us_distribution}}@elseif(old('desired_us_distribution')!=''){{old('desired_us_distribution')}} @endif">
													@if($errors->has('desired_us_distribution'))
		                                      			<span class="text-danger">{{$errors->first('desired_us_distribution')}}</span>
		                                    		@endif	
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Pick Up Location</label>
													<input type="text" name="pick_up_location" id="autocomplete" tabindex="21" placeholder="Pick Up Location" class="form-control" onFocus="geolocate()" maxlength="50" value="@if(isset($product->pick_up_location)){{$product->pick_up_location}}@elseif(old('pick_up_location')!=''){{old('pick_up_location')}} @endif">
													@if($errors->has('pick_up_location'))
		                                      			<span class="text-danger">{{$errors->first('pick_up_location')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Pick Up LAT</label>
													<input type="text" name="pick_lat" placeholder="Pick Up LAT" tabindex="22" id="latitude" class="form-control" maxlength="50" value="@if(isset($product->pick_lat)){{$product->pick_lat}}@elseif(old('pick_lat')!=''){{old('pick_lat')}} @endif">
													@if($errors->has('pick_lat'))
		                                      			<span class="text-danger">{{$errors->first('pick_lat')}}</span>
		                                    		@endif	
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Pick Up LON</label>
													<input type="text" name="pic_lon" placeholder="Pick Up LON" tabindex="23" class="form-control" id="longitude" maxlength="50" value="@if(isset($product->pic_lon)){{$product->pic_lon}}@elseif(old('pic_lon')!=''){{old('pic_lon')}} @endif">
													@if($errors->has('pic_lon'))
		                                      			<span class="text-danger">{{$errors->first('pic_lon')}}</span>
		                                    		@endif	
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>SEO page title</label>
													<input type="text" name="seo_title" tabindex="24" class="form-control" placeholder="SEO page title" value="@if(isset($seo->page_title)){{ $seo->page_title }}@elseif(old('seo_title')!=''){{ old('seo_title') }} @endif">
													@if($errors->has('seo_title'))
		                                      			<span class="text-danger">{{$errors->first('seo_title')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>SEO page description</label>
													<textarea name="seo_page_description" placeholder="SEO page description" tabindex="25" class="form-control">@if(isset($seo->page_description)){{$seo->page_description}}@elseif(old('seo_page_description')!=''){{old('seo_page_description')}} @endif</textarea>
													@if($errors->has('seo_page_description'))
		                                      			<span class="text-danger">{{$errors->first('seo_page_description')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>SEO page keywords</label>
													<select name="seo_page_keywords[]" id="seo_page_keywords" tabindex="26" class="form-control select2" multiple="multiple">
														@if(isset($page_keywords))
															@foreach($page_keywords as $key => $value)
															<option
																 selected= "selected" value="{{ $value }}"
															>{{$value}}</option>
															@endforeach
														@else
															<option></option>	
														@endif
													</select>
													@if($errors->has('seo_page_keywords'))
		                                      			<span class="text-danger">{{$errors->first('seo_page_keywords')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Product Appellation<span class="red">*</span></label>
													<input type="text" name="product_appellation" tabindex="27" class="form-control" placeholder="Product Appellation" value="@if(isset($product->product_appellation)){{ $product->product_appellation }}@elseif(old('product_appellation')!=''){{ old('product_appellation') }} @endif">
													@if($errors->has('product_appellation'))
		                                      			<span class="text-danger">{{$errors->first('product_appellation')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Product Short description</label>
													<textarea name="product_description_short" class="form-control" tabindex="28" placeholder="Product Short description">@if(isset($product->product_description_short)){{$product->product_description_short}}@elseif(old('product_description_short')!=''){{old('product_description_short')}} @endif</textarea>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8">
												<div class="form-group">
													<label>Product Full Description</label>
													<textarea class="form-control" tabindex="29" id="content1" name="product_description_long">
														@if(isset($product->description)){{$product->description}}@elseif(old('product_description_long')!=''){{old('product_description_long')}} @endif</textarea>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="file-upload">
													<div class="file-select">
														<div class="file-select-button" id="fileName">Brand Main Image<span class="red">*</span></div>
														<div class="file-select-name" id="noFile">No file chosen...</div>
														<input type="file" name="image" tabindex="30" id="image" accept="image/*" value="" >
													</div>
												</div>
												@if($errors->has('image'))
	                                      			<span class="text-danger">{{$errors->first('image')}}</span>
	                                    		@endif
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="file-upload">
													<div class="file-select">
														<div class="file-select-button" id="fileName">Brand Additional images</div>
														<div class="file-select-name" id="noFile_additonal">No file chosen...</div>
														<input type="file" name="additional_image[]" tabindex="31" id="additional_image" accept="image/*" value="" multiple="">
													</div>
												</div>
												@if($errors->has('additional_image'))
	                                      			<span class="text-danger">{{$errors->first('additional_image')}}</span>
	                                    		@endif
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div id="image_beside">
													@if(isset($product->img))
													<img src="{{url($product->img)}}" height="300px" id="edit_image" width="300px">
													@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div id="image_additional_beside">
													@if(isset($product_images))
													@foreach($product_images as $product_image)
														<img src="{{url($product_image->src)}}" height="300px" id="edit_additional_image" width="300px">
													@endforeach
													@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="file-upload">
													<div class="file-select">
														<div class="file-select-button" id="fileName">Brand Header Image</div>
														<div class="file-select-name" id="noFile">No file chosen...</div>
														<input type="file" name="header_image" tabindex="32" id="header_image" accept="image/*" value="" >
													</div>
												</div>
												@if($errors->has('brand_header'))
	                                      			<span class="text-danger">{{$errors->first('brand_header')}}</span>
	                                    		@endif
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div id="image_additional_beside">
													@if(isset($product->header_image))
													<img src="{{url($product->header_image)}}" height="300px" id="edit_additional_image" width="300px">
													@endif
												</div>
											</div>
										</div>
										<div class="row" style="margin-top: 10px">
											<div class="col-sm-12">
												<div class="submit-btn">
													<button type="submit" class="btn btn-submit">Submit</button>
												</div>
											</div>
										</div>
								 	</form>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@push('css')	
<style type="text/css">
.errors_cover{
	color: red;
}	
.product_image{
	height: 72px;
	width: 82px;
}
.file-select-name{
	display: contents!important;
}
.show-image {
    position: relative;
    float:left;
    margin:12px;
}
.show-image:hover img{
    opacity:0.5;
}
.show-image:hover a.delete {
    display: block;
}
.show-image a.delete {
    position:absolute;
    display:none;
}
.show-image a.delete {
	color: red;
	width: 100%;
    top:30%;
}
</style>
@endpush
@push('js')
<script type="text/javascript" src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script type="text/javascript" src="{{url('js/custom/custom.js')}}"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>


<script type="text/javascript">
$(document).ready(function() {
	$('#create_date').datepicker({
        autoclose: true,  
        format: "dd-mm-yyyy",
        todayHighlight: true,
    });
    $('#edit_date').datepicker({
        autoclose: true,  
        format: "dd-mm-yyyy",
        todayHighlight: true,
    });
    $('#additional_image').change(function(){
        readURL(this);
    });
    
    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#noFile_additonal').empty();
            $('#edit_additional_image').remove();
            $('#image_additional_beside').empty();
            $("#image_additional_beside").append('<div class="row">'+
                '<img src ='+e.target.result+' width="250px" height="240px" >'+
                    '</div>'+
            '</div>');
        }
        reader.readAsDataURL(input.files[0]);
    }
    


}
})
</script>
<script>
        // This example displays an address form, using the autocomplete feature
        // of the Google Places API to help users fill in the information.

        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

        var placeSearch, autocomplete;
       
        function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */
                (document.getElementById('autocomplete')), { types: ['geocode'] });

            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            autocomplete.addListener('place_changed', fillInAddress);
        }

        function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();
            address = place.formatted_address;
            var geocoder = new google.maps.Geocoder();
			geocoder.geocode( { 'address': address}, function(results, status) {
			  	if (status == google.maps.GeocoderStatus.OK) {
				    var latitude = results[0].geometry.location.lat();
				    var longitude = results[0].geometry.location.lng();
				    document.getElementById("latitude").value = latitude;
					document.getElementById("longitude").value = longitude;
			  	} 
			});
        }

        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfDJaVdwAS-KTBw3WIyMPV2CcrTJAUKFI&libraries=places&callback=initAutocomplete" async defer></script>

@endpush
