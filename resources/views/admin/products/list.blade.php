@extends('layouts.admin.layout')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage Products</h4> </div>
		</div>
		@include('layouts.errors-and-messages')
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-listing">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							<li><a href="{{ route('admin.products.create') }}">Add Product</a></li>
							<li class="active"><a>Products Listing</a></li>
					  	</ul>
					  	<div class="tab-content">
							<div id="ProductListing" class="tab-pane fade in active">
								<div class="product-listing">
									<div class="table-responsive">
										<table id="products" class="table border-b1px products" style="width:100%">
											<thead>
												<tr>
													<th class="th-25">Product Name</th>
													<th class="th-15">Image</th>
													<th class="th-5">Category</th>
													<th class="th-5">Price</th>
													<th class="th-35">Description</th>
													<th class="th-10">Actions</th>
												</tr>
											</thead>
											
											<tbody>
												@forelse ($products as $product)
													<td>{{ $product->name }}</td>
													<td>
														<img src="{{url ($product->img) }}" alt="" height="60px" width="60px" class="">
													</td>
													<td>{{ $product->categories_name }}</td>
													<td>{{ $product->price_unit_usd }}</td>
													<td>{{ $product->description }}</td>
													<td>
														<div class="action-btn">
															<form action="{{ route('admin.products.destroy', $product->id) }}" method="post" class="form-horizontal">
																{{ csrf_field() }}
																<input type="hidden" name="_method" value="delete">
																<div class="btn-group">
																	<a href="{{ route('admin.products.edit', $product->id) }}" class="btn btn-edit"><i class="mdi mdi-pencil"></i></a>
																	<button onclick="return confirm('Are you sure you want to delete?')" type="submit" class="btn btn-delete"><i class="mdi mdi-close"></i></button>
																</div>
															</form>
														</div>
													</td>
												</tr>
												@empty
													<tr>
														<td colspan="8" align="center">Data Not Found</td>
													</tr>
												@endforelse
											</tbody>
										</table>
									</div>
								</div>
							</div>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
<style type="text/css">
.btn-info, .btn-info.disabled {
    color: black!important;
    background: #e4e7ea!important;
}
.btn-info:hover{
    border:1px solid!important;
    border-color: none!important;
}
</style>
@push('js')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        init_datatable();
    } );
    
    function init_datatable(){
        $('.products').DataTable().destroy();
        setTimeout(function(){
            var table = $('.products').DataTable({
                deferRender: true,
                scrollX: true
            });
        },300);
    }
</script>
@endpush