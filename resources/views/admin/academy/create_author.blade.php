@extends('layouts.admin.layout')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage Academy</h4> 
			</div>
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-tab">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							@if(isset($author->id))
								<li class="active"><a>Edit Author</a></li>
							@else
								<li class="active"><a>Add Author</a></li>
							@endif
							<li><a href="{{ route('admin.academys.list_author') }}">Author Listing</a></li>
					  	</ul>
					  	<div class="tab-content">
							<div id="AddProcucts" class="tab-pane fade in active">
								<div class="add-product">
									<form id="create_product" action="{{route('admin.academys.store_author')}}" method="post" class="form" enctype="multipart/form-data">
										{{csrf_field()}}
										@if(isset($author->id))
				            				<input type="hidden" name="id" value="{{$author->id}}">
				            				<input type="hidden" name="seo_id" value="{{$author->seo_id}}">
		            					@else
				            				<input type="hidden" name="id" value="">
			            				@endif
			            				@php
											if(isset($seo->page_keywords)){
												$page_keywords = explode(',',$seo->page_keywords);
											}
											if(isset($author->record_tags)){
												$record_tags = explode(',',$author->record_tags);
											}
										@endphp
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Record Tages</label>
													<select name="record_tags[]" id="record_tags" tabindex="26" class="form-control select2" multiple="multiple">
														@if(isset($record_tags))
															@foreach($record_tags as $key => $value)
															<option
																 selected= "selected" value="{{ $value }}"
															>{{$value}}</option>
															@endforeach
														@else
															<option></option>	
														@endif
													</select>
													@if($errors->has('record_tags'))
		                                      			<span class="text-danger">{{$errors->first('record_tags')}}</span>
		                                    		@endif
												</div>
											</div>
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
						  						<div class="form-group">
													<label>Name</label>
													<input type="text" tabindex="5" name="name" class="form-control" maxlength="50" value="@if(isset($author->name)){{$author->name}} @elseif(old('name')!=''){{ old('name') }} @endif" placeholder="Enter name">
													@if($errors->has('name'))
	                                      				<span class="text-danger">{{$errors->first('name')}}</span>
			                                    	@endif
												</div>
					  						</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
						  						<div class="form-group">
													<label>Surname</label>
													<input type="text" tabindex="5" name="surname" class="form-control" maxlength="50" value="@if(isset($author->surname)){{$author->surname}} @elseif(old('surname')!=''){{ old('surname')}} @endif" placeholder="Enter surname">
													@if($errors->has('surname'))
		                                      			<span class="text-danger">{{$errors->first('surname')}}</span>
		                                    		@endif
												</div>
					  						</div>
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
						  						<div class="form-group">
													<label>Author Website URL</label>
													<input type="text" tabindex="5" name="author_website_url" class="form-control" maxlength="50" value="@if(isset($author->author_website_url)){{$author->author_website_url}} @elseif(old('author_website_url')!=''){{ old('author_website_url') }}@endif" placeholder="Enter website url">
													@if($errors->has('author_website_url'))
			                                      		<span class="text-danger">{{$errors->first('author_website_url')}}</span>
			                                    	@endif
												</div>
					  						</div>
										</div>
										<div class="row">
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
						  						<div class="form-group">
													<label>Twitter(only username,no @)</label>
													<input type="text" tabindex="5" name="twitter" class="form-control" maxlength="50" value="@if(isset($author->twitter)){{$author->twitter}} @elseif(old('twitter')!=''){{ old('twitter') }} @endif" placeholder="Enter twitter">
													@if($errors->has('twitter'))
		                                      			<span class="text-danger">{{$errors->first('twitter')}}</span>
		                                    		@endif
												</div>
					  						</div>
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
						  						<div class="form-group">
													<label>Facebook(URL)</label>
													<input type="text" tabindex="5" name="facebook" class="form-control" maxlength="50" value="@if(isset($author->facebook)){{$author->facebook}} @elseif(old('facebook')!=''){{ old('facebook')}} @endif" placeholder="Enter facebook">
													@if($errors->has('facebook'))
		                                      			<span class="text-danger">{{$errors->first('facebook')}}</span>
		                                    		@endif
												</div>
					  						</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
						  						<div class="form-group">
													<label>Linkdin(URL)</label>
													<input type="text" tabindex="5" name="linkdin" class="form-control" maxlength="50" value="@if(isset($author->linkdin)){{$author->linkdin}} @elseif(old('linkdin')!=''){{ old('linkdin')}} @endif" placeholder="Enter linkdin">
													@if($errors->has('linkdin'))
		                                      			<span class="text-danger">{{$errors->first('linkdin')}}</span>
		                                    		@endif
												</div>
					  						</div>
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
						  						<div class="form-group">
													<label>Author Blog(URL)</label>
													<input type="text" tabindex="5" name="author_blog" class="form-control" maxlength="50" value="@if(isset($author->author_blog)){{$author->author_blog}} @elseif(old('author_blog')!=''){{ old('author_blog')}} @endif" placeholder="Enter author blog">
													@if($errors->has('author_blog'))
		                                      			<span class="text-danger">{{$errors->first('author_blog')}}</span>
		                                    		@endif
												</div>
					  						</div>
										</div>
										<div class="row">
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
						  						<div class="form-group">
													<label>Email</label>
														<input type="email" tabindex="5" name="email" class="form-control" maxlength="50" value="@if(isset($author->email)){{$author->email}} @elseif(old('email')!=''){{ old('email')}} @endif" placeholder="Enter email">
														@if($errors->has('email'))
			                                      			<span class="text-danger">{{$errors->first('email')}}</span>
			                                    		@endif
												</div>
					  						</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
						  						<div class="form-group">
													<label>Place</label>
													<input type="text" tabindex="5" name="place" class="form-control" maxlength="50" value="@if(isset($author->place)){{$author->place}} @elseif(old('place')!=''){{ old('place')}} @endif" placeholder="Enter place">
													@if($errors->has('place'))
		                                      			<span class="text-danger">{{$errors->first('place')}}</span>
		                                    		@endif
												</div>
					  						</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>SEO page title</label>
													<input type="text" tabindex="42" name="seo_title" maxlength="50" class="form-control" placeholder="SEO page title" value="@if(isset($seo->page_title)){{ $seo->page_title }}@elseif(old('seo_title')!=''){{ old('seo_title') }} @endif">
													@if($errors->has('seo_title'))
		                                      			<span class="text-danger">{{$errors->first('seo_title')}}</span>
		                                    		@endif
												</div>
											</div>
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>SEO page description</label>
													<textarea name="seo_page_description" placeholder="SEO page description" maxlength="100" tabindex="43" class="form-control">@if(isset($seo->page_description)){{$seo->page_description}}@elseif(old('seo_page_description')!=''){{old('seo_page_description')}} @endif</textarea>
													@if($errors->has('seo_page_description'))
		                                      			<span class="text-danger">{{$errors->first('seo_page_description')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>SEO page keywords</label>
													<select name="seo_page_keywords[]" id="seo_page_keywords" tabindex="44" class="form-control select2" multiple="multiple">
														@if(isset($page_keywords))
															@foreach($page_keywords as $key => $value)
															<option
																 selected= "selected" value="{{ $value }}"
															>{{$value}}</option>
															@endforeach
														@else
															<option></option>	
														@endif
													</select>
													@if($errors->has('seo_page_keywords'))
		                                      			<span class="text-danger">{{$errors->first('seo_page_keywords')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Author Tagline English<span class="red">*</span></label>
													<textarea class="form-control" tabindex="46" id="content1" name="author_tagline_english">
													@if(isset($author->author_tagline_english)){{$author->author_tagline_english}} @elseif(old('author_tagline_english')!=''){{ old('author_tagline_english')}} @endif
					                           		</textarea>
					                           		@if($errors->has('author_tagline_english'))
		                                      			<span class="text-danger">{{$errors->first('author_tagline_english')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Full Bio English<span class="red">*</span></label>
													<textarea class="form-control" tabindex="46" id="content2" name="full_bio_english">
													@if(isset($author->full_bio_english)){{$author->full_bio_english}} @elseif(old('full_bio_english')!=''){{ old('full_bio_english')}} @endif
					                           		</textarea>
					                           		@if($errors->has('full_bio_english'))
		                                      			<span class="text-danger">{{$errors->first('full_bio_english')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<label>Author Photo</label>
												<div class="file-upload">
													<div class="file-select">
														<div class="file-select-button" id="fileName">Author Photo<span class="red">*</span></div>
														<div class="file-select-name" id="noFile">No file chosen...</div>
														<input tabindex="47" type="file" name="author_photo" id="image" accept="image/*" value="" >
													</div>
												</div>
												@if($errors->has('author_photo'))
	                                      			<span class="text-danger">{{$errors->first('author_photo')}}</span>
	                                    		@endif
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<div id="image_beside">
														@if(isset($author->author_photo))
														<img src="{{url($author->author_photo)}}" class="image" id="edit_image">
														@endif
													</div>
												</div>
											</div>
										</div><br>
										<div class="row" style="margin-top: 10px">
											<div class="col-sm-12">
												<div class="submit-btn">
													<button type="submit" tabindex="48" class="btn btn-submit">Submit</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@push('js')
<script type="text/javascript" src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script type="text/javascript" src="{{url('js/custom/custom.js')}}"></script>
<script type="text/javascript">
	CKEDITOR.replace( 'content2' );
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfDJaVdwAS-KTBw3WIyMPV2CcrTJAUKFI&libraries=places&callback=initAutocomplete" async defer></script>
@endpush