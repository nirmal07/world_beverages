@extends('layouts.admin.layout')
@section('content')
<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage Academys</h4> </div>
			<!-- <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="#">Manage Products</a></li>
					<li class="active">Add Products</li>
				</ol>
			</div> -->
			<!-- /.col-lg-12 -->
		</div>
		@include('layouts.errors-and-messages')
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-listing">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							<li><a href="{{ route('admin.academys.create_content') }}">Add Content</a></li>
							<li class="active"><a>Content Listing</a></li>
					  </ul>
					  <div class="tab-content">
							<div id="ProductListing" class="tab-pane fade in active">
								<div class="product-listing">
									<div class="table-responsive">
										<table id="events" class="events table border-b1px" style="width:100%">
											<thead>
												<tr>
													<th>Creation Date</th>
													<th>Edition Date</th>
													<th>Content Type</th>
													<th>Document Type</th>
													<th>Moderator</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
												@forelse ($contents as $content)
												<tr>
													<td>{{ date('d-m-Y',strtotime($content->created_at)) }}</td>
													<td>{{ date('d-m-Y',strtotime($content->updated_at)) }}</td>
													<td>{{$content->content_type}}</td>
													<td>{{$content->document_type}}</td>
													<td>{{$content->modreator}}</td>
													<td>

														<div class="action-btn">
															<form action="{{ route('admin.academys.destroy_content', $content->id) }}" method="get" class="form-horizontal">
																{{ csrf_field() }}
																<input type="hidden" name="_method" value="delete">
																<div class="btn-group">
																	<a href="{{ route('admin.academys.edit_content', $content->id) }}" class="btn btn-edit"><i class="mdi mdi-pencil"></i></a>
																	<button onclick="return confirm('Are you sure you want to delete?')" type="submit" class="btn btn-delete"><i class="mdi mdi-close"></i></button>
																</div>
															</form>
														</div>
													</td>
												</tr>
												@empty
												<tr>
													<td colspan="3" align="center">Data Not Found</td>
												</tr>
												@endforelse
											</tbody>
										</table>
								</div>
							</div>
						</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@endsection
<style type="text/css">
	.btn-info, .btn-info.disabled {
        color: black!important;
        background: #e4e7ea!important;
    }
    .btn-info:hover{
        border:1px solid!important;
        border-color: none!important;
    }
</style>
@push('js')
<script>
    $(document).ready(function() {
        init_datatable();
    } );
    
    function init_datatable(){
        $('#events').DataTable().destroy();
        setTimeout(function(){
            var table = $('#events').DataTable({
                deferRender: true,
                scrollX: true
            });
        },300);
    }
</script>
@endpush