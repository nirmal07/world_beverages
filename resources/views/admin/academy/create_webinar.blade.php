@extends('layouts.admin.layout')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage Academy</h4> 
			</div>
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-tab">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							@if(isset($webinar->id))
								<li class="active"><a>Edit Webinar</a></li>
							@else
								<li class="active"><a>Add Webinar</a></li>
							@endif
							<li><a href="{{ route('admin.academys.list_webinar') }}">Webinar Listing</a></li>
					  	</ul>
					  	<div class="tab-content">
					  		<div id="AddProcucts" class="tab-pane fade in active">
					  			<div class="addproduct">
					  				<form action="{{route('admin.academys.store_webinar')}}" method="post" enctype="multipart/form-data">
					  					{{csrf_field()}}
					  					@if(isset($webinar->id))
				            				<input type="hidden" name="id" value="{{$webinar->id}}">
				            				<input type="hidden" name="seo_id" value="{{$webinar->seo_id}}">
		            					@else
				            				<input type="hidden" name="id" value="">
			            				@endif
			            				@php
											if(isset($seo->page_keywords)){
												$page_keywords = explode(',',$seo->page_keywords);
											}
											if(isset($webinar->section_tags)){
												$section_tags = explode(',',$webinar->section_tags);
											}
											if(isset($webinar->tags_english)){
												$tags_english = explode(',',$webinar->tags_english);
											}
											if(isset($webinar->category_association)){
												$category_association = explode(',',$webinar->category_association);
											}
											if(isset($webinar->expertise_association)){
												$expertise_association = explode(',',$webinar->expertise_association);
											}
											if(isset($webinar->btn_use_association)){
												$btn_use_association = explode(',',$webinar->btn_use_association);
											}
										@endphp
					  					<div class="row">
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
					  							<div class="form-group">
					  								<label>Content Type</label>
					  								<select name="content_type" id="content_type" tabindex="1" class="form-control" style="width: 100%; height:36px;">
					  									<option value="">Select content type</option>
														<option value="webinars"@if(old('content_type')=="webinars") selected="true"@elseif(isset($webinar->content_type) && $webinar->content_type == 'webinars') selected @endif>Webinars</option>
														<option value="articles" @if(old('content_type')=="articles") selected="true"@elseif(isset($webinar->content_type) && $webinar->content_type == 'articles') selected @endif>Articles</option>
														<option value="videos" @if(old('content_type')=="videos") selected="true"@elseif(isset($webinar->content_type) && $webinar->content_type == 'videos') selected @endif>Videos</option>
													</select>
													@if($errors->has('content_type'))
		                                      			<span class="text-danger">{{$errors->first('content_type')}}</span>
		                                    		@endif
					  							</div>
					  						</div>
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
					  							<div class="form-group">
					  								<label>Document Type</label>
					  								<select name="document_type" id="content_type" tabindex="1" class="form-control" style="width: 100%; height:36px;">
					  									<option value="">Select document type</option>
														<option value="youtubevideo" @if(old('document_type')=="youtubevideo") selected="true" @elseif(isset($webinar->document_type) && $webinar->document_type == 'youtubevideo') selected @endif>Youtube video</option>
														<option value="slidesharepresentation" @if(old('document_type')=="slidesharepresentation") selected="true"@elseif(isset($webinar->document_type) && $webinar->document_type == 'slidesharepresentation') selected @endif>Slideshare presentation</option>
													</select>
													@if($errors->has('document_type'))
		                                      			<span class="text-danger">{{$errors->first('document_type')}}</span>
		                                    		@endif
					  							</div>
					  						</div>
					  					</div>
					  					<div class="row">
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
					  							<div class="form-group">
					  								<label>Author 1</label>
					  								<select name="author_one" id="content_type" tabindex="1" class="form-control" style="width: 100%; height:36px;">
					  									<option value="">Select author name</option>
					  									@foreach($authors as $author)
														<option value="{{$author->id}}" @if(old('author_one') == $author->id)
	                                						selected="true"
	                                						@endif
					                                        @if(isset($webinar->author_one) && $author->id== $webinar->author_one))
					                                            selected="true"
					                                        @endif>{{$author->name}}</option>
					  								@endforeach
													</select>
													@if($errors->has('author_one'))
		                                      			<span class="text-danger">{{$errors->first('author_one')}}</span>
		                                    		@endif
					  							</div>
					  						</div>
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
					  							<div class="form-group">
					  								<label>Author 2</label>
					  								<select name="author_two" id="content_type" tabindex="1" class="form-control" style="width: 100%; height:36px;">
														<option value="">Select author surname</option>
					  									@foreach($authors as $data)
														<option value="{{$data->id}}" @if(old('author_two') == $data->id)
	                                						selected="true"
	                                						@endif
	                                						@if(isset($webinar->author_two) && $data->id== $webinar->author_two))
					                                            selected="true"
					                                        @endif>{{$data->surname}}</option>
														@endforeach
													</select>
													@if($errors->has('author_two'))
		                                      			<span class="text-danger">{{$errors->first('author_two')}}</span>
		                                    		@endif
					  							</div>
					  						</div>
					  					</div>
					  					<div class="row">
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
					  							<div class="form-group">
					  								<label>Modreator</label>
					  								<select name="modreator" id="content_type" tabindex="1" class="form-control" style="width: 100%; height:36px;">
					  									<option value="">Select modreator</option>
														<option value="roger" @if(old('modreator')=="roger") selected="true"@elseif(isset($webinar->modreator) && $webinar->modreator == 'roger') selected @endif>Roger</option>
														<option value="tom"@if(old('modreator')=="tom") selected="true" @elseif(isset($webinar->modreator) && $webinar->modreator == 'tom') selected @endif>Tom</option>
													</select>
													@if($errors->has('modreator'))
		                                      			<span class="text-danger">{{$errors->first('modreator')}}</span>
		                                    		@endif
					  							</div>
					  						</div>
					  					</div>
					  					@php
											$today_date = date('d-m-Y');
										@endphp
					  					<div class="row">
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
					  							<div class="form-group">
													<label>Publication Date</label>
													<input type="text" tabindex="5" name="publication_date" class="form-control" id="publication_date" maxlength="50" value="@if(isset($webinar->created_at)){{date('d-m-Y',strtotime($webinar->created_at))}}@else{{$today_date}}@endif">
													@if($errors->has('publication_date'))
		                                      			<span class="text-danger">{{$errors->first('publication_date')}}</span>
		                                    		@endif
												</div>
					  						</div>
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
					  							<div class="form-group">
													<label>Section Tags</label>
													<select name="section_tags[]" id="section_tags" tabindex="26" class="form-control select2" multiple="multiple">
														@if(isset($section_tags))
															@foreach($section_tags as $key => $value)
															<option
																 selected= "selected" value="{{ $value }}"
															>{{$value}}</option>
															@endforeach
														@else
															<option></option>	
														@endif
													</select>
													@if($errors->has('section_tags'))
		                                      			<span class="text-danger">{{$errors->first('section_tags')}}</span>
		                                    		@endif
												</div>
					  						</div>
					  					</div>
					  					<div class="row">
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>User Category Association</label>
													<br>
													<input type="checkbox" name="category_association[]" tabindex="35" class="" value="other" @if(isset($category_association))
														@if (in_array("other", $category_association))
														checked =""
														@endif
														@endif
													> Other
													<br>
													<input type="checkbox" name="category_association[]" value="non_alcoholic_drinks_distributor" @if(isset($category_association))
														@if (in_array("non_alcoholic_drinks_distributor", $category_association))
														checked =""
														@endif
														@endif
													>Non Alcoholic Drinks Distributor
													<br>
													<input type="checkbox" name="category_association[]" value="non_alcoholic_drinks_supplier" @if(isset($category_association))
														@if (in_array("non_alcoholic_drinks_distributor", $category_association))
														checked="" 
														@endif
													@endif
													>Non Alcoholic Drinks Supplier
													<br>
													<input type="checkbox" name="category_association[]" value="large_retail_chain" @if(isset($category_association))
														@if (in_array("large_retail_chain", $category_association))
														checked="" 
														@endif
													@endif
													> Large Retail Chain
													<br>
													<input type="checkbox" name="category_association[]" value="consultant" @if(isset($category_association))
														@if (in_array("consultant", $category_association))
														checked="" 
														@endif
													@endif
													> Consultant
													<br>
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<br>
													<input type="checkbox" name="category_association[]" tabindex="35" class="" value="brewery" @if(isset($category_association))
														@if (in_array("brewery", $category_association))
														checked="" 
														@endif
													@endif
													> Brewery
													<br>
													<input type="checkbox" name="category_association[]" value="distillery" @if(isset($category_association))
														@if (in_array("distillery", $category_association))
														checked="" 
														@endif
													@endif
													>Distillery
													<br>
													<input type="checkbox" name="category_association[]" value="winery" @if(isset($category_association))
														@if (in_array("winery", $category_association))
														checked="" 
														@endif
													@endif
													>Winery
													<br>
													<input type="checkbox" name="category_association[]" value="beer_wholesaler" @if(isset($category_association))
														@if (in_array("beer_wholesaler", $category_association))
														checked="" 
														@endif
													@endif 
													> Beer Wholesaler
													<br>
													<input type="checkbox" name="category_association[]" value="beer_importer" @if(isset($category_association))
														@if (in_array("beer_importer", $category_association))
														checked="" 
														@endif
													@endif
													> Beer Importer
													<br>
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<br>
													<input type="checkbox" name="category_association[]" tabindex="35" class="" value="spirits_wholesaler" @if(isset($category_association))
														@if (in_array("spirits_wholesaler", $category_association))
														checked="" 
														@endif
													@endif
													> Spirits Wholesaler
													<br>
													<input type="checkbox" name="category_association[]" value="spirits_importer" @if(isset($category_association))
														@if (in_array("spirits_importer", $category_association))
														checked="" 
														@endif
													@endif
													>Spirits Importer
													<br>
													<input type="checkbox" name="category_association[]" value="wine_wholer" @if(isset($category_association))
														@if (in_array("wine_wholer", $category_association))
														checked="" 
														@endif
													@endif
													>Wine Wholesaler
													<br>
													<input type="checkbox" name="category_association[]" value="wine_importer" @if(isset($category_association))
														@if (in_array("wine_importer", $category_association))
														checked="" 
														@endif
													@endif
													> Wine Importer
													<br>
													<input type="checkbox" name="category_association[]" value="retailer" @if(isset($category_association))
														@if (in_array("retailer", $category_association))
														checked="" 
														@endif
													@endif
													> Retailer
													<br>
												</div>
											</div>
					  					</div>
					  					<div class="row">
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<br>
													<input type="checkbox" name="category_association[]" tabindex="35" class="" value="restaurant_bar" @if(isset($category_association))
														@if (in_array("restaurant_bar", $category_association))
														checked="" 
														@endif
													@endif
													> Restaurant/bar
													<br>
													<input type="checkbox" name="category_association[]" value="hotel_resort" @if(isset($category_association))
														@if (in_array("hotel_resort", $category_association))
														checked="" 
														@endif
													@endif
													>Hotel/Resort 
													<br>
													<input type="checkbox" name="category_association[]" value="press" @if(isset($category_association))
														@if (in_array("press", $category_association))
														checked="" 
														@endif
													@endif
													> Press
													<br>
													<input type="checkbox" name="category_association[]" value="association" @if(isset($category_association))
														@if (in_array("association", $category_association))
														checked="" 
														@endif
													@endif
													> Association 
													<br>
													<input type="checkbox" name="category_association[]" value="competition" @if(isset($category_association))
														@if (in_array("competition", $category_association))
														checked="" 
														@endif
													@endif
													> Competition
													<br>
													<input type="checkbox" name="category_association[]" tabindex="35" class="" value="others" @if(isset($category_association))
														@if (in_array("others", $category_association))
														checked="" 
														@endif
													@endif
													> Others
													<br>
												</div>
											</div>
					  					</div>
					  					<div class="row">
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>User Expertise Association</label>
													<br>
													<input type="checkbox" name="expertise_association[]" tabindex="35" value="3-5" @if(isset($expertise_association))
														@if (in_array("3-5", $expertise_association))
														checked="" 
														@endif
													@endif
													> 3-5 Years
													<br>
													<input type="checkbox" name="expertise_association[]" value="6-10" @if(isset($expertise_association))
														@if (in_array("6-10", $expertise_association))
														checked="" 
														@endif
													@endif
													>6-10 Years
													<br>
													<input type="checkbox" name="expertise_association[]" value="0-2" @if(isset($expertise_association))
														@if (in_array("0-2", $expertise_association))
														checked="" 
														@endif
													@endif
													> 0-2 Years
													<br>
													<input type="checkbox" name="expertise_association[]" value="11-20" @if(isset($expertise_association))
														@if (in_array("11-20", $expertise_association))
														checked="" 
														@endif
													@endif
													> 11-20 Years 
													<br>
													<input type="checkbox" name="expertise_association[]" value="20" @if(isset($expertise_association))
														@if (in_array("20", $expertise_association))
														checked="" 
														@endif
													@endif
													> 20+ Years
													<br>
													<input type="checkbox" name="expertise_association[]" tabindex="35" class="" value="wine_exporter" @if(isset($expertise_association))
														@if (in_array("wine_exporter", $expertise_association))
														checked="" 
														@endif
													@endif
													> Wine Exporter
													<br>
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>User BTN Use Association</label>
													<br>
													<input type="checkbox" name="btn_use_association[]" tabindex="35" class="" value="education_resources" @if(isset($btn_use_association))
														@if (in_array("education_resources", $btn_use_association))
														checked="" 
														@endif
													@endif
													> Education and resources
													<br>
													<input type="checkbox" name="btn_use_association[]" value="source_new_brands" @if(isset($btn_use_association))
														@if (in_array("source_new_brands", $btn_use_association))
														checked="" 
														@endif
													@endif
													>Source new brands for my portfolio
													<br>
													<input type="checkbox" name="btn_use_association[]" value="find_consultants" @if(isset($btn_use_association))
														@if (in_array("find_consultants", $btn_use_association))
														checked="" 
														@endif
													@endif
													>Find consultants
													<br>
													<input type="checkbox" name="btn_use_association[]" value="beverage_business" @if(isset($btn_use_association))
														@if (in_array("beverage_business", $btn_use_association))
														checked="" 
														@endif
													@endif 
													> Starting a new beverage business
													<br>
													<input type="checkbox" name="btn_use_association[]" value="find_importers" @if(isset($btn_use_association))
														@if (in_array("find_importers", $btn_use_association))
														checked="" 
														@endif
													@endif
													> Find importers and distributors
													<br>
												</div>
											</div>
					  					</div>
					  					<div class="row">
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
					  							<div class="form-group">
					  								<label>Article availability</label>
					  								<select name="article_availability" id="content_type" tabindex="1" class="form-control" style="width: 100%; height:36px;">
					  									<option value="">Select article availability</option>
					  									<option value="available_to_all_visitors" @if(old('article_availability')=="available_to_all_visitors") selected="true"@elseif(isset($webinar->article_availability) && $webinar->article_availability == 'available_to_all_visitors') selected @endif>Available to all visitors</option>
														<option value="restrict_to_subscribers" @if(old('article_availability')=="restrict_to_subscribers") selected="true"@elseif(isset($webinar->article_availability) && $webinar->article_availability == 'restrict_to_subscribers') selected @endif>Restrict to subscribers</option>
													</select>
													@if($errors->has('article_availability'))
		                                      			<span class="text-danger">{{$errors->first('article_availability')}}</span>
		                                    		@endif
					  							</div>
					  						</div>
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
					  							<div class="form-group">
					  								<label>Layout and photo</label>
					  								<select name="layout_photo" id="content_type" tabindex="1" class="form-control" style="width: 100%; height:36px;">
					  									<option value="">Select layout and photo</option>
					  									<option value="left_picture" @if(old('layout_photo')=="left_picture") selected="true" @elseif(isset($webinar->layout_photo) && $webinar->layout_photo == 'left_picture') selected @endif>Left pictrue</option>
														<option value="right_picture" @if(old('layout_photo')=="right_picture") selected="true" @elseif(isset($webinar->layout_photo) && $webinar->layout_photo == 'right_picture') selected @endif>Right picture</option>
														<option value="no_picture" @if(old('layout_photo')=="no_picture") selected="true" @elseif(isset($webinar->layout_photo) && $webinar->layout_photo == 'no_picture') selected @endif>No picture</option>
													</select>
													@if($errors->has('layout_photo'))
		                                      			<span class="text-danger">{{$errors->first('layout_photo')}}</span>
		                                    		@endif
					  							</div>
					  						</div>
					  					</div>
										<div class="row">
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
					  							<div class="form-group">
					  								<label>Related Content 1</label>
					  								<select name="related_content_one" id="content_type" tabindex="1" class="form-control" style="width: 100%; height:36px;">
					  									<option value="">Select related content one</option>
					  									<option value="wine_competition" @if(old('related_content_one')=="wine_competition") selected="true" @elseif(isset($webinar->related_content_one) && $webinar->related_content_one == 'wine_competition') selected @endif> Wine competition</option>
														<option value="consultant_page" @if(old('related_content_one')=="consultant_page") selected="true" @elseif(isset($webinar->related_content_one) && $webinar->related_content_one == 'consultant_page') selected @endif>Consultant page</option>
														<option value="lessons" @if(old('related_content_one')=="lessons") selected="true" @elseif(isset($webinar->related_content_one) && $webinar->related_content_one == 'lessons') selected @endif>Lessons</option>
													</select>
													@if($errors->has('related_content_one'))
		                                      			<span class="text-danger">{{$errors->first('related_content_one')}}</span>
		                                    		@endif
					  							</div>
					  						</div>
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
					  							<div class="form-group">
					  								<label>Related Content 2</label>
					  								<select name="related_content_two" id="content_type" tabindex="1" class="form-control" style="width: 100%; height:36px;">
					  									<option value="">Select related content two</option>
					  									<option value="press_release" @if(old('related_content_two')=="press_release") selected="true" @elseif(isset($webinar->related_content_two) && $webinar->related_content_two == 'press_release') selected @endif>Press release</option>
														<option value="content_detail" @if(old('related_content_two')=="content_detail") selected="true" @elseif(isset($webinar->related_content_two) && $webinar->related_content_two == 'content_detail') selected @endif>Content detail</option>
														<option value="content_post" @if(old('related_content_two')=="content_post") selected="true" @elseif(isset($webinar->related_content_two) && $webinar->related_content_two == 'content_post') selected @endif>Content post</option>
													</select>
													@if($errors->has('related_content_two'))
		                                      			<span class="text-danger">{{$errors->first('related_content_two')}}</span>
		                                    		@endif
					  							</div>
					  						</div>
					  					</div>
					  					<div class="row">
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
					  							<div class="form-group">
					  								<label>Related Content 3</label>
					  								<select name="related_content_three" id="content_type" tabindex="1" class="form-control" style="width: 100%; height:36px;">
					  									<option value="">Select related content three</option>
					  									<option value="related_item"@if(old('related_content_three')=="related_item") selected="true" @elseif(isset($webinar->related_content_three) && $webinar->related_content_three == 'related_item') selected @endif>Related item</option>
														<option value="related_colunm" @if(old('related_content_three')=="related_colunm") selected="true" @elseif(isset($webinar->related_content_three) && $webinar->related_content_three == 'related_colunm') selected @endif>Related colunm</option>
														<option value="user_related_data" @if(old('related_content_three')=="user_related_data") selected="true" @elseif(isset($webinar->related_content_three) && $webinar->related_content_three == 'user_related_data') selected @endif>User related data</option>
													</select>
													@if($errors->has('related_content_three'))
		                                      			<span class="text-danger">{{$errors->first('related_content_three')}}</span>
		                                    		@endif
					  							</div>
					  						</div>
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
					  							<div class="form-group">
					  								<label>Related Content 4</label>
					  								<select name="related_content_four" id="content_type" tabindex="1" class="form-control" style="width: 100%; height:36px;">
					  									<option value="">Select related content four</option>
					  									<option value="client_data"@if(old('related_content_four')=="client_data") selected="true" @elseif(isset($webinar->related_content_four) && $webinar->related_content_four == 'client_data') selected @endif>Client data</option>
														<option value="client_webinar" @if(old('related_content_four')=="client_webinar") selected="true" @elseif(isset($webinar->related_content_four) && $webinar->related_content_four == 'client_webinar') selected @endif>Client webinar</option>
														<option value="distribution" @if(old('related_content_four')=="distribution") selected="true" @elseif(isset($webinar->related_content_four) && $webinar->related_content_four == 'distribution') selected @endif>Distribution</option>
													</select>
													@if($errors->has('related_content_four'))
		                                      			<span class="text-danger">{{$errors->first('related_content_four')}}</span>
		                                    		@endif
					  							</div>
					  						</div>
					  					</div>
					  					<div class="row">
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
					  							<div class="form-group">
					  								<label>Webinar Status</label>
					  								<select name="webinar_status" id="content_type" tabindex="1" class="form-control" style="width: 100%; height:36px;">
					  									<option value="">Select webinar status</option>
					  									<option value="growth_prospects" @if(old('webinar_status')=="growth_prospects") selected="true" @elseif(isset($webinar->webinar_status) && $webinar->webinar_status == 'growth_prospects') selected @endif>Growth prospects</option>
														<option value="building_strategy" @if(old('webinar_status')=="building_strategy") selected="true" @elseif(isset($webinar->webinar_status) && $webinar->webinar_status == 'building_strategy') selected @endif>Building strategy</option>
														<option value="brand_idea" @if(old('webinar_status')=="brand_idea") selected="true" @elseif(isset($webinar->webinar_status) && $webinar->webinar_status == 'brand_idea') selected @endif>Brand idea</option>
													</select>
													@if($errors->has('webinar_status'))
		                                      			<span class="text-danger">{{$errors->first('webinar_status')}}</span>
		                                    		@endif
					  							</div>
					  						</div>
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
					  							<div class="form-group">
													<label>Broadcast Day</label>
													<input type="text" tabindex="5" name="broadcast_day" class="form-control number" maxlength="50" value="@if(isset($webinar->broadcast_day)){{$webinar->broadcast_day}} @elseif(old('broadcast_day')!=''){{ old('broadcast_day')}}  @endif" placeholder="Broadcast Day">
													@if($errors->has('broadcast_day'))
		                                      			<span class="text-danger">{{$errors->first('broadcast_day')}}</span>
		                                    		@endif
												</div>
					  						</div>
					  					</div>
					  					<div class="row">
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
					  							<div class="form-group">
													<label>Broadcast Month</label>
													<input type="text" tabindex="5" name="broadcast_month" class="form-control number" maxlength="50" value="@if(isset($webinar->broadcast_month)){{$webinar->broadcast_month}} @elseif(old('broadcast_month')!=''){{ old('broadcast_month')}} @endif" placeholder="Broadcast Month">
													@if($errors->has('broadcast_month'))
		                                      			<span class="text-danger">{{$errors->first('broadcast_month')}}</span>
		                                    		@endif
												</div>
					  						</div>
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
					  							<div class="form-group">
													<label>Broadcast Year</label>
													<input type="text" tabindex="5" name="broadcast_year" class="form-control number" maxlength="50" value="@if(isset($webinar->broadcast_year)){{$webinar->broadcast_year}} @elseif(old('broadcast_year')!=''){{ old('broadcast_year')}} @endif" placeholder="Broadcast Year">
													@if($errors->has('broadcast_year'))
		                                      			<span class="text-danger">{{$errors->first('broadcast_year')}}</span>
		                                    		@endif
												</div>
					  						</div>
					  					</div>
					  					<div class="row">
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
					  							<div class="form-group">
					  								<label>Broadcast Hour</label>
					  								<select name="broadcast_hour" id="content_type" tabindex="1" class="form-control" style="width: 100%; height:36px;">
					  									<option value="">Select hour</option>
														<option value="0" @if(old('broadcast_hour')=="0") selected="true" @elseif(isset($webinar->broadcast_hour) && $webinar->broadcast_hour == '0') selected @endif>0</option>
														<option value="1" @if(old('broadcast_hour')=="1") selected="true" @elseif(isset($webinar->broadcast_hour) && $webinar->broadcast_hour == '1') selected @endif>1</option>
														<option value="2" @if(old('broadcast_hour')=="2") selected="true" @elseif(isset($webinar->broadcast_hour) && $webinar->broadcast_hour == '2') selected @endif>2</option>
														<option value="3" @if(old('broadcast_hour')=="3") selected="true" @elseif(isset($webinar->broadcast_hour) && $webinar->broadcast_hour == '3') selected @endif>3</option>
													</select>
													@if($errors->has('broadcast_hour'))
		                                      			<span class="text-danger">{{$errors->first('broadcast_hour')}}</span>
		                                    		@endif
					  							</div>
					  						</div>
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
					  							<div class="form-group">
					  								<label>Broadcast Minutes</label>
					  								<select name="broadcast_minute" id="content_type" tabindex="1" class="form-control" style="width: 100%; height:36px;">
					  									<option value="">Select minute</option>
														<option value="00"@if(old('broadcast_minute')=="00") selected="true" @elseif(isset($webinar->broadcast_minute) && $webinar->broadcast_minute == '00') selected @endif>00</option>
														<option value="15"@if(old('broadcast_minute')=="15") selected="true" @elseif(isset($webinar->broadcast_minute) && $webinar->broadcast_minute == '15') selected @endif>15</option>
														<option value="25"@if(old('broadcast_minute')=="25") selected="true" @elseif(isset($webinar->broadcast_minute) && $webinar->broadcast_minute == '25') selected @endif>25</option>
														<option value="35"@if(old('broadcast_minute')=="35") selected="true" @elseif(isset($webinar->broadcast_minute) && $webinar->broadcast_minute == '35') selected @endif>35</option>
													</select>
													@if($errors->has('broadcast_minute'))
		                                      			<span class="text-danger">{{$errors->first('broadcast_minute')}}</span>
		                                    		@endif
					  							</div>
					  						</div>
					  					</div>
					  					<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Webinars Registration Link</label>
													<input type="text" name="webinars_registration_link" placeholder="Webinars Registration Link" tabindex="23" class="form-control" maxlength="50" value="@if(isset($webinar->webinars_registration_link)){{$webinar->webinars_registration_link}} @elseif(old('webinars_registration_link')!=''){{ old('webinars_registration_link')}} @endif">
													@if($errors->has('webinars_registration_link'))
		                                      			<span class="text-danger">{{$errors->first('webinars_registration_link')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Webinars Youtube Video Id</label>
													<input type="text" name="webinars_youtube_video" placeholder="Webinars Youtube Video Id" tabindex="23" class="form-control" maxlength="50" value="@if(isset($webinar->webinars_youtube_video)){{$webinar->webinars_youtube_video}} @elseif(old('webinars_youtube_video')!=''){{ old('webinars_youtube_video')}} @endif">
													@if($errors->has('webinars_youtube_video'))
		                                      			<span class="text-danger">{{$errors->first('webinars_youtube_video')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Webinar Slideshare Embed Code</label>
													<textarea name="webinar_slideshare_embed_code" placeholder="Webinar Slideshare Embed Code" tabindex="43" maxlength="100" class="form-control">@if(isset($webinar->webinar_slideshare_embed_code)){{$webinar->webinar_slideshare_embed_code}} @elseif(old('webinar_slideshare_embed_code')!=''){{ old('webinar_slideshare_embed_code')}} @endif</textarea>
													@if($errors->has('webinar_slideshare_embed_code'))
		                                      			<span class="text-danger">{{$errors->first('webinar_slideshare_embed_code')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Tags English</label>
													<select name="tags_english[]" id="tags_english" tabindex="26" class="form-control select2" multiple="multiple">
														@if(isset($tags_english))
															@foreach($tags_english as $key => $value)
															<option
																 selected= "selected" value="{{ $value }}"
															>{{$value}}</option>
															@endforeach
														@else
															<option></option>	
														@endif
													</select>
													@if($errors->has('tags_english'))
		                                      			<span class="text-danger">{{$errors->first('tags_english')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Title English</label>
													<input type="text" name="title_english" placeholder="Title English" tabindex="23" class="form-control" maxlength="50" value="@if(isset($webinar->title_english)){{$webinar->title_english}} @elseif(old('title_english')!=''){{ old('title_english')}} @endif">
													@if($errors->has('title_english'))
		                                      			<span class="text-danger">{{$errors->first('title_english')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Sub Title English</label>
													<textarea name="sub_title_english" placeholder="Sub Title English" tabindex="43" maxlength="100" class="form-control">@if(isset($webinar->sub_title_english)){{$webinar->sub_title_english}} @elseif(old('sub_title_english')!=''){{ old('sub_title_english')}} @endif</textarea>
													@if($errors->has('sub_title_english'))
		                                      			<span class="text-danger">{{$errors->first('sub_title_english')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>SEO page title</label>
													<input type="text" tabindex="42" name="seo_title" class="form-control" placeholder="SEO page title" maxlength="50" value="@if(isset($seo->page_title)){{ $seo->page_title }}@elseif(old('seo_title')!=''){{ old('seo_title') }} @endif">
													@if($errors->has('seo_title'))
		                                      			<span class="text-danger">{{$errors->first('seo_title')}}</span>
		                                    		@endif
												</div>
											</div>
					  						<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>SEO page description</label>
													<textarea name="seo_page_description" placeholder="SEO page description" maxlength="100" tabindex="43" class="form-control">@if(isset($seo->page_description)){{$seo->page_description}}@elseif(old('seo_page_description')!=''){{old('seo_page_description')}} @endif</textarea>
													@if($errors->has('seo_page_description'))
		                                      			<span class="text-danger">{{$errors->first('seo_page_description')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>SEO page keywords</label>
													<select name="seo_page_keywords[]" id="seo_page_keywords" tabindex="44" class="form-control select2" multiple="multiple">
														@if(isset($page_keywords))
															@foreach($page_keywords as $key => $value)
															<option
																 selected= "selected" value="{{ $value }}"
															>{{$value}}</option>
															@endforeach
														@else
															<option></option>	
														@endif
													</select>
													@if($errors->has('seo_page_keywords'))
		                                      			<span class="text-danger">{{$errors->first('seo_page_keywords')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Teaser English<span class="red">*</span></label>
													<textarea class="form-control" tabindex="46" id="content1" name="teaser_english">@if(isset($webinar->teaser_english)){{$webinar->teaser_english}} @elseif(old('teaser_english')!=''){{ old('teaser_english')}} @endif
					                           		</textarea>
					                           		@if($errors->has('teaser_english'))
		                                      			<span class="text-danger">{{$errors->first('teaser_english')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Body English<span class="red">*</span></label>
													<textarea class="form-control" tabindex="46" id="content2" name="body_english">@if(isset($webinar->teaser_english)){{$webinar->teaser_english}} @elseif(old('body_english')!=''){{ old('body_english')}} @endif</textarea>
													@if($errors->has('body_english'))
		                                      			<span class="text-danger">{{$errors->first('body_english')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Text Box English<span class="red">*</span></label>
													<textarea class="form-control" tabindex="46" id="content3" name="text_box_english">@if(isset($webinar->text_box_english)){{$webinar->text_box_english}} @elseif(old('text_box_english')!=''){{ old('text_box_english')}} @endif
					                           		</textarea>
					                           		@if($errors->has('text_box_english'))
		                                      			<span class="text-danger">{{$errors->first('text_box_english')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Audio Description English<span class="red">*</span></label>
													<textarea class="form-control" tabindex="46" id="content4" name="audio_description_english">@if(isset($webinar->audio_description_english)){{$webinar->audio_description_english}} @elseif(old('audio_description_english')!=''){{ old('audio_description_english')}} @endif
					                           		</textarea>
					                           		@if($errors->has('audio_description_english'))
		                                      			<span class="text-danger">{{$errors->first('audio_description_english')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Video Index English<span class="red">*</span></label>
													<textarea class="form-control" tabindex="46" id="content5" name="video_index_english">@if(isset($webinar->video_index_english)){{$webinar->video_index_english}} @elseif(old('video_index_english')!=''){{ old('video_index_english')}} @endif
					                           		</textarea>
					                           		@if($errors->has('video_index_english'))
		                                      			<span class="text-danger">{{$errors->first('video_index_english')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="file-upload">
													<div class="file-select">
														<div class="file-select-button" id="fileName">Main Image<span class="red">*</span></div>
														<div class="file-select-name" id="noFile">No file chosen...</div>
														<input type="file" name="main_image" tabindex="30" id="image" accept="image/*" value="" >
													</div>
												</div>
												@if($errors->has('main_image'))
	                                      			<span class="text-danger">{{$errors->first('image')}}</span>
	                                    		@endif
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="file-upload">
													<div class="file-select">
														<div class="file-select-button" id="fileName">Webinar Poster</div>
														<div class="file-select-name" id="noFile_additonal">No file chosen...</div>
														<input type="file" name="webinar_poster" tabindex="31" id="additional_image" accept="image/*" value="" multiple="">
													</div>
												</div>
												@if($errors->has('webinar_poster'))
	                                      			<span class="text-danger">{{$errors->first('webinar_poster')}}</span>
	                                    		@endif
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div id="image_beside">
													@if(isset($webinar->main_image))
													<img src="{{url($webinar->main_image)}}" id="edit_image" height="200px" width="200px">
													@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div id="image_additional_beside">
													@if(isset($webinar->webinar_poster))
														<img src="{{url($webinar->webinar_poster)}}" id="edit_additional_image" height="200px" width="200px">
													@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="file-upload">
													<div class="file-select">
														<div class="file-select-button" id="fileName">Audio File Upload</div>
														<div class="file-select-name" id="noFile">No file chosen...</div>
														<input type="file" name="audio_file" tabindex="32" id="header_image" accept="image/*" value="" >
													</div>
												</div>
												@if($errors->has('audio_file'))
	                                      			<span class="text-danger">{{$errors->first('audio_file')}}</span>
	                                    		@endif
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div id="image_additional_beside">
													@if(isset($webinar->audio_file))
													<img src="{{url($webinar->audio_file)}}" id="edit_additional_image" height="200px" width="200px">
													@endif
												</div>
											</div>
										</div>
										<div class="row" style="margin-top: 10px">
											<div class="col-sm-12">
												<div class="submit-btn">
													<button type="submit" tabindex="48" class="btn btn-submit">Submit</button>
												</div>
											</div>
										</div>
					  				</form>
					  			</div>
					  		</div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@push('js')
<script type="text/javascript" src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script type="text/javascript" src="{{url('js/custom/custom.js')}}"></script>
<script type="text/javascript">
	CKEDITOR.replace( 'content2' );
	CKEDITOR.replace( 'content3' );
	CKEDITOR.replace( 'content4' );
	CKEDITOR.replace( 'content5' );
	$(document).ready(function(){
		$('#additional_image').change(function(){
        	readURL(this);
    	});
		function readURL(input) {
    	if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function (e) {
	            $('#noFile_additonal').empty();
	            $('#edit_additional_image').remove();
	            $('#image_additional_beside').empty();
	            $("#image_additional_beside").append('<div class="row">'+
	                '<img src ='+e.target.result+' width="250px" height="240px" >'+
	                    '</div>'+
	            '</div>');
	        }
	        reader.readAsDataURL(input.files[0]);
	    }
	}
	$('#publication_date').datepicker({
        autoclose: true,  
        format: "dd-mm-yyyy",
        todayHighlight: true,
    });
	 $('.number').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });
})
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfDJaVdwAS-KTBw3WIyMPV2CcrTJAUKFI&libraries=places&callback=initAutocomplete" async defer></script>
@endpush
