@extends('layouts.admin.layout')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage Events</h4> 
			</div>
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-tab">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							@if(isset($event->id))
								<li class="active"><a>Edit Events</a></li>
							@else
								<li class="active"><a>Add Events</a></li>
							@endif
							<li><a href="{{ route('admin.events.list') }}">Events Listing</a></li>
					  	</ul>
					  	<div class="tab-content">
							<div id="AddProcucts" class="tab-pane fade in active">
								<div class="add-product">
								   	<form id="create_product" action="{{ route('admin.events.store') }}" method="post" class="form" enctype="multipart/form-data">
									{{ csrf_field() }}
									@if(isset($event->id))
				            			<input type="hidden" name="id" value="{{$event->id}}">
				            			<input type="hidden" name="seo_id" value="{{$event->seo_id}}">
		            				@else
				            			<input type="hidden" name="id" value="">
			            			@endif
			            				<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Title<span class="red">*</span></label>
													<input type="text" tabindex="1" name="title" class="form-control" placeholder="Title Event" maxlength="500" value="@if(isset($event->title)){{ $event->title }}@elseif(old('title')!=''){{ old('title') }} @endif">
													@if($errors->has('title'))
		                                      			<span class="text-danger">{{$errors->first('title')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Event description<span class="red">*</span></label>
													<textarea class="form-control" tabindex="2" id="content1" name="description">
														@if(isset($event->description)){{$event->description}}@elseif(old('description')!=''){{old('description')}} @endif
					                           		</textarea>
													@if($errors->has('description'))
		                                      			<span class="text-danger">{{$errors->first('description')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Category<span class="red">*</span></label>
													<select name="category_id" id="category_id" tabindex="3" class="form-control" style="width: 100%; height:36px;">
														<option value="">Select Category</option>
														@foreach($categories as $data)
															<option value="{{ $data->id }}"
															@if(old('category_id') == $data->id)
	                                						selected="true"
	                                						@endif
					                                        @if(isset($event->category) && $data->id== $event->category))
					                                            selected="true"
					                                        @endif
					                                        >{{ $data->name }}</option>
					                                    @endforeach
													</select>
													@if($errors->has('category_id'))
		                                      			<span class="text-danger">{{$errors->first('category_id')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>SEO page title</label>
													<input type="text" name="seo_page_title" tabindex="18" class="form-control" placeholder="SEO Page Title" maxlength="500" value="@if(isset($seo->page_title)){{ $seo->page_title }}@elseif(old('seo_page_title')!=''){{ old('seo_page_title') }} @endif">
													@if($errors->has('seo_page_title'))">
		                                      			<span class="text-danger">{{$errors->first('seo_page_title')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Priority<span class="red">*</span></label>
													<select name="priority" class="form-control" tabindex="4">
														<option value="">Select Priority</option>
														<option value="highest" @if(old('priority')=="highest") selected="true" @elseif(isset($event->priority) && $event->priority=="highest") selected="true" @endif>Sticky (Highest)</option>
														<option value="high" @if(old('priority')=="high") selected="true" @elseif(isset($event->priority) && $event->priority=="high") selected="true" @endif>High</option>
														<option value="normal" @if(old('priority')=="normal") selected="true" @elseif(isset($event->priority) && $event->priority=="normal") selected="true" @endif>Normal</option>
													</select>
													@if($errors->has('priority'))
		                                      			<span class="text-danger">{{$errors->first('priority')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>SEO page description</label>
													<textarea name="seo_page_description" class="form-control" tabindex="19" placeholder="SEO Page Description">@if(isset($seo->page_description)){{$seo->page_description}}@elseif(old('page_description')!=''){{old('page_description')}} @endif</textarea>
													@if($errors->has('seo_page_description'))
		                                      			<span class="text-danger">{{$errors->first('seo_page_description')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										@php
											if(isset($event->section_tags)){
												$section_tags = explode(',',$event->section_tags);
											}
											if(isset($seo->page_keywords)){
												$page_keywords = explode(',',$seo->page_keywords);
											}
											if(isset($seo->public_metatags)){
												$public_metatags = explode(',',$seo->public_metatags);
											}
										@endphp
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Section Tags<span class="red">*</span></label>
													<select name="section_tags[]" id="section_tags" tabindex="5" class="form-control select2" multiple="multiple">
														@if(isset($section_tags))
															@foreach($section_tags as $key => $value)
															<option
																 selected= "selected" value="{{ $value }}"
															>{{$value}}</option>
															@endforeach
														@else
															<option></option>	
														@endif
													</select>
													@if($errors->has('section_tags'))
		                                      			<span class="text-danger">{{$errors->first('section_tags')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>SEO Page Keywords</label>
													<select name="seo_page_keyword[]" tabindex="20" id="seo_page_keyword" class="form-control select2" multiple="multiple">
														@if(isset($page_keywords))
															@foreach($page_keywords as $key => $value)
															<option
																 selected= "selected" value="{{ $value }}"
															>{{$value}}</option>
															@endforeach
														@else
															<option></option>	
														@endif
													</select>
													@if($errors->has('seo_page_keyword'))
		                                      			<span class="text-danger">{{$errors->first('seo_page_keyword')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Start Date<span class="red">*</span></label>
													<input type="text" name="start_date" tabindex="6" id="start_date" placeholder="dd-mm-yyyy" class="form-control"value="@if(isset($event->start_date)){{date('d-m-Y',strtotime($event->start_date))}}@elseif(old('start_date')!=''){{ old('start_date') }} @endif">
													@if($errors->has('start_date'))
		                                      			<span class="text-danger">{{$errors->first('start_date')}}</span>
		                                    		@endif
												</div>
												
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Public Metatags</label>
													<select  name="public_metatags[]" tabindex="21" id="public_metatags" class="form-control select2" multiple="multiple">
														@if(isset($public_metatags))
															@foreach($public_metatags as $key => $value)
															<option
																 selected= "selected" value="{{ $value }}"
															>{{$value}}</option>
															@endforeach
														@else
															<option></option>	
														@endif
													</select>
													@if($errors->has('public_metatags'))
		                                      			<span class="text-danger">{{$errors->first('public_metatags')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>End Date<span class="red">*</span></label>
													<input type="text" name="end_date" tabindex="22" id="end_date" placeholder="dd-mm-yyyy" class="form-control"value="@if(isset($event->end_date)){{date('d-m-Y',strtotime($event->end_date))}}@elseif(old('end_date')!=''){{ old('end_date') }} @endif">
													@if($errors->has('end_date'))
		                                      			<span class="text-danger">{{$errors->first('end_date')}}</span>
		                                    		@endif
												</div>
												
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<label>Status<span class="red">*</span></label>
												<select tabindex="12" class="form-control" name="status">
													<option value="">Select status</option>
													<option value="1"@if(old('status')=="1") selected="true" @elseif(isset($event->status) && $event->status=="1") selected="true" @endif>Active</option>
													<option value="0"@if(old('status')=="0") selected="true" @elseif(isset($event->status) && $event->status=="0") selected="true" @endif>InActive</option>
												</select>
												@if($errors->has('status'))
	                                      			<span class="text-danger">{{$errors->first('status')}}</span>
	                                    		@endif
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Event Map</label>
													<textarea class="form-control" tabindex="8" placeholder="Event Map" id="event_map" name="event_map">@if(isset($event->event_map)){{$event->event_map}}@elseif(old('event_map')!=''){{old('event_map')}} @endif</textarea>
					                           		@if($errors->has('event_map'))
		                                      			<span class="text-danger">{{$errors->first('event_map')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Event Location</label>
													<input type="text" name="event_location" tabindex="9" maxlength="200" class="form-control" placeholder="Event Location" value="@if(isset($event->event_location)){{$event->event_location}}@elseif(old('event_location')!=''){{ old('event_location') }} @endif">
													@if($errors->has('event_location'))
		                                      			<span class="text-danger">{{$errors->first('event_location')}}</span>
		                                    		@endif	
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Event Opening Hours </label>
													<input type="text" name="event_opening_hour" maxlength="200" tabindex="10" class="form-control" placeholder="Event Opening Hours" maxlength="100" value="@if(isset($event->event_opening_hour)){{$event->event_opening_hour}}@elseif(old('event_opening_hour')!=''){{ old('event_opening_hour') }} @endif">
													@if($errors->has('event_opening_hour'))
		                                      			<span class="text-danger">{{$errors->first('event_opening_hour')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Organizer profile</label>
													<input type="text" name="organizer_profile" maxlength="200" tabindex="11" class="form-control" placeholder="Organizer Profile" maxlength="1000" value="@if(isset($event->organizer_profile)){{$event->organizer_profile}}@elseif(old('organizer_profile')!=''){{ old('organizer_profile') }} @endif">
													@if($errors->has('organizer_profile'))
		                                      			<span class="text-danger">{{$errors->first('organizer_profile')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Further info URL</label>
													<span>( http:// format)</span>
													<input type="text" name="further_info_url" tabindex="12" maxlength="200" class="form-control" placeholder="Further info URL" maxlength="100" value="@if(isset($event->further_info_url)){{$event->further_info_url}}@elseif(old('further_info_url')!=''){{ old('further_info_url') }} @endif">
													@if($errors->has('further_info_url'))
		                                      			<span class="text-danger">{{$errors->first('further_info_url')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Further info Email</label>
													<span>( http:// format)</span>
													<input type="text" name="further_info_email" maxlength="200" tabindex="13" class="form-control" placeholder="Further info Email" maxlength="100" value="@if(isset($event->further_info_email)){{$event->further_info_email}}@elseif(old('further_info_email')!=''){{ old('further_info_email') }} @endif">
													@if($errors->has('further_info_email'))
		                                      			<span class="text-danger">{{$errors->first('further_info_email')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Registration URL</label>
													<input type="text" name="registration_url" maxlength="200" tabindex="14" class="form-control" placeholder="Registration URL" maxlength="100" value="@if(isset($event->registration_url)){{$event->registration_url}}@elseif(old('registration_url')!=''){{ old('registration_url') }} @endif">
													@if($errors->has('registration_url'))
		                                      			<span class="text-danger">{{$errors->first('registration_url')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-4 col-xs-12">
												<div class="form-group">
													<label>Cost/Fee</label>
													<input type="text" name="cost" maxlength="200" class="number  form-control" placeholder="Cost/Fee" tabindex="15" maxlength="100" value=" @if(isset($event->cost)){{$event->cost}}@elseif(old('cost')!=''){{ old('cost') }} @endif">
													@if($errors->has('cost'))
		                                      			<span class="text-danger">{{$errors->first('cost')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<div class="form-group">
														<label>Layout and photo</label>
														<select name="layouts_image" class="form-control" tabindex="16" name="layouts_image">
															<option value="left"
															@if(old('layouts_image')=='left') selected="true" @elseif(isset($event->layout_image) && $event->layout_image=="left") selected="true" @endif>Left</option>
															<option value="right"
															@if(old('layouts_image')=='right') selected="true" @elseif(isset($event->layout_image) && $event->layout_image=="right") selected="true" @endif>Right</option>
															<option value="center"
															@if(old('layouts_image')=='center') selected="true" @elseif(isset($event->layout_image) && $event->layout_image=="center") selected="true" @endif>Center</option>
														</select>
														@if($errors->has('layouts_image'))
			                                      			<span class="text-danger">{{$errors->first('layouts_image')}}</span>
			                                    		@endif
													</div>			
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="file-upload">
													<div class="file-select">
														<div class="file-select-button" id="fileName">Choose Image <span class="red">*</span></div>
														<div class="file-select-name" id="noFile">No file chosen...</div>
														<input tabindex="17" type="file" name="image" id="image" accept="image/*" value="" >
													</div>
												</div>
												@if($errors->has('image'))
	                                      			<span class="text-danger">{{$errors->first('image')}}</span>
	                                    		@endif
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<div id="image_beside">
														@if(isset($event->image))
														<img src="{{url($event->image)}}" height="300px" id="edit_image" width="300px">
														@endif
													</div>
												</div>
											</div>
										</div>
										
										<div class="row" style="margin-top: 10px">
											<div class="col-sm-12">
												<div class="submit-btn">
													<button type="submit" class="btn btn-submit">Submit</button>
												</div>
											</div>
										</div>
								 	</form>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@push('js')
<script type="text/javascript" src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script type="text/javascript" src="{{url('js/custom/custom.js')}}"></script>
<script type="text/javascript">
    $('#start_date').datepicker({
        autoclose: true,  
        format: "dd-mm-yyyy",
        todayHighlight: true,

    });
    $('#end_date').datepicker({
        autoclose: true,  
        format: "dd-mm-yyyy",
        todayHighlight: true,

    });
    
</script>
@endpush