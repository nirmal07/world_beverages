@extends('layouts.admin.layout')
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Create User</h4> 
            </div>
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="product-tab">
                        <ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
                            @if(isset($customer->id))
                                <li class="active"><a>Edit User</a></li>
                            @else
                                <li class="active"><a>Add User</a></li>
                            @endif
                            <li><a href="{{ route('admin.customers.index') }}">User Listing</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="AddProcucts" class="tab-pane fade in active">
                                <div class="add-product">
                                <!-- Main content -->
                                    <section class="content">
                                        <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                                            <div class="box">
                                                <form action="{{ route('admin.customers.store') }}" method="post" class="form">
                                                    <input type="hidden" name="id" value="@if(isset($customer->id)){{ $customer->id }}@elseif(old('id')!=''){{ old('id') }} @endif" maxlength="50">
                                                    <div class="box-body">
                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <label for="username">Username <span class="text-danger">*</span></label>
                                                            <input type="text" name="username" id="username" placeholder="Username" class="form-control" value="@if(isset($customer->username)){{ $customer->username }}@elseif(old('username')!=''){{ old('username') }} @endif">
                                                            @if($errors->has('username'))
                                                                <span class="text-danger">{{$errors->first('username')}}</span>
                                                            @endif
                                                        </div>
                                                        @if(isset($customer->id))
                                                        <div class="form-group">
                                                            <label>Create New Password</label>
                                                            <input type="password" name="new_password" id="new_password" class="form-control" placeholder="****" maxlength="15">
                                                            @if($errors->has('password'))
                                                                <span class="text-danger">{{$errors->first('password')}}</span>
                                                            @endif
                                                        </div>
                                                        @else
                                                        <div class="form-group">
                                                            <label for="password">Password <span class="text-danger">*</span></label>
                                                            <input type="password" name="password" id="password" placeholder="xxxxx" class="form-control" maxlength="15">
                                                            @if($errors->has('password'))
                                                                <span class="text-danger">{{$errors->first('password')}}</span>
                                                            @endif
                                                        </div>
                                                        @endif
                                                        <div class="form-group">
                                                            <label for="name">Name <span class="text-danger">*</span></label>
                                                            <input type="text" name="name" id="name" placeholder="Name" class="form-control" value="@if(isset($customer->name)){{ $customer->name }}@elseif(old('name')!=''){{ old('name') }} @endif" maxlength="100">
                                                            @if($errors->has('name'))
                                                                <span class="text-danger">{{$errors->first('name')}}</span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="surname">Surname <span class="text-danger">*</span></label>
                                                            <input type="text" name="surname" id="surname" placeholder="Surname" class="form-control" value="@if(isset($customer->surname)){{ $customer->surname }}@elseif(old('surname')!=''){{ old('surname') }} @endif" maxlength="50">
                                                            @if($errors->has('surname'))
                                                                <span class="text-danger">{{$errors->first('surname')}}</span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="country">Address Country: <span class="text-danger">*</span></label>
                                                            <select name="country" class="form-control">
                                                                <option value="">Select Country</option>
                                                                @foreach ($countrys as $country)
                                                                <option value="{{ $country->id }}"
                                                                @if(old('country')==$country->id) selected="true"
                                                                @elseif(isset($customer->country) && $customer->country==$country->id)
                                                                selected="true"
                                                                @endif>{{ $country->country }}</option>
                                                                @endforeach
                                                            </select>
                                                            @if($errors->has('country'))
                                                                <span class="text-danger">{{$errors->first('country')}}</span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="surname">Landline <span class="text-danger">*</span></label>
                                                            <input type="text" name="phone" id="number" placeholder="Landline Number" maxlength="15" class="form-control number" value="@if(isset($customer->phone)){{ $customer->phone }}@elseif(old('phone')!=''){{ old('phone') }} @endif">
                                                            @if($errors->has('phone'))
                                                                <span class="text-danger">{{$errors->first('phone')}}</span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="email">Email <span class="text-danger">*</span></label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">@</span>
                                                                <input type="text" name="email" id="email" placeholder="Email" maxlength="50" class="form-control" value="@if(isset($customer->email)){{ $customer->email }}@elseif(old('email')!=''){{ old('email') }} @endif">
                                                            </div>
                                                            @if($errors->has('email'))
                                                                <span class="text-danger">{{$errors->first('email')}}</span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="email">Comments/ Remarks<span class="text-danger"></span></label>
                                                            <textarea name="comments" maxlength="500" class="form-control">@if(isset($customer->comments)){{ $customer->comments }}@elseif(old('comments')!=''){{ old('comments') }} @endif</textarea>
                                                           <!--  @if($errors->has('comments'))
                                                                <span class="text-danger">{{$errors->first('comments')}}</span>
                                                            @endif -->
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="status">Status </label>
                                                            <select name="status" id="status" class="form-control">
                                                                <option value="0"   @if(old('status')=="0") selected="true" @elseif(isset($customer->status) && $customer->status==0)selected="true"@endif>Disable</option>
                                                                <option value="1"
                                                                   @if(old('status')=="1") selected="true" @elseif(isset($customer->status) && $customer->status==1)selected="true"@endif>Enable</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <!-- /.box-body -->
                                                    <div class="box-footer">
                                                        <div class="btn-group">
                                                            <a href="{{ route('admin.dashboard') }}" class="btn btn-default">Back</a>
                                                            @if(isset($customer->id))
                                                            <button type="submit" class="btn btn-primary">Update</button>
                                                            @else
                                                            <button type="submit" class="btn btn-primary">Create</button>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{url('js/custom/custom.js')}}"></script>
@endpush

