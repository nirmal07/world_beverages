@extends('layouts.admin.layout')
@section('content')
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Manage User</h4>
        </div>
    </div>
    @include('layouts.errors-and-messages')
    <div class="page-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="customer-tab">                          
                    <ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
    					<li class="active"><a  href="{{route('admin.customers.index')}}">User Listing</a></li>
    					<li><a href="{{route('admin.customers.create')}}">User Create </a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="ProductListing" class="tab-pane fade in active">
                            <div class="product-listing">
                                <div class="table-responsive">
                                <table  class="table border-b1px customers" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>E-mail</th>
                                        <th>Country</th>
                                        <th>Phone</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($customers as  $customer)
                                        <tr>
                                            <td>{{ $customer->name}}</td>
                                            <td>{{ $customer->email }}</td>
                                            <td>{{ $customer->country_name }}</td>
                                            <td>{{ $customer->phone }}</td>
                                            <td>
                                            <form action="{{route('admin.customers.is_active',$customer->id)}}" method="post" class="form-horizontal">
                                                {{ csrf_field() }}
                                                @if($customer->status == "1")
                                                    <button class="btn in-active-status" onclick="return confirm('Are you sure? You want to change status.')">Active</button>
                                                @else
                                                    <button class="btn in-active-status" onclick="return confirm('Are you sure? You want to change status.')">InActive</button>
                                                @endif
                                            </form>
                                            </td>
                                            <td>
                                                <div class="action-btn">
                                                    <form action="{{ route('admin.customers.destroy', $customer->id) }}" method="post" class="form-horizontal">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="delete">
                                                        <div class="btn-group">
                                                            <a href="{{ route('admin.customers.edit', $customer->id) }}" class="btn btn-edit"><i class="mdi mdi-pencil"></i></a>
                                                            <button onclick="return confirm('Are you sure you want to delete?')" type="submit" class="btn btn-delete"><i class="mdi mdi-close"></i></button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="10" align="center">Data Not Found</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<style type="text/css">
    .search{
        float: right!important;
        padding-bottom: 10px;
    }
    .btn-info, .btn-info.disabled {
        color: black!important;
        background: #e4e7ea!important;
    }
    .btn-info:hover{
        border:1px solid!important;
        border-color: none!important;
    }
</style>
@push('js')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        init_datatable();
    } );
    
    function init_datatable(){
        $('.customers').DataTable().destroy();
        setTimeout(function(){
            var table = $('.customers').DataTable({
                deferRender: true,
                scrollX: true
            });
        },300);
    }
</script>
@endpush