@extends('layouts.admin-sol.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
            <div class="form-group">
                <select class="minimal form-control" tabindex="1" name="employee_id" id="employee_id">
                   <option value="">--Select Employee--</option>
                </select>
                <input type="text" name="comment" id="comment">
                <button id="available_vendor">Available</button>

            </div>

        </div>
        <div class="col-sm-12 col-md-12 col-sm-6 col-xs-12">
            <div class="" id="dashboard_map">
            </div>
        </div>
    </div>
</div>
@endsection
<style>
    #dashboard_map {
        height: 380px !important;
        width: 100%!important;
    }
</style>
@section('js')
<script type="text/javascript" src="http://{{ request()->getHost() }}:8080/socket.io/socket.io.js"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->

<script type="text/javascript">
    $('#table_tab tr').click(function () {
        customer_id = $("#customer_id").val();
        console.log(customer_id);
    });
    /* variables */
    var hostname = window.location.hostname;
    var marker;

    /* google map initialize */
    var map;
    function initMap() {
        map = new google.maps.Map(document.getElementById('dashboard_map'), {
          center: {lat: 22.994106, lng: 72.497948},
          zoom: 16
        });
        marker = new google.maps.Marker();
    }

    var socket = io.connect(hostname+":8080/");
    /* set marker */
    var icon=[];
    $("#employee_id").on('change',function(){
        var employee_id = $("#employee_id").val();
        socket.emit("employee_get",{employee_id:employee_id});
    });
    // Available Status Test
    // $("#available_vendor").on('click',function(){
    //     var customer_id='2';
    //     var latitude='123';
    //     var longitude='123';
    //     var status='on';
    //     socket.emit("available_vendors_insert",{customer_id:customer_id,latitude:latitude,longitude:longitude,status:status});
    // })

    $("#available_vendor").on('click',function(){
        comment = $("#comment").val();
        customer_id = "1";
        date = yyyymmdd();
        socket.emit("comment_add",{customerId:customer_id,comment:comment,date:date});
    });
    /* location listion and set marker */
    socket.on("location_get",function(res_data){
        iconBase=null;
        latitude = res_data[0].latitude;
        longitude = res_data[0].longitude;
        if(latitude!=null && longitude!=null){
            var iconBase = '/assets/images/map-marker.png';
            var position = new google.maps.LatLng(latitude,longitude);
            marker.setPosition(position);
            marker.setIcon(iconBase);
            marker.setMap(map);
        }
    });
    // $("#available_vendor").on('click',function(){
    // var customer_id='2';
    // // var latitude='19.0825223';
    // var latitude='23.0974716';
    // var longitude='72.5292302';
    // var status='on';
    // var date = yyyymmdd();
    // console.log(latitude);
    // socket.emit("location_insert",{customerId:customer_id,latitude:latitude,longitude:longitude,status:status,date:date});
    // });
    
    socket.on("available_vendors_get",function(res_data){
        $('#employee_id').empty();
        $('#employee_id').append('<option value="">Select city</option>');
        $.each(res_data, function(key, value){
            if(value['status']=="on"){
                $("#employee_id").append('<option value="'+ value['customer_id'] +'">'+ value['vendorName']+    
                    '</option>'
                );
            }
        });
    });
    function yyyymmdd() {
        var x = new Date();
        var y = x.getFullYear().toString();
        var mo = (x.getMonth() + 1).toString();
        var d = x.getDate().toString();
        var h = x.getHours();
        var m = x.getMinutes();
        var s = x.getSeconds();

        (d.length == 1) && (d = '0' + d);
        (mo.length == 1) && (mo = '0' + mo);
        (h.length == 1) && (h = '0' + h);
        (m.length == 1) && (m = '0' + m);
        (s.length == 1) && (s = '0' + s);
        var yyyymmdd = y +'-'+ mo +'-'+ d +' '+ h +':'+m+':'+s;
        return yyyymmdd;
    }    
</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfDJaVdwAS-KTBw3WIyMPV2CcrTJAUKFI&callback=initMap">
</script>
@endsection