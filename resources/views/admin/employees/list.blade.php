@extends('layouts.admin-sol.app')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage Vendors</h4> </div>
			<!-- <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="#">Manage Products</a></li>
					<li class="active">Add Products</li>
				</ol>
			</div> -->
			<!-- /.col-lg-12 -->
		</div>
		@include('layouts.errors-and-messages')
		@if(!$employees->isEmpty())
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-listing">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							<li><a href="{{ route('admin.employees.create') }}">Add Vender</a></li>
							<li class="active"><a>Vendor's Listing</a></li>
					  </ul>
					  
					  <div class="tab-content">
							<div id="ProductListing" class="tab-pane fade in active">
								<div class="product-listing">
									<div class="table-responsive">
										<table id="example" class="table border-b1px" style="width:100%">
											<thead>
												<tr>
													<th>Email</th>
													<th>Name</th>
													<th>Registration Date</th>
													<th>Status</th>
													<th width="100">Actions</th>
												</tr>
											</thead>
											<tbody>
												@foreach ($employees as $employee)
													<tr>
														<td>{{ $employee->email }}</td>
														<td>{{ $employee->first_name }} {{ $employee->last_name }}</td>
														<td>{{ date('d-m-Y',strtotime($employee->created_at)) }}</td>
														<td>
			                                            <form action="{{ route('admin.employees.is_active',$employee->id) }}" method="post" class="form-horizontal">
			                                                {{ csrf_field() }}
			                                                @if($employee->status == "1")
			                                                    <button class="btn in-active-status" onclick="return confirm('Are you sure? You want to change status.')">Active</button>
			                                                @else
			                                                    <button class="btn in-active-status" onclick="return confirm('Are you sure? You want to change status.')">InActive</button>
			                                                @endif
			                                            </form>
			                                            </td>
														<td>
															<div class="action-btn">
																<form action="{{ route('admin.employees.destroy', $employee->id) }}" method="post" class="form-horizontal">
																	{{ csrf_field() }}
																	<input type="hidden" name="_method" value="delete">
																	<div class="btn-group">
																		<a href="{{ route('admin.employees.edit', $employee->id) }}" class="btn btn-edit"><i class="mdi mdi-pencil"></i></a>
																		<button onclick="return confirm('Are you sure? You want to delete.')" type="submit" class="btn btn-delete"><i class="mdi mdi-close"></i></button>
																	</div>
																</form>
															</div>
														</td>
													</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
		@endif
	</div>
@endsection
@section('js')
<script>
	$(document).ready(function() {
		$('#example').DataTable();
	} );
</script>
@endsection
