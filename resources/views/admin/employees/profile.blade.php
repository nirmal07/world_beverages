@extends('layouts.admin-sol.app')

@section('content')
<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Admin Profile</h4> </div>
		<!-- <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			<ol class="breadcrumb">
				<li><a href="#">Manage Products</a></li>
				<li class="active">Add Products</li>
			</ol>
		</div> -->
		<!-- /.col-lg-12 -->
	</div>
	@include('layouts.errors-and-messages')
	<div class="page-content">
		<div class="row">
			<div class="col-sm-12">
				<div class="admin-profile">
					<div class="row">
						<div class="col-sm-4 col-md-3">
							<div class="admin-img">
								<img src="{{ $employee->profile_image }}" onerror="{{ asset('assets/plugins/images/profile.jpg') }}" alt="">
							</div>
							<div class="admin-detail">
								<ul>
									<li><h2>{{ $employee->first_name }} {{ $employee->last_name }}</h2></li>
									<li>Admin</li>
									<li>{{ $employee->email }}</li>
								</ul>
							</div>
						</div>
						<div class="col-sm-8 col-md-9">
							<div class="admin-edit">
								<h3>Update Profile Information</h3>
								<form action="{{ route('admin.employee.profile.update', $employee->id) }}" method="post" class="form" enctype="multipart/form-data">
									<input type="hidden" name="_method" value="put">
									{{ csrf_field() }}
									<div class="row p-15">
										<div class="col-sm-4 col-lg-3 col-md-3">
											<label>Profile Picture</label>
										</div>
										<div class="col-sm-8 col-lg-6 col-md-9">
											<div class="form-group">
												<input id="profile_image" name="profile_image" type="file" class="form-control" accept="image/*">
											</div>
										</div>
									</div>
									<div class="row p-15">
										<div class="col-sm-4 col-lg-3 col-md-3">
											<label>First Name</label>
										</div>
										<div class="col-sm-8 col-lg-6 col-md-9">
											<div class="form-group">
												<input id="first_name" name="first_name" type="text" class="form-control" value="{{ $employee->first_name }}">
											</div>
										</div>
									</div>
									<div class="row p-15">
										<div class="col-sm-4 col-lg-3 col-md-3">
											<label>Last Name</label>
										</div>
										<div class="col-sm-8 col-lg-6 col-md-9">
											<div class="form-group">
												<input id="last_name" name="last_name" type="text" class="form-control" value="{{ $employee->last_name }}">
											</div>
										</div>
									</div>
									<div class="row p-15">
										<div class="col-sm-4 col-lg-3 col-md-3">
											<label>Email Address</label>
										</div>
										<div class="col-sm-8 col-lg-6 col-md-9">
											<div class="form-group">
												<input ip="email" name="email" type="email" class="form-control" value="{{ $employee->email }}">
											</div>
										</div>
									</div>
									<div class="row p-15">
										<div class="col-sm-4 col-lg-3 col-md-3">
											<label>Username</label>
										</div>
										<div class="col-sm-8 col-lg-6 col-md-9">
											<div class="form-group">
												<input id="username" name="username" type="text" class="form-control" value="{{ $employee->username }}">
											</div>
										</div>
									</div>

									<h3 class="mt-50">Update Profile Information</h3>

									<div class="row p-15">
										<div class="col-sm-4 col-lg-3 col-md-3">
											<label>Old Password</label>
										</div>
										<div class="col-sm-8 col-lg-6 col-md-9">
											<div class="form-group">
												<input id="old_password" name="old_password" type="password" class="form-control" value="" placeholder="xxxxxx">
											</div>
										</div>
									</div>
									<div class="row p-15">
										<div class="col-sm-4 col-lg-3 col-md-3">
											<label>New Password</label>
										</div>
										<div class="col-sm-8 col-lg-6 col-md-9">
											<div class="form-group">
												<input id="password" name="password" type="password" class="form-control" value="" placeholder="xxxxxx">
											</div>
										</div>
									</div>
									<div class="row p-15">
										<div class="col-sm-4 col-lg-3 col-md-3">
											<label>Confirm Password</label>
										</div>
										<div class="col-sm-8 col-lg-6 col-md-9">
											<div class="form-group">
												<input id="password_confirmation" name="password_confirmation" type="password" class="form-control" value="" placeholder="xxxxxx">
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-4 col-lg-3 col-md-3">
										</div>
										<div class="col-sm-8 col-lg-6 col-md-9">
											<div class="btn-part">
												<button class="btn btn-rounded">SUBMIT</button>
											</div>
										</div>
									</div>

								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
