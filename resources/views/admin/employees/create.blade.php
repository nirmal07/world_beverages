@extends('layouts.admin-sol.app')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage Venders</h4> </div>
			<!-- <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="#">Manage Products</a></li>
					<li class="active">Add Products</li>
				</ol>
			</div> -->
			<!-- /.col-lg-12 -->
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-tab">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							@if(isset($employees->id))
								<li class="active"><a>Edit Vender</a></li>
							@else
								<li class="active"><a>Add Vender</a></li>
							@endif
							<li><a href="{{ route('admin.employees.index') }}">Vender's Listing</a></li>
					  </ul>
					  
					  <div class="tab-content">
							<div id="AddProcucts" class="tab-pane fade in active">
								<div class="add-product">
								  <form action="{{ route('admin.employees.store') }}" method="post" class="form"  enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="row">
											<div class="col-lg-5 col-md-6 col-sm-6">
												<div class="form-group">
													<input type="hidden" name="id" value="@if(isset($employees->id)){{$employees->id}}@elseif(old('id')!=''){{old('id')}} @endif">
													<label>First Name <span class="red">*</span></label>
													<input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name" value="@if(isset($employees->first_name)){{$employees->first_name}}@elseif(old('first_name')!=''){{old('first_name')}} @endif">
													@if($errors->has('first_name'))
				                                    	<span class="text-danger">{{$errors->first('first_name')}}</span>
				                                    @endif
												</div>
											</div>
											<div class="col-lg-5 col-md-6 col-sm-6">
												<div class="form-group">
													<label>Last Name <span class="red">*</span></label>
													<input type="text" name="last_name" id="last_name" placeholder="Last Name" class="form-control" value="@if(isset($employees->last_name)){{$employees->last_name}}@elseif(old('last_name')!=''){{old('last_name')}} @endif">
													@if($errors->has('last_name'))
				                                    	<span class="text-danger">{{$errors->first('last_name')}}</span>
				                                    @endif
												</div>
											</div>
											<div class="col-lg-5 col-md-6 col-sm-6">
												<div class="form-group">
													<label>E-mail Address <span class="red">*</span></label>
													<input type="text" name="email" placeholder="Email" autocomplete="off"  id="email" class="form-control" value="@if(isset($employees->email)){{$employees->email}}@elseif(old('email')!=''){{old('email')}} @endif">
													@if($errors->has('email'))
				                                    	<span class="text-danger">{{$errors->first('email')}}</span>
				                                    @endif
												</div>
											</div>
											@if(isset($employees->id))
											<div class="col-lg-5 col-md-6 col-sm-6">
												<div class="form-group">
													<label>Create New Password</label>
													<input type="password" name="new_password" id="new_password" class="form-control" placeholder="****">
													@if($errors->has('password'))
				                                    	<span class="text-danger">{{$errors->first('password')}}</span>
				                                    @endif
												</div>
											</div>
											@else
											<div class="col-lg-5 col-md-6 col-sm-6">
												<div class="form-group">
													<label>Password <span class="red">*</span></label>
													<input type="password" name="password" id="password" class="form-control" placeholder="****">
													@if($errors->has('password'))
				                                    	<span class="text-danger">{{$errors->first('password')}}</span>
				                                    @endif
												</div>
											</div>
											@endif
											<div class="col-lg-5 col-md-6 col-sm-6">
												<div class="form-group">
													<label for="role">Role <span class="red">*</span></label>
													@php
														if(isset($employees->role)){
															$data['selectedIds']=$employees->role; 
														}
													@endphp
													<select name="role" id="role" class="form-control select2">
														<option value="">Select Role</option>
														@foreach($roles as $role)
															<option @if(isset($employees->role)) @if(in_array($role->id, $data))selected="selected" @endif @endif value="{{ $role->id }}">{{ $role->name }}</option>
														@endforeach
													</select>
													@if($errors->has('role'))
				                                    	<span class="text-danger">{{$errors->first('role')}}</span>
				                                    @endif
												</div>
											</div>
											<div class="col-lg-5 col-md-6 col-sm-6">
												@if(isset($employees->id))
													@include('admin.shared.status-select', ['status' => $employees->status])
												@else
													@include('admin.shared.status-select', ['status' => 0])
												@endif
											</div>
										</div>
										<div class="row">
										<div class="col-sm-12">
											<hr style="height: 1px; background: #e4e4e4;">
										</div>
										<div class="col-sm-12">
											<div class="submit-btn">
												<button type="submit" class="btn btn-submit">Submit</button>
											</div>
										</div>
									</div>
								</form>
							</div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection