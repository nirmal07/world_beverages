@extends('layouts.admin.layout')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage news</h4> </div>
		</div>
		@include('layouts.errors-and-messages')
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-listing">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							<li><a href="{{ route('admin.news.create') }}">Add News</a></li>
							<li class="active"><a>News Listing</a></li>
					  	</ul>
					  
					  	<div class="tab-content">
							<div id="ProductListing" class="tab-pane fade in active">
								<div class="product-listing">
									<div class="table-responsive">
										<table id="news" class="news table border-b1px" style="width:100%">
											<thead>
												<tr>
													<th style="width: 15%">Creation Date</th>
													<th style="width: 15%">Public Date</th>
													<th style="width: 15%">Title</th>
													<th style="width: 15%">Image</th>
													<th style="width: 15%">Status</th>
													<th style="width: 10%">Actions</th>
												</tr>
											</thead>
											<tbody>
												@foreach ($news as $data)
												<tr>
													<td>{{date('d-m-Y',strtotime($data->created_at))}}</td>
													<td>{{date('d-m-Y',strtotime($data->publication_date))}}</td>
													<td>{{ $data->title }}</td>
													<td>
														<img src="{{url($data->image)}}" height="100px" width="100px">
													</td>
													<td>
													<form action="{{ route('admin.news.status',$data->id) }}" method="post" class="form-horizontal">
			                                                {{ csrf_field() }}
			                                                @if($data->status == "1")
			                                                    <button class="btn in-active-status" onclick="return confirm('Are you sure? You want to change status.')">Active</button>
			                                                @else
			                                                    <button class="btn in-active-status" onclick="return confirm('Are you sure? You want to change status.')">InActive</button>
			                                                @endif
			                                            </form>
			                                        </td>
													<td>
														<div class="action-btn">
															<form action="{{ route('admin.news.destroy', $data->id) }}" method="get" class="form-horizontal">
																{{ csrf_field() }}
																<input type="hidden" name="_method" value="delete">
																<div class="btn-group">
																	<a href="{{ route('admin.news.edit', $data->id) }}" class="btn btn-edit"><i class="mdi mdi-pencil"></i></a>
																	<button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-delete"><i class="mdi mdi-close"></i></button>
																</div>
															</form>
														</div>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
<style type="text/css">
	.btn-info, .btn-info.disabled {
        color: black!important;
        background: #e4e7ea!important;
    }
    .btn-info:hover{
        border:1px solid!important;
        border-color: none!important;
    }
</style>
@push('js')

<script>
    $(document).ready(function() {
        init_datatable();
    } );
    
    function init_datatable(){
        $('.news').DataTable().destroy();
        setTimeout(function(){
            var table = $('.news').DataTable({
                deferRender: true,
                scrollX: true
            });
        },300);
    }
</script>
@endpush
