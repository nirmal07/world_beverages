@extends('layouts.admin.layout')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage News</h4> 
			</div>
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-tab">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							@if(isset($news->id))
								<li class="active"><a>Edit News</a></li>
							@else
								<li class="active"><a>Add News</a></li>
							@endif
							<li><a href="{{ route('admin.news.list') }}">News Listing</a></li>
					  	</ul>
					  	<div class="tab-content">
							<div id="AddProcucts" class="tab-pane fade in active">
								<div class="add-product">
								   	<form id="create_product" action="{{ route('admin.news.store') }}" method="post" class="form" enctype="multipart/form-data">
									{{ csrf_field() }}
									@if(isset($news->id))
				            			<input type="hidden" name="id" value="{{$news->id}}">
		            				@else
				            			<input type="hidden" name="id" value="">
			            			@endif
			            			@if(isset($news->id))
				            			<input type="hidden" name="seo_id" value="{{$news->seo_id}}">
									@endif				            	
			            				<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Title<span class="red">*</span></label>
													<input type="text" tabindex="1" name="title" class="form-control" placeholder="Title News" maxlength="500" value="@if(isset($news->title)){{ $news->title }}@elseif(old('title')!=''){{ old('title') }} @endif">
													@if($errors->has('title'))
		                                      			<span class="text-danger">{{$errors->first('title')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Sub Title<span class="red">*</span></label>
													<input type="text" name="sub_title" tabindex="2" class="form-control" placeholder="Sub Title" maxlength="500" value="@if(isset($news->sub_title)){{ $news->sub_title }}@elseif(old('sub_title')!=''){{ old('sub_title') }} @endif">
													@if($errors->has('sub_title'))
		                                      			<span class="text-danger">{{$errors->first('sub_title')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8 col-md-8 col-sm-8 col-xs-12">
												<div class="form-group">
													<label>Body<span class="red">*</span></label>
													<textarea class="form-control" tabindex="3" id="content1" name="content">
														@if(isset($news->body)){{$news->body}}@elseif(old('content')!=''){{old('content')}} @endif
					                           		</textarea>
													@if($errors->has('content'))
		                                      			<span class="text-danger">{{$errors->first('content')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Category<span class="red">*</span></label>
													<!--  -->
													<select name="category_id" id="category_id" tabindex="4" class="form-control" style="width: 100%; height:36px;">
														<option value="">Select Category</option>
														@foreach($categories as $data)
															<option value="{{ $data->id }}"
															@if(old('category_id') == $data->id)
	                                						selected="true"
	                                						@endif
					                                        @if(isset($news->category) && $data->id== $news->category))
					                                            selected="true"
					                                        @endif
					                                        >{{ $data->name }}</option>
					                                    @endforeach
													</select>
													@if($errors->has('category_id'))
		                                      			<span class="text-danger">{{$errors->first('category_id')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>SEO page title</label>
													<input type="text" name="seo_page_title" tabindex="7" class="form-control" placeholder="SEO Page Title" maxlength="500" value="@if(isset($seo->page_title)){{ $seo->page_title }}@elseif(old('seo_page_title')!=''){{ old('seo_page_title') }} @endif">
													@if($errors->has('seo_page_title'))">
		                                      			<span class="text-danger">{{$errors->first('seo_page_title')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Publication Date <span class="red">*</span></label>
													<input type="text" name="publication_date" tabindex="5" id="publication_date" placeholder="dd-mm-yyyy" class="form-control"value="@if(isset($news->publication_date)){{date('d-m-Y',strtotime($news->publication_date))}}@elseif(old('publication_date')!=''){{ old('publication_date') }} @endif">
													@if($errors->has('publication_date'))
		                                      			<span class="text-danger">{{$errors->first('publication_date')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>SEO page description</label>
													<textarea name="seo_page_description" class="form-control" tabindex="8" placeholder="SEO Page Description">@if(isset($seo->page_description)){{$seo->page_description}}@elseif(old('page_description')!=''){{old('page_description')}} @endif</textarea>
													@if($errors->has('seo_page_description'))
		                                      			<span class="text-danger">{{$errors->first('seo_page_description')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										@php
											if(isset($news->section_tags)){
												$section_tags = explode(',',$news->section_tags);
											}
											if(isset($seo->page_keywords)){
												$page_keywords = explode(',',$seo->page_keywords);
											}
											if(isset($seo->public_metatags)){
												$public_metatags = explode(',',$seo->public_metatags);
											}
										@endphp
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Section Tags<span class="red">*</span></label>
													<select name="section_tags[]" id="section_tags" tabindex="6" class="form-control select2" multiple="multiple">
														@if(isset($section_tags))
															@foreach($section_tags as $key => $value)
															<option
																 selected= "selected" value="{{ $value }}"
															>{{$value}}</option>
															@endforeach
														@else
															<option></option>	
														@endif
													</select>
													@if($errors->has('section_tags'))
		                                      			<span class="text-danger">{{$errors->first('section_tags')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>SEO Page Keywords</label>
													<select name="seo_page_keyword[]" id="seo_page_keyword" tabindex="9" class="form-control select2" multiple="multiple">
														@if(isset($page_keywords))
															@foreach($page_keywords as $key => $value)
															<option
																 selected= "selected" value="{{ $value }}"
															>{{$value}}</option>
															@endforeach
														@else
															<option></option>	
														@endif
													</select>
													@if($errors->has('seo_page_keyword'))
		                                      			<span class="text-danger">{{$errors->first('seo_page_keyword')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Layout and photo</label>
													<select name="layouts_image" class="form-control" name="layouts_image" tabindex="11">
														<option value="left"
														@if(old('layouts_image')=='left') selected="true" @elseif(isset($news->layouts) && $news->layouts=="left") selected="true" @endif>Left</option>
														<option value="right"
														@if(old('layouts_image')=='right') selected="true" @elseif(isset($news->layouts) && $news->layouts=="right") selected="true" @endif>Right</option>
														<option value="center"
														@if(old('layouts_image')=='center') selected="true" @elseif(isset($news->layouts) && $news->layouts=="center") selected="true" @endif>Center</option>
													</select>
													@if($errors->has('layouts_image'))
		                                      			<span class="text-danger">{{$errors->first('layouts_image')}}</span>
		                                    		@endif
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Public Metatags</label>
													<select name="public_metatags[]" id="public_metatags" tabindex="10" class="form-control select2" multiple="multiple">
														@if(isset($public_metatags))
															@foreach($public_metatags as $key => $value)
															<option
																 selected= "selected" value="{{ $value }}"
															>{{$value}}</option>
															@endforeach
														@else
															<option></option>	
														@endif
													</select>
													@if($errors->has('public_metatags'))
		                                      			<span class="text-danger">{{$errors->first('public_metatags')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="file-upload">
													<div class="file-select">
														<div class="file-select-button" id="fileName">Choose Image <span class="red">*</span></div>
														<div class="file-select-name" id="noFile">No file chosen...</div>
														<input type="file" name="image" tabindex="12" id="image" accept="image/*" value="" >
													</div>
												</div>
												@if($errors->has('image'))
	                                      			<span class="text-danger">{{$errors->first('image')}}</span>
	                                    		@endif
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<label>Status<span class="red">*</span></label>
												<select tabindex="13" class="form-control" name="status">
													<option value="">Select status</option>
													<option value="1"@if(old('status')=="1") selected="true" @elseif(isset($news->status) && $news->status=="1") selected="true" @endif>Active</option>
													<option value="0"@if(old('status')=="0") selected="true" @elseif(isset($news->status) && $news->status=="0") selected="true" @endif>InActive</option>
												</select>
												@if($errors->has('status'))
	                                      			<span class="text-danger">{{$errors->first('status')}}</span>
	                                    		@endif
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div id="image_beside">
													@if(isset($news->image))
													<img src="{{url($news->image)}}" height="300px" id="edit_image" width="300px">
													@endif
												</div>
											</div>
										</div>
										<div class="row" style="margin-top: 10px">
											<div class="col-sm-12">
												<div class="submit-btn">
													<button type="submit" tabindex="14" class="btn btn-submit">Submit</button>
												</div>
											</div>
										</div>
								 	</form>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@push('js')
<script type="text/javascript" src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script type="text/javascript" src="{{url('js/custom/custom.js')}}"></script>
<script type="text/javascript">
    $('#publication_date').datepicker({
        autoclose: true,  
        format: "dd-mm-yyyy",
        todayHighlight: true,

    });
</script>
@endpush