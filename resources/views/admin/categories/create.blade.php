@extends('layouts.admin.layout')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage Categories</h4> </div>
			<!-- <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="#">Manage Products</a></li>
					<li class="active">Add Products</li>
				</ol>
			</div> -->
			<!-- /.col-lg-12 -->
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					
					<div class="product-tab">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							@if(isset($category->id))
								<li class="active"><a>Edit Category</a></li>
							@else
								<li class="active"><a>Add Category</a></li>
							@endif
							<li><a href="{{ route('admin.categories.index') }}">Categories Listing</a></li>
					  </ul>
					  <div class="tab-content">
							<div id="AddProcucts" class="tab-pane fade in active">
								<div class="add-product">
								  <form action="{{ route('admin.categories.store') }}" method="post" class="form" enctype="multipart/form-data">
									@csrf
		                			@if(isset($category->id))
				            			<input type="hidden" name="id" id="id" value="{{$category->id}}">
		            				@else
				            			<input type="hidden" name="id" value="">
			            			@endif
									<div class="row">
										<div class="col-sm-6 col-md-6 col-sm-6 col-xs-12">
											<div class="form-group">
												<label>Category Availability</label>
												<select name="category_availability" class="form-control" id="category_availability">
													<option value="">None</option>
													@foreach($category_availabilitys as $category_availability)
													<option value="{{$category_availability->id}} "@if(old('category_availability')==$category_availability->id) selected="true" @elseif(isset($parent_id->id) && $parent_id->id==$category_availability->id)selected="true" @endif>{{$category_availability->name}}</option>
													@endforeach
												</select>
												<input type="hidden" name="selected_parent" id="selected_parent" value="@if(isset($category->parent_id)){{ $category->parent_id }}@elseif(old('selected_parent')!=''){{ old('selected_parent') }} @endif">
												@if($errors->has('category_availability'))
	                                      			<span class="text-danger">{{$errors->first('category_availability')}}</span>
	                                    		@endif
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6 col-md-6 col-sm-6 col-xs-12">
											<div class="form-group">
												<label>Parent Category</label>
												<select name="subcat_id" class="form-control" id="subcat_id">
													<option value="">None</option>
												</select>
												<input type="hidden" name="selected_subcat" id="selected_subcat" value="@if(isset($category->subcat_id)){{ $category->subcat_id }}@elseif(old('selected_subcat')!=''){{ old('selected_subcat') }} @endif">
												@if($errors->has('subcat_id'))
	                                      			<span class="text-danger">{{$errors->first('subcat_id')}}</span>
	                                    		@endif
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6 col-md-6 col-sm-6 col-xs-12">
											<div class="form-group">
												<label>Name<span class="red">*</span></label>
												<input type="text" class="form-control" name="name" id="name" value="@if(isset($category->name)){{$category->name}}@elseif(old('name')!=''){{old('name')}} @endif" maxlength="50" placeholder="Enter Category Name">
												@if($errors->has('name'))
	                                      			<span class="text-danger">{{$errors->first('name')}}</span>
	                                    		@endif
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="submit-btn">
												<button type="submit" class="btn btn-submit">Submit</button>
											</div>
										</div>
									</div>
								</form>
							</div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@push('js')
<script type="text/javascript">
$(document).ready(function() {
	$('#cover').bind('change', function () {
		var filename = $("#cover").val();
		if (/^\s*$/.test(filename)) {
			$(".file-upload").find('#cover').removeClass('active');
			$("#noFile").text("No file chosen...");
		}
		else {
			$(".file-upload").find('#cover').addClass('active');
			$("#noFile").text(filename.replace("C:\\fakepath\\", ""));
		}
	});
	$("#category_availability").on('change',function($e){
		category_availability = $("#category_availability").val();
		getSubcat(category_availability);
		console.log(category_availability);
	});
	if($("#selected_parent").val()!=""){
		selected_parent = $("#selected_parent").val();
		getSubcat(selected_parent);
	};
	if($("#selected_subcat").val()!=""){
		selected_subcat = $("#selected_subcat").val();
	};
});
function getSubcat(id){
	$.ajax({
        type : "GET",
        url : "{{ url('admin/categories/subcat/') }}/"+id,
        success : function(response){
        	$('#subcat_id').empty();
            $('#subcat_id').append('<option value="">None</option>');
            $.each(response, function(key, value){
                if(selected_subcat==value){
                	if (id!=value) {
						$('#subcat_id').append('<option value="'+ value +'" selected="true">' + key + '</option>');
                	}
                }
             	else{
             		id = $("#id").val();
                 	if (id!=value) {
                    	$('#subcat_id').append('<option value="'+ value +'">' + key + '</option>');
                	}
                }
            });
    	},
    	error: function(error){
        	console.log(error);
        }
    });	
}
</script>
@endpush