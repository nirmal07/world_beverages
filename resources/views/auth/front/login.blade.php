@extends('layouts.layout')
@section('title', 'World-beverages')
@section('content')
	<div class="container">
		<div class="row d_flex">
			<div class="col-md-6">                 
                <div class="col-md-12 col-xs-12 login-blocks">
                    @include('layouts.errors-and-messages')
                    <h2>Login : </h2> 
                    <form action="{{route('front.logindata')}}" method="post" id="login">
                    	{{csrf_field()}}
                        <div class="form-group">
                            <label for="email">Username</label>
                            <input type="text" class="form-control" id="email" name="username" maxlength="50" placeholder="Enter username" value="@if(old('username')!=''){{old('username')}} @endif">
                             @if($errors->has('username'))
                                <span class="text-danger">{{$errors->first('username')}}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" maxlength="50" placeholder="Enter password" value="@if(old('password')!=''){{old('password')}} @endif">
                             @if($errors->has('password'))
                                <span class="text-danger">{{$errors->first('password')}}</span>
                            @endif
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-default log_data" > Log in</button>
                        </div>
                    </form>
                    <br>
                    
                    <h2>Social login :  </h2> 
                    
                    <p>
                    <a class="login-social" href="#"><i class="fa fa-facebook"></i>&nbsp;Facebook</a> 
                    <a class="login-social" href="#"><i class="fa fa-google-plus"></i>&nbsp;Gmail</a> 
                    <a class="login-social" href="#"><i class="fa fa-twitter"></i>&nbsp;Twitter</a>  
                    </p> 
                </div>
			</div>
		</div>
	</div>
@endsection
