@extends('layouts.admin.login_layout')
@section('title','World-beverages | Admin')
@section('content')
<div class="login-bg">
    <div class="bg-color">
        <div class="log-form">
            <div class="header">
                <a href="{{ url('admin') }}"><img src="{{url('img/icon/logo1.png')}}" alt=""></a>
            </div>
            <!--login -->
            <form action="{{ route('admin.login') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>Email</label>
                    <input name="email" type="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
                    @if($errors->has('email'))
                        <span class="text-danger">{{$errors->first('email')}}</span>
                    @endif
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input name="password" type="password" class="form-control" placeholder="Password">
                    @if($errors->has('password'))
                        <span class="text-danger">{{$errors->first('password')}}</span>
                    @endif
                </div>
                <div class="submit-btn">
                <button type="submit" class="btn btn-submit">Login</button>
                <!-- <a class="forgot" href="#">Forgot Username?</a> -->
                </div>
                <!-- <div class="checkbox checkbox-primary">
                    <input id="checkbox3" type="checkbox">
                    <label for="checkbox3">Remember Me</label>
                </div> -->
            </form>
        </div>
    </div>
</div>
@endsection

    


