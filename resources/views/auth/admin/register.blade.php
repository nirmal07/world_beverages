@extends('layouts.layout')
@section('title', 'World-beverages')
@section('content')
	<div class="register-area" style="background-color: rgb(249, 249, 249);">
            <div class="container reg_flex">

                <div class="col-md-6">
                    <div class="box-for overflow">
                        <div class="col-md-12 col-xs-12 register-blocks">
                            <h2>New account : </h2> 
                            <form action="{{route('front.store')}}" method="post">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="username">User Name</label>
                                    <input type="text" name="username" class="form-control" id="username" maxlength="50" placeholder="Enter username" value="@if(old('username')!=''){{old('username')}} @endif">
                                    @if($errors->has('username'))
                                        <span class="text-danger">{{$errors->first('username')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" id="password" name="password" maxlength="50" placeholder="Enter password" value="@if(old('password')!=''){{old('password')}} @endif">
                                    @if($errors->has('password'))
                                        <span class="text-danger">{{$errors->first('password')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" class="form-control" id="name" maxlength="50" placeholder="Enter name" value="@if(old('name')!=''){{old('name')}} @endif">
                                    @if($errors->has('name'))
                                        <span class="text-danger">{{$errors->first('name')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="surname">Surname</label>
                                    <input type="text" name="surname" class="form-control" id="surname" maxlength="50" placeholder="Enter surname" value="@if(old('surname')!=''){{old('surname')}} @endif">
                                    @if($errors->has('surname'))
                                        <span class="text-danger">{{$errors->first('surname')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="email">Email address</label>
                                    <input type="email" name="email" class="form-control" id="email" maxlength="50" placeholder="Enter email" value="@if(old('email')!=''){{old('email')}} @endif">
                                    @if($errors->has('email'))
                                        <span class="text-danger">{{$errors->first('email')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="phone">Phone number</label>
                                    <input type="text" name="phone" class="numeric form-control" id="number" maxlength="15" placeholder="Enter phone" value="@if(old('phone')!=''){{old('phone')}} @endif">
                                    @if($errors->has('phone'))
                                        <span class="text-danger">{{$errors->first('phone')}}</span>
                                    @endif
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-default register">Register</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
@endsection

<script language="JavaScript"  src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.numeric').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
        });
    })
</script>