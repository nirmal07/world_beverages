<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>@yield('title')</title>
        <meta name="description" content="GARO is a real-estate template">
        <meta name="author" content="Kimarotec">
        <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="{{url('img/icon/favicon.png')}}" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">

        <link rel="stylesheet" href="{{url('css/normalize.css')}}">
        <link rel="stylesheet" href="{{url('css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{url('css/fontello.css')}}">
        <link href="{{url('fonts/icon-7-stroke/css/pe-icon-7-stroke.css')}}" rel="stylesheet">
        <link href="{{url('fonts/icon-7-stroke/css/helper.css')}}" rel="stylesheet">
        <link href="{{url('css/animate.css')}}" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="{{url('css/bootstrap-select.min.css')}}"> 
        <link rel="stylesheet" href="{{url('bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{url('css/icheck.min_all.css')}}">
        <link rel="stylesheet" href="{{url('css/price-range.css')}}">
        <link rel="stylesheet" href="{{url('css/owl.carousel.css')}}">  
        <link rel="stylesheet" href="{{url('css/owl.theme.css')}}">
        <link rel="stylesheet" href="{{url('css/owl.transitions.css')}}">
        <link rel="stylesheet" href="{{url('css/style.css')}}">
        <link rel="stylesheet" href="{{url('css/responsive.css')}}">
        <link rel="stylesheet" href="{{url('css/lightslider.min.css')}}">
        @stack('css')
    </head>
    <body>
        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>
        <!-- Start header -->
        @include('layouts.header')
        <!--End top header -->
        @yield('content')
        @include('layouts.footer')    
        <script type="text/javascript" src="{{url('js/modernizr-2.6.2.min.js')}}"></script>

        <script type="text/javascript" src="{{url('js/jquery-1.10.2.min.js')}}"></script> 
        <script type="text/javascript" src="{{url('bootstrap/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{url('js/bootstrap-select.min.js')}}"></script>
        <script type="text/javascript" src="{{url('js/bootstrap-hover-dropdown.js')}}"></script>

        <script type="text/javascript" src="{{url('js/easypiechart.min.js')}}"></script>
        <script type="text/javascript" src="{{url('js/jquery.easypiechart.min.js')}}"></script>

        <script type="text/javascript" src="{{url('js/owl.carousel.min.js')}}"></script>
        <script type="text/javascript" src="{{url('js/wow.js')}}"></script>

        <script type="text/javascript" src="{{url('js/icheck.min.js')}}"></script>
        <script type="text/javascript" src="{{url('js/price-range.js')}}"></script>

        <script type="text/javascript" src="{{url('js/main.js')}}"></script>
        <script  type="text/javascript" src="{{url('js/lightslider.min.js')}}"></script>
        @stack('script')
    </body>
</html>