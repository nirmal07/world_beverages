<div class="header-connect">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-8  col-xs-12">
                <div class="header-half header-call">
                    <p>
                        <span><i class="pe-7s-call"></i> +1 234 567 7890</span>
                        <span><i class="pe-7s-mail"></i> your@company.com</span>
                    </p>
                </div>
            </div>
            <div class="col-md-2 col-md-offset-5  col-sm-3 col-sm-offset-1  col-xs-12">
                <div class="header-half header-social">
                    <ul class="list-inline">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-vine"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div> 
<!-- Body content -->
<nav class="navbar navbar-default ">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{route('home.index')}}">
                <img src="{{url('img/icon/logo1.png')}}" class="header-logo logo-clear header_lg">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse yamm" id="navigation">
            <div class="button navbar-right btn_center">
                
                @if(Auth::guard('customer')->check())
                    <a href="{{route('front.profile')}}" class="navbar-btn nav-button wow bounceInRight" data-wow-delay="0.45s">Profile</a>
                    <a href="{{route('front.logout')}}" class="navbar-btn nav-button wow fadeInRight" data-wow-delay="0.48s">logout</a>
                @else
                    <a href="{{route('front.login')}}" class="navbar-btn nav-button wow bounceInRight" id="login"  data-wow-delay="0.45s">Login</a>
                    <a href="{{route('front.create')}}" class="navbar-btn nav-button wow fadeInRight" data-wow-delay="0.48s">Register</a>
                @endif
            </div>
            <ul class="main-nav nav navbar-nav navbar-right toggle_padding">
                <li class="dropdown ymm-sw " data-wow-delay="0.1s">
                    <a href="{{route('home.index')}}" class="@php
                if(Request::route()->getName() == 'home.index'){
                @endphp active @php }
                @endphp" data-delay="200">Home</a>
                </li>
                <li class="dropdown ymm-sw" data-wow-delay="0.4s"><a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200" href="#">Buyers<b class="caret"></b></a>
                    <ul class="dropdown-menu navbar-nav">
                        <li>
                            <a href="{{route('home.search')}}">View Brands</a>
                        </li>
                    </ul>
                </li>
                <li class="wow fadeInDown" data-wow-delay="0.3s"><a class="" href="#">Sellers</a></li>
                <li class="dropdown ymm-sw" data-wow-delay="0.4s">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Jobs <b class="caret"></b></a>
                    <ul class="dropdown-menu navbar-nav">
                        <li>
                            <a href="#">Post Jobs</a>
                        </li>
                        <li>
                            <a href="#">View Jobs</a>
                        </li>
                    </ul>
                </li>
                <li class="wow fadeInDown " data-wow-delay="0.5s"><a href="{{route('front.digitaldirectory')}}" class="@php
                if(Request::route()->getName() == 'front.digitaldirectory'){
                @endphp active @php }
                @endphp">Digital Directory</a></li>
                <li class="wow fadeInDown" data-wow-delay="0.6s"><a href="#">Contact</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<style type="text/css">
    .header-logo{
        height: 65px!important;
    }
</style>
