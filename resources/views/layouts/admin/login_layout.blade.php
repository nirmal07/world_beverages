<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('img/icon/favicon.png') }}">

    <title>@yield('title')</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ url('admin/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('admin/css/themify-icons.css') }}" rel="stylesheet">
   
   
    <link href="{{ url('admin/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- animation CSS -->
    <link href="{{ url('admin/css/animate.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    
    <link href="{{ url('admin/css/login.css') }}" rel="stylesheet">
    <link href="{{ url('admin/css/style.css') }}" rel="stylesheet">
    <link href="{{ url('admin/css/default.cs') }}s" rel="stylesheet">
    
    <link href="{{ url('admin/css/custom.cs') }}s" rel="stylesheet">
    <link href="{{ url('admin/css/responsive.css') }}" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,600,700" rel="stylesheet">
    @stack('css')
</head>

<body>
    @yield('content')
</body>
<!-- All Jquery -->
    <script src="{{ url('admin/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{ url('admin/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ url('admin/js/admin.min.js') }}"></script>   
    @stack('script') 
</html>