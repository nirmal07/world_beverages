<!-- Left Sidebar - style you can find in sidebar.scss  -->
<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav slimscrollsidebar">
		<div class="sidebar-head">
			<h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
		
		<ul class="nav" id="side-menu">
			<li class="sidebar-item @if(Request::route()->getName() == 'admin.dashboard') active @endif">
				<a href="{{ route('admin.dashboard') }}" class="sidebar-link has-arrow waves-effect has-error">
					<span class="hide-menu">Dashboard</span>
				</a>
		 	</li>
		 	<li class="sidebar-item @if(request()->segment(2) == 'customers' || request()->segment(2) == 'addresses' || Request::route()->getName() == 'admin.customers.search') active @endif">
				<a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
					<span class="hide-menu">Manage User</span>
				</a>
				<ul aria-expanded="false" class="collapse first-level">
					<li class="sidebar-item">
						<a href="{{ route('admin.customers.index') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.customers.index' || Request::route()->getName() == 'admin.customers.search') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">User Listing</span>
						</a>
					</li>
					<li class="sidebar-item">
						<a href="{{ route('admin.customers.create') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.customers.create') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">User Create </span>
						</a>
					</li>
					
				</ul>
			</li>
			<li class="sidebar-item @if(request()->segment(2) == 'products' || Request::route()->getName() == 'admin.products.search' ) active @endif">
				<a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
					<span class="hide-menu">Manage Products</span>
				</a>
				<ul aria-expanded="false" class="collapse first-level">
					<li class="sidebar-item">
						<a href="{{ route('admin.products.index') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.products.index' || Request::route()->getName() == 'admin.products.search' ) active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">Product Listing</span>
						</a>
					</li>
					
					<li class="sidebar-item">
						<a href="{{ route('admin.products.create') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.products.create' || Request::route()->getName() == 'admin.products.edit') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">Add Procucts</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="sidebar-item @if(request()->segment(2) == 'categories' || Request::route()->getName() == 'admin.categories.search') active @endif">
				<a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
					<span class="hide-menu">Manage Categories</span>
				</a>
				<ul aria-expanded="false" class="collapse first-level">
					<li class="sidebar-item">
						<a href="{{ route('admin.categories.index') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.categories.index' || Request::route()->getName() == 'admin.categories.search') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">List categories</span>
						</a>
					</li>
					<li class="sidebar-item">
						<a href="{{ route('admin.categories.create') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.categories.create' || Request::route()->getName() == 'admin.categories.edit') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">Create category</span>
						</a>
					</li>
				</ul>
		 	</li>
		 	<li class="sidebar-item @if(request()->segment(2) == 'news' || Request::route()->getName() == 'admin.news.list' || Request::route()->getName() == 'admin.news.search' ) active @endif">
				<a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
					<span class="hide-menu">Manage News</span>
				</a>
				<ul aria-expanded="false" class="collapse first-level">
					<li class="sidebar-item">
						<a href="{{ route('admin.news.list') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.news.list' || Request::route()->getName() == 'admin.news.search' ) active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">List news</span>
						</a>
					</li>
					<li class="sidebar-item">
						<a href="{{ route('admin.news.create') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.news.create' || Request::route()->getName() == 'admin.news.edit') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">Create news</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="sidebar-item @if(request()->segment(2) == 'events' || Request::route()->getName() == 'admin.events.list' || Request::route()->getName() == 'admin.events.search' ) active @endif">
				<a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
					<span class="hide-menu">Manage Events</span>
				</a>
				<ul aria-expanded="false" class="collapse first-level">
					<li class="sidebar-item">
						<a href="{{ route('admin.events.list') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.events.list') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">List Events</span>
						</a>
					</li>
					<li class="sidebar-item">
						<a href="{{ route('admin.events.create') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.events.create' || Request::route()->getName() == 'admin.events.edit') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">Create Events</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="sidebar-item @if(request()->segment(2) == 'digitaldirectory' || Request::route()->getName() == 'admin.digitaldirectory.list'||Request::route()->getName() =='admin.digitaldirectory.none_listed_consultant') active @endif">
				<a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
					<span class="hide-menu">Manage Digital Directory</span>
				</a>
				<ul aria-expanded="false" class="collapse first-level">
					<li class="sidebar-item">
						<a href="{{ route('admin.digitaldirectory.list') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.digitaldirectory.list') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">List Digital Directory</span>
						</a>
					</li>
					<li class="sidebar-item">
						<a href="{{ route('admin.digitaldirectory.create') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.digitaldirectory.create' || Request::route()->getName() == 'admin.digitaldirectory.edit') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">Create Digital Directory</span>
						</a>
					</li>
					<li class="sidebar-item">
						<a href="{{ route('admin.digitaldirectory.none_listed_consultant') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.digitaldirectory.none_listed_consultant') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">None listed Consultant</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="sidebar-item @if(request()->segment(2) == 'academy' || Request::route()->getName() == 'admin.academys.list_author'||Request::route()->getName() =='admin.academys.create_author' || Request::route()->getName() == 'admin.academys.edit_author' || Request::route()->getName() == 'admin.academys.create_content' || Request::route()->getName() == 'admin.academys.list_content' || Request::route()->getName() == 'admin.academys.edit_content' || Request::route()->getName() == 'admin.academys.list_webinar' || Request::route()->getName() == 'admin.academys.create_webinar' || Request::route()->getName() == 'admin.academys.edit_webinar' || Request::route()->getName() == 'admin.academys.list_article' || Request::route()->getName() == 'admin.academys.create_article' || Request::route()->getName() == 'admin.academys.edit_article') active @endif">
				<a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
					<span class="hide-menu">Manage Academy</span>
				</a>
				<ul aria-expanded="false" class="collapse first-level">
					<li class="sidebar-item">
						<a href="{{ route('admin.academys.create_webinar') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.academys.create_webinar' || Request::route()->getName() == 'admin.academys.edit_webinar') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">Create Webinars</span>
						</a>
					</li>
					<li class="sidebar-item">
						<a href="{{ route('admin.academys.list_webinar') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.academys.list_webinar') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">List Webinars</span>
						</a>
					</li>
					<li class="sidebar-item">
						<a href="{{ route('admin.academys.create_author') }}"  class="sidebar-link @if(Request::route()->getName() == 'admin.academys.create_author' || Request::route()->getName() == 'admin.academys.edit_author') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">Create Authors</span>
						</a>
					</li>
					<li class="sidebar-item">
						<a href="{{ route('admin.academys.list_author') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.academys.list_author') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">List Authors</span>
						</a>
					</li>
					<li class="sidebar-item">
						<a href="{{ route('admin.academys.create_content') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.academys.create_content' || Request::route()->getName() == 'admin.academys.edit_content') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">Create Contents</span>
						</a>
					</li>
					<li class="sidebar-item">
						<a href="{{ route('admin.academys.list_content') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.academys.list_content') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">List Contents</span>
						</a>
					</li>
					<li class="sidebar-item">
						<a href="{{ route('admin.academys.create_article') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.academys.create_article' || Request::route()->getName() == 'admin.academys.edit_article') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">Create Articles</span>
						</a>
					</li>
					<li class="sidebar-item">
						<a href="{{ route('admin.academys.list_article') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.academys.list_article') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">List Articles</span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</div>
</div>