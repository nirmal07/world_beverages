<!-- Topbar header - style you can find in pages.scss -->
<nav class="navbar navbar-default navbar-static-top m-b-0">
	<div class="navbar-header">
		<div class="top-left-part">
			<!-- Logo -->
				<a href="{{route('admin.dashboard')}}" class="logo">
				<img class="logofull" src="{{ url('img/icon/logo1.png') }}" alt="home" />
				<img class="logosmall" src="{{ url('img/icon/logo-admin.png') }}" alt="home" />
			  </a>
		</div>
		<!-- /Logo -->
		<!-- Search input and Toggle icon -->
		<ul class="nav navbar-top-links navbar-left">
			
			<li><a href="javascript:void(0)" class="open-close waves-effect waves-light"><i class="ti-menu"></i></a></li>
			<li>
				<form role="search" class="app-search hidden-sm hidden-xs m-r-10">
					<input type="text" placeholder="Search..." class="form-control"> <a href=""><i class="fa fa-search"></i></a> </form>
			</li>
		   
		</ul>
		<ul class="nav navbar-top-links navbar-right pull-right clr-icn">
				<li class="dropdown">
						
						<ul class="dropdown-menu mailbox animated bounceInDown">
							<li>
								<div class="drop-title">You have 4 new messages</div>
							</li>
							<li>
								<div class="message-center">
									<a href="#">
										<div class="user-img"> <img src="{{ asset('assets/plugins/images/users/pawandeep.jpg') }}" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
										<div class="mail-contnet">
											<h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
									</a>
									<a href="#">
										<div class="user-img"> <img src="{{ asset('assets/plugins/images/users/sonu.jpg') }}" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
										<div class="mail-contnet">
											<h5>Sonu Nigam</h5> <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </div>
									</a>
									<a href="#">
										<div class="user-img"> <img src="{{ asset('assets/plugins/images/users/arijit.jpg') }}" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
										<div class="mail-contnet">
											<h5>Arijit Sinh</h5> <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </div>
									</a>
									<a href="#">
										<div class="user-img"> <img src="{{ asset('assets/plugins/images/users/pawandeep.jpg') }}" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
										<div class="mail-contnet">
											<h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
									</a>
								</div>
							</li>
							<li>
								<a class="text-center" href="javascript:void(0);"> <strong>See all notifications</strong> <i class="fa fa-angle-right"></i> </a>
							</li>
						</ul>
						<!-- /.dropdown-messages -->
					</li>
			<li class="dropdown">
				<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#">
					
        		<img src="#" alt="user-img" width="36" height="36" class="img-circle">
        	
					<b class="hidden-xs">
					</b><span class="caret"></span> </a>
				<ul class="dropdown-menu dropdown-user animated flipInY">
					<li>
						<div class="dw-user-box">
							<div class="u-img">
			        		<img src="" alt="user">
			        	
							</div>
							<div class="u-text">
								<h4></h4>
								<p class="text-muted"></p><a href="#" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
						</div>
					</li>
					<li role="separator" class="divider"></li>
					<li><a href="#"><i class="ti-user"></i> My Profile</a></li>
					<li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
					<li><a href="#"><i class="ti-email"></i> Inbox</a></li>
					<li role="separator" class="divider"></li>
					<li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
					<li role="separator" class="divider"></li>
					<li><a href="{{ route('admin.logout') }}"><i class="fa fa-power-off"></i> Logout</a></li>
				</ul>
				<!-- /.dropdown-user -->
			</li>
			<!-- /.dropdown -->
		</ul>
	</div>
	<!-- /.navbar-header -->
	<!-- /.navbar-top-links -->
	<!-- /.navbar-static-side -->
</nav>
<!-- End Top Navigation -->
