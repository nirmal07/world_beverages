<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('img/icon/favicon.png') }}">

    
    <title>{{ config('app.name') }}</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ url('admin/css/bootstrap.min.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ url('admin/css/bootstrap-switch.min.css') }}" rel="stylesheet">
    <link href="{{ url('admin/css/themify-icons.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ url('admin/plugins/bower_components/libs/select2/select2.min.css') }}">
    <!-- <link href="{{ url('admin/css/dataTables.fontAwesome.css') }}" rel="stylesheet"> -->
    <!-- @yield('css') -->
  <link type="text/css" href="{{ url('admin/plugins/bower_components/libs/quill/quill.snow.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ url('admin/plugins/bower_components/libs/select2/select2.min.css') }}" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="{{ url('admin/css/sidebar-nav.min.css') }}" rel="stylesheet">
    <!-- toast CSS -->
    <link href="{{ url('admin/plugins/bower_components/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
    <!-- morris CSS -->
    <link href="{{ url('admin/plugins/bower_components/morrisjs/morris.css') }}" rel="stylesheet">
    <!-- data table -->
    <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="{{ url('admin/plugins/bower_components/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ url('admin/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') }}" rel="stylesheet">
    <link href="{{ url('admin/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- animation CSS -->
    <link href="{{ url('admin/css/animate.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ url('admin/css/style.css') }}" rel="stylesheet">
    <link href="{{ url('admin/css/default.css') }}" rel="stylesheet">
    
    <link href="{{ url('admin/css/custom.css') }}" rel="stylesheet">
    <link href="{{ url('admin/css/responsive.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,600,700" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('admin/css/custom-sol.css') }}" rel="stylesheet">
    
    <style>
        .error{
            color:red;
        }
        div.dataTables_wrapper div.dataTables_paginate{
            margin-top: 9px !important;
        }
    </style>
    @yield('css')  
    @stack('css')  
</head>
<body class="fix-header">
<noscript>
    <p class="alert alert-danger">
        You need to turn on your javascript. Some functionality will not work if this is disabled.
        <a href="https://www.enable-javascript.com/" target="_blank">Read more</a>
    </p>
</noscript>
<!-- Wrapper -->
<div id="wrapper">
    @include('layouts.admin.header')

    @include('layouts.admin.sidebar')

    <!-- Page Content -->
    <div id="page-wrapper">
        {{-- @include("layouts.admin-sol.breadcumb") --}}
        @yield('content')
        @include('layouts.admin.footer')
    </div>
    <!-- /.content-wrapper -->


    {{-- @include('layouts.admin-sol.control-sidebar') --}}
</div>
<!-- ./wrapper -->

<!-- All Jquery -->
<script src="{{ url('admin/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ url('admin/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Menu Plugin JavaScript -->
<script src="{{ url('admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
<!--slimscroll JavaScript -->
<script src="{{ url('admin/js/jquery.slimscroll.js') }}"></script>
<!--Wave Effects -->
<script src="{{ url('admin/js/waves.js') }}"></script>
<!--Counter js -->
<script src="{{ url('admin/plugins/bower_components/waypoints/lib/jquery.waypoints.js') }}"></script>
<script src="{{ url('admin/plugins/bower_components/counterup/jquery.counterup.min.js') }}"></script>

<!-- Sparkline chart JavaScript -->
<script src="{{ url('admin/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
<!-- Custom Theme JavaScript -->
<script src="{{ url('admin/js/custom.min.js') }}"></script>
<script src="{{ url('admin/plugins/bower_components/toast-master/js/jquery.toast.js') }}"></script>
<!--Style Switcher -->
<!-- <script src="{{ url('admin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script> -->
<script src="{{ url('admin/plugins/bower_components/libs/quill/quill.min.js') }}"></script>
<script src="{{ url('admin/js/bootstrap-switch.min.js') }}"></script>

<!-- Select 2js -->
<script src="{{ url('admin/plugins/bower_components/libs/select2/select2.full.min.js') }}"></script>
<script src="{{ url('admin/plugins/bower_components/libs/select2/select2.min.js') }}"></script>
<script src="{{ url('admin/plugins/bower_components/libs/select2/select2.init.js') }}"></script>
<!-- Data table -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
@stack('js')
</body>
</html>
