<div class="col-md-3 p0 padding-top-40">
    <div class="blog-asside-right pr0">
        <div class="panel panel-default sidebar-menu wow fadeInRight animated" >
            <div class="panel-heading">
                <h3 class="panel-title">Quick search</h3>
            </div>
            <div class="panel-body search-widget">
                <form action="{{route('home.search')}}" class="form-inline"> 
                    <fieldset>
                        <div class="row">
                            <div class="col-xs-12">
                                <select id="lunchBegins" class="form-control" data-live-search="true" data-live-search-style="begins" title="Product Variety" name="product_category">
                                    <option value="">Select category</option> 
                                    @foreach($categories as $category)                
                                    <option value="{{$category->id}}" data-pluralform="{{$category->id}}" data-singularform="{{$category->id}}" data-seo="absinthe" @if(Request::input('product_category')==$category->id)
                                        selected="true" 
                                        @endif>{{$category->name}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="text" class="form-control font-color" placeholder="Key word" name="keyword" value="@if(Request::input('keyword')){{trim(Request::input('keyword'))}}@endif">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="padding-5">
                        <div class="row">
                            <div class="col-xs-6">
                                @php
                                    if(Request::input('price_range')){
                                        $price_range = (Request::input('price_range'));
                                    }
                                @endphp
                                <label for="price-range">Price range ($):</label>
                                @if(isset($price_range))
                                <input type="text" class="span2" value="" data-slider-min="0" 
                                       data-slider-max="600" data-slider-step="5" 
                                       data-slider-value="[{{$price_range}}]" id="price-range" name="price_range"><br />
                                @else
                                <input type="text" class="span2" value="" data-slider-min="0" 
                                       data-slider-max="600" data-slider-step="5" 
                                       data-slider-value="[0,1]" id="price-range" name="price_range"><br />
                                @endif
                                <b class="pull-left color">0$</b> 
                                <b class="pull-right color">600$</b>
                            </div>
                        </div>
                    </fieldset>                                
                    <fieldset >
                        <div class="row">
                            <div class="col-xs-12">  
                                <input class="button btn largesearch-btn" value="Search" type="submit">
                            </div>  
                        </div>
                    </fieldset>                                     
                </form>
            </div>
        </div>

        <div class="panel panel-default sidebar-menu wow fadeInRight animated">
            <div class="panel-heading">
                <h3 class="panel-title">Ads her  </h3>
            </div>
            <div class="panel-body recent-property-widget">
                <img src="img/ads.jpg">
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .intro{
        position: static;
    }
</style>
<script language="JavaScript"  src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>
<script type="text/javascript">
   $(document).ready(function () {
        $(".dropdown-menu.open").addClass("intro");
    });
     
</script>