<!-- News -->
    <div class="col-md-4 padding-top-40 properties-page user-properties">
        <div class="content-area recent-property">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-sm-12 page-title nws_pge">
                    <h3>News</h3>
                </div>
                <div class="section section_bgs"> 
                    <div id="list-type" class="proerty-th-list">
                        @foreach($news_all as $news)
                            <div class="col-md-4 p0">
                                <div class="item box-two proerty-item">
                                        @if($news->layouts=="left")
                                        <div class="item-thumb">
                                            <a href="{{route('front.news_details',$news->id)}}"><img src="{{url($news->image)}}" class="news-image img-itm" ></a>
                                        </div>
                                        <div class="item-entry overflow">
                                            <h5><a href="{{route('front.news_details',$news->id)}}">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</a></h5>
                                            <div class="dot-hr"></div>
                                            <p class="desc_lor">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        </div>
                                        @else
                                        <div class="item-entry overflow">
                                            <h5><a href="{{route('front.news_details',$news->id)}}">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</a></h5>
                                            <div class="dot-hr"></div>
                                            <p class="desc_lor">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        </div>
                                        <div class="item-thumb">
                                            <a href="{{route('front.news_details',$news->id)}}" ><img src="{{url($news->image)}}" class="news-image img-itm"></a>
                                        </div>
                                        @endif
                                </div>
                            </div>            
                        @endforeach
                    </div>     
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
        .news-image{
            height: 176px;
        }
    </style>

    <script type="text/javascript">
        function image() {
            
        }
    </script>


