<div class="col-md-4 pl0 padding-top-40">
    <div class="blog-asside-right pl0">
        <div class="panel panel-default sidebar-menu wow fadeInRight animated">
            <div class="tab tab_data">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="tablinks active" onclick="opentype(event, 'academy')">World-beverages Academy</button><button class="tablinks" onclick="opentype(event, 'webinars')">Webinars</button>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div id="academy" class="tabcontent active">
                    <div id="" class="proerty-th-list item-thumb">
                        <div class="col-lg-6">              
                            <a href="#" ><img src="{{url('img/product/product-home2.jpg')}}"></a>
                            <h5><a href="#"> </a></h5>
                        </div>
                        <h5 class="innov_data">How to Be a Beverage Boss: Tips for Budding Beverage Innovators</h5>
                        <div class="description">In this article BTN writes about 6 factors that brand owners can focus on to differentiate them from the crowded market place.</div>
                    </div>
                    <hr>
                    <div id="" class="proerty-th-list item-thumb">
                        <div class="col-lg-6">              
                            <a href="#" ><img src="{{url('img/product/product-home3.jpg')}}"></a>
                            <h5><a href="#"> </a></h5>
                        </div>
                        <h5 class="innov_data">How to Be a Beverage Boss: Tips for Budding Beverage Innovators</h5>
                        <div class="description">In this article BTN writes about 6 factors that brand owners can focus on to differentiate them from the crowded market place.</div>
                    </div>
                    <hr>
                    <div id="" class="proerty-th-list item-thumb">
                        <div class="col-lg-6">              
                            <a href="#" ><img src="{{url('img/product/product-home4.jpg')}}"></a>
                            <h5><a href="#"> </a></h5>
                        </div>
                        <h5 class="innov_data">How to Be a Beverage Boss: Tips for Budding Beverage Innovators</h5>
                        <div class="description">In this article BTN writes about 6 factors that brand owners can focus on to differentiate them from the crowded market place.</div>
                    </div>
                </div>
                <div id="webinars" class="tabcontent">
                    <div id="" class="proerty-th-list item-thumb">
                        <div class="col-lg-6">              
                            <a href="#" ><img src="{{url('img/product/product-home5.jpg')}}"></a>
                            <h5><a href="#"> </a></h5>
                        </div>
                        <h5 class="innov_data">Franchise State Law for Craft Breweries</h5>
                        <div class="description">Information for craft breweries on expanding into Franchise States</div>
                        <div class="time-webinar">4/11/2015  23:30</div>
                        <br>
                        <div class="register-webinar">
                            <button class="webinars_register">Register</button>
                        </div>
                    </div>
                    <hr>
                    <div id="" class="proerty-th-list item-thumb">
                        <div class="col-lg-6">              
                            <a href="#" ><img src="{{url('img/product/product-home6.jpg')}}"></a>
                            <h5><a href="#"> </a></h5>
                        </div>
                        <h5 class="innov_data">Franchise State Law for Craft Breweries</h5>
                        <div class="description">Information for craft breweries on expanding into Franchise States</div>
                        <div class="time-webinar">4/11/2015  23:30</div>
                        <br>
                        <div class="register-webinar">
                            <button class="webinars_register">Register</button>
                        </div>
                    </div>
                    <hr>
                    <div id="" class="proerty-th-list item-thumb">
                        <div class="col-lg-6">              
                            <a href="#" ><img src="{{url('img/product/product-home3.jpg')}}"></a>
                            <h5><a href="#"> </a></h5>
                        </div>
                        <h5 class="innov_data">Franchise State Law for Craft Breweries</h5>
                        <div class="description">Information for craft breweries on expanding into Franchise States</div>
                        <div class="time-webinar">4/11/2015  23:30</div>
                        <br>
                        <div class="register-webinar">
                            <button class="webinars_register">Register</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
.description {
    font-size: 14px;
    line-height: 18px;
}
.webinars_register {
    margin-left: 15px;
    background-color:#96021b;
    color:white;
}
.time-webinar{
    font-weight: bold;
}
</style>
