<div class="col-md-3 p0 padding-top-40">
    <div class="blog-asside-right pr0">
        <div class="panel panel-default sidebar-menu wow fadeInRight animated" >
            <div class="panel-heading">
                <h3 class="panel-title">Quick search</h3>
            </div>
            <div class="panel-body search-widget">
                <form action="{{route('front.digitaldirectory')}}" method="get" class="form-inline"> 
                    <fieldset>
                        <div class="row">
                            <div class="col-xs-12">
                                <select id="lunchBegins" class="selectpicker z_index" data-live-search="true" data-live-search-style="begins" title="Company Type" name="company_type">
                                    <option value="">Company Type</option>
                                    @foreach($company_types as $company_type)
                                    <option value="{{$company_type->id}}"data-pluralform="{{$company_type->id}}" data-singularform="{{$company_type->id}}" data-seo="{{$company_type->id}}"
                                    @if(Request::input('company_type')==$company_type->id)
                                        selected="true" 
                                    @endif>{{$company_type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="row">
                            <div class="col-xs-12">
                                <select id="" class="form-control" data-live-search="true" data-live-search-style="begins" title="Country" name="country_name">
                                    <option value="">Select Country</option>
                                    @foreach($countrys as $country)
                                    <option value="{{$country->id}}"
                                        @if(Request::input('country_name')==$country->id)
                                        selected="true" 
                                        @endif>{{$country->country}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="row">
                            <div class="col-xs-12">  
                                <input class="button btn largesearch-btn" value="Search" type="submit">
                            </div>  
                        </div>
                    </fieldset>                                     
                </form>
            </div>
        </div>
        <div class="panel panel-default sidebar-menu wow fadeInRight animated">
            <div class="panel-heading">
                <h3 class="panel-title">Ads her  </h3>
            </div>
            <div class="panel-body recent-property-widget">
                <img src="img/ads.jpg">
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .intro{
        position: static;
    }
</style>
<script language="JavaScript"  src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>
<script type="text/javascript">
   $(document).ready(function () {
        $(".dropdown-menu.open").addClass("intro");
    });
     
</script>