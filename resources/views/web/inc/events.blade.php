    <div class="col-md-4 padding-top-40 properties-page user-properties">
        <div class="content-area recent-property">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-sm-12 page-title evnt_mrg">
                    <h3>Events</h3>
                    
                </div>
                <div class="section section_bgs"> 
                    <div id="list-type" class="proerty-th-list events-home event_list">
                        @foreach($events as $event)  
                        <div class="col-md-4 p0">
                            <div class="box-two proerty-item">
                                @if($event->layout_image == "left")
                                <div class="item-thumb">
                                    <a href="{{route('front.event_details',$event->id)}}"><img src="{{url($event->image)}}" class="image-event"></a>
                                </div>
                                <div class="item-entry overflow event_padding">
                                    <h5><a href="{{route('front.event_details',$event->id)}}"> Probo scripserit et ius, cum semper consulatu assentior eu. An nam lobortis facilisis. Cu semper invenire vix. </a></h5>
                                    <div class="dot-hr"></div>
                                    <p class="txt_bgc">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi.</p>
                                </div>
                                @else
                                <div class="item-entry overflow ">
                                    <h5><a href="{{route('front.event_details',$event->id)}}"> Probo scripserit et ius, cum semper consulatu assentior eu. An nam lobortis facilisis. Cu semper invenire vix. </a></h5>
                                    <div class="dot-hr"></div>
                                    <p class="txt_bgc">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi.</p>
                                </div>
                                <div class="item-thumb">
                                    <a href="{{route('front.event_details',$event->id)}}" ><img src="{{url($event->image)}}" class="image-event"></a>
                                </div>
                                @endif
                            </div>
                        </div>                                           
                        @endforeach
                    </div>
                </div>
               <!--  -->                
            </div>
        </div>
    </div>

