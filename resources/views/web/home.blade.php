@extends('layouts.layout')
@section('title', 'World-beverages')
@section('content')
<div class="slider-area">
    <div class="slider">
        <div id="bg-slider" class="owl-carousel owl-theme">
            <div class="item"><img src="{{url('img/homePageSearchBackground_4.jpg')}}" alt="GTA V" style=""></div>
            <div class="item"><img src="{{url('img/homePageSearchBackground_4.jpg')}}" alt="The Last of us"></div>
        </div>
    </div>
    <div class="slider-content">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi deserunt deleniti, ullam commodi sit ipsam laboriosam velit adipisci quibusdam aliquam teneturo!</p>
            </div>
            <div class="col-lg-6 col-lg-offset-3 col-md-5 col-md-offset-1 col-sm-12">
                <div class="search-form wow pulse" data-wow-delay="0.8s">
                    <form action="{{route('home.search')}}" method="get" class=" form-inline">
                        <div class="form-group"> 
                            <select id="lunchBegins" class="selectpicker select-font" data-live-search="true" data-live-search-style="begins" title="Product Variety" name="product_category">
                                @foreach($categories as $category)                
                                    <option value="{{$category->id}}" data-pluralform="{{$category->id}}" data-singularform="{{$category->id}}" data-seo="absinthe">{{$category->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                       <div class="form-group">
                            <input type="text" class="form-control font-color" placeholder="Key word" style="border-color: black;" name="keyword"> 
                        </div>
                        <button class="btn search-btn" type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <!-- News -->
    @include('web.inc.news')

    <!-- Events -->
    @include('web.inc.events')  

    <!-- Article -->
    @include('web.inc.article')
</div>    
<!-- property area -->
<div class="content-area home-area-1 recent-property" style="background-color: #FCFCFC; padding-bottom: 55px;">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12 text-center page-title">
                <!-- /.feature title -->
                <h2>Top Brands</h2>
                <p>Our collection of brands covering different fields of the beverage industry.</p>
            </div>
        </div>

        <div class="row">
            <div class="proerty-th">
                <div class="col-sm-6 col-md-3 p0">
                    <div class="box-two proerty-item">
                        <div class="item-thumb">
                            <a href="#" ><img src="{{url('img/product/product-home1.jpg')}}" class="top-brands-product"></a>
                        </div>
                        <div class="item-entry overflow">
                            <h5><a href="#">Don Jacobo | Spain | Wine </a></h5>
                            <div class="dot-hr"></div>
                            <span class="pull-left"><b>Product type :</b> Whisky </span>
                            <span class="proerty-price pull-right">$ 300,000</span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 p0">
                    <div class="box-two proerty-item">
                        <div class="item-thumb">
                            <a href="#" ><img src="{{url('img/product/product-home2.jpg')}}" class="top-brands-product"></a>
                        </div>
                        <div class="item-entry overflow">
                            <h5><a href="#" >Don Jacobo | Spain | Wine </a></h5>
                            <div class="dot-hr"></div>
                            <span class="pull-left"><b>Product type :</b> Whisky </span>
                            <span class="proerty-price pull-right">$ 300,000</span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 p0">
                    <div class="box-two proerty-item">
                        <div class="item-thumb">
                            <a href="#" ><img src="{{url('img/product/product-home3.jpg')}}" class="top-brands-product"></a>
                        </div>
                        <div class="item-entry overflow">
                            <h5><a href="#" >Don Jacobo | Spain | Wine </a></h5>
                            <div class="dot-hr"></div>
                            <span class="pull-left"><b>Product type :</b> Whisky </span>
                            <span class="proerty-price pull-right">$ 300,000</span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 p0">
                    <div class="box-two proerty-item">
                        <div class="item-thumb">
                            <a href="#" ><img src="{{url('img/product/product-home4.jpg')}}" class="top-brands-product"></a>
                        </div>
                        <div class="item-entry overflow">
                            <h5><a href="#" >Don Jacobo | Spain | Wine </a></h5>
                            <div class="dot-hr"></div>
                            <span class="pull-left"><b>Product type :</b> Whisky </span>
                            <span class="proerty-price pull-right">$ 300,000</span>
                        </div>
                    </div>
                </div>


                <div class="col-sm-6 col-md-3 p0">
                    <div class="box-two proerty-item">
                        <div class="item-thumb">
                            <a href="#" ><img src="{{url('img/product/product-home5.jpg')}}" class="top-brands-product"></a>
                        </div>
                        <div class="item-entry overflow">
                            <h5><a href="#" >Don Jacobo | Spain | Wine </a></h5>
                            <div class="dot-hr"></div>
                            <span class="pull-left"><b>Product type :</b> Whisky </span>
                            <span class="proerty-price pull-right">$ 300,000</span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 p0">
                    <div class="box-two proerty-item">
                        <div class="item-thumb">
                            <a href="#" ><img src="{{url('img/product/product-home6.jpg')}}" class="top-brands-product"></a>
                        </div>
                        <div class="item-entry overflow">
                            <h5><a href="#" >Don Jacobo | Spain | Wine </a></h5>
                            <div class="dot-hr"></div>
                            <span class="pull-left"><b>Product type :</b> Whisky </span>
                            <span class="proerty-price pull-right">$ 300,000</span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 p0">
                    <div class="box-two proerty-item">
                        <div class="item-thumb">
                            <a href="#" ><img src="{{url('img/product/product-home3.jpg')}}" class="top-brands-product"></a>
                        </div>
                        <div class="item-entry overflow">
                            <h5><a href="#" >Don Jacobo | Spain | Wine </a></h5>
                            <div class="dot-hr"></div>
                            <span class="pull-left"><b>Product type :</b> Whisky </span>
                            <span class="proerty-price pull-right">$ 300,000</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 p0">
                    <div class="box-tree more-proerty text-center">
                        <div class="item-tree-icon">
                            <i class="fa fa-th"></i>
                        </div>
                        <div class="more-entry overflow">
                            <h5><a href="#" >CAN'T DECIDE ? </a></h5>
                            <h5 class="tree-sub-ttl">Show all brands</h5>
                            <button class="btn border-btn more-black" value="All properties">All brands</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  

<!--Welcome area -->


<!--TESTIMONIALS -->
<!-- <div class="testimonial-area recent-property" style="background-color: #FCFCFC; padding-bottom: 15px;">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12 text-center page-title">
                /.feature title -->
                <!-- <h2>Our Customers Said  </h2> 
            </div>
        </div> -->

        <div class="row test_monial">
            <div class="row testimonial test_monial">
                <div class="col-md-12">
                    <div id="testimonial-slider">
                        <div class="item">
                            <div class="client-text">                                
                                <p>Nulla quis dapibus nisl. Suspendisse llam sed arcu ultried arcu ultricies !</p>
                                <h4><strong>Ohidul Islam, </strong><i>Web Designer</i></h4>
                            </div>
                            <div class="client-face wow fadeInRight" data-wow-delay=".9s"> 
                                <img src="img/client-face1.png" alt="">
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-text">                                
                                <p>Nulla quis dapibus nisl. Suspendisse llam sed arcu ultried arcu ultricies !</p>
                                <h4><strong>Ohidul Islam, </strong><i>Web Designer</i></h4>
                            </div>
                            <div class="client-face">
                                <img src="img/client-face2.png" alt="">
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-text">                                
                                <p>Nulla quis dapibus nisl. Suspendisse llam sed arcu ultried arcu ultricies !</p>
                                <h4><strong>Ohidul Islam, </strong><i>Web Designer</i></h4>
                            </div>
                            <div class="client-face">
                                <img src="img/client-face1.png" alt="">
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-text">                                
                                <p>Nulla quis dapibus nisl. Suspendisse llam sed arcu ultried arcu ultricies !</p>
                                <h4><strong>Ohidul Islam, </strong><i>Web Designer</i></h4>
                            </div>
                            <div class="client-face">
                                <img src="img/client-face2.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
<!-- </div> -->

<!-- Count area -->
<!-- <div class="count-area">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12 text-center page-title"> -->
                <!-- /.feature title -->
                <!-- <h2>You can trust Us </h2> 
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xs-12 percent-blocks m-main" data-waypoint-scroll="true">
                <div class="row">
                    <div class="col-sm-3 col-xs-6">
                        <div class="count-item">
                            <div class="count-item-circle">
                                <span class="pe-7s-users"></span>
                            </div>
                            <div class="chart" data-percent="5000">
                                <h2 class="percent" id="counter">0</h2>
                                <h5>HAPPY CUSTOMER </h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-6">
                        <div class="count-item">
                            <div class="count-item-circle">
                                <span class="pe-7s-home"></span>
                            </div>
                            <div class="chart" data-percent="12000">
                                <h2 class="percent" id="counter1">0</h2>
                                <h5>Properties in stock</h5>
                            </div>
                        </div> 
                    </div> 
                    <div class="col-sm-3 col-xs-6">
                        <div class="count-item">
                            <div class="count-item-circle">
                                <span class="pe-7s-flag"></span>
                            </div>
                            <div class="chart" data-percent="120">
                                <h2 class="percent" id="counter2">0</h2>
                                <h5>City registered </h5>
                            </div>
                        </div> 
                    </div> 
                    <div class="col-sm-3 col-xs-6">
                        <div class="count-item">
                            <div class="count-item-circle">
                                <span class="pe-7s-graph2"></span>
                            </div>
                            <div class="chart" data-percent="5000">
                                <h2 class="percent"  id="counter3">5000</h2>
                                <h5>DEALER BRANCHES</h5>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->

<!-- boy-sale area -->
<!-- <div class="boy-sale-area">
    <div class="container">
        <div class="row">

            <div class="col-md-6 col-sm-10 col-sm-offset-1 col-md-offset-0 col-xs-12">
                <div class="asks-first">
                    <div class="asks-first-circle">
                        <span class="fa fa-search"></span>
                    </div>
                    <div class="asks-first-info">
                        <h2>ARE YOU LOOKING FOR A Property?</h2>
                        <p> varius od lio eget conseq uat blandit, lorem auglue comm lodo nisl no us nibh mas lsa</p>                                        
                    </div>
                    <div class="asks-first-arrow">
                        <a href="properties.html"><span class="fa fa-angle-right"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-10 col-sm-offset-1 col-xs-12 col-md-offset-0">
                <div  class="asks-first">
                    <div class="asks-first-circle">
                        <span class="fa fa-usd"></span>
                    </div>
                    <div class="asks-first-info">
                        <h2>DO YOU WANT TO SELL A Property?</h2>
                        <p> varius od lio eget conseq uat blandit, lorem auglue comm lodo nisl no us nibh mas lsa</p>
                    </div>
                    <div class="asks-first-arrow">
                        <a href="properties.html"><span class="fa fa-angle-right"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <p  class="asks-call">QUESTIONS? CALL US  : <span class="strong"> + 3-123- 424-5700</span></p>
            </div>
        </div>
    </div>
</div> -->
@endsection  
@push('css')
<link rel="stylesheet" type="text/css" href="{{url('css/customer/home.css')}}">
@endpush
    
