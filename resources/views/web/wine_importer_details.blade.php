@extends('layouts.layout')
@section('content')
	<div class="content-area single-property" style="background-color: #FCFCFC;">&nbsp;
            <div class="container">
                <div class="row">
                    <div class="col-md-3">   
                        <h3 class="sec_clr">Digital Directory</h3>
                        <h5 class="sec_clr">Make new connections and grow</h5>
                    </div>
                    <div class="col-md-6 result">
                        <p><a class="cnt_nme">Go Premium and Unlock More Results</a>
                                    | Want to be part of this Directory</p>
                    </div>
                </div>
                <div class="clearfix padding-top-40">
                    <div class="row">
                        <div class="col-md-3">
                           <div class="cat_nav_bg">
                                <div class="panel panel-default sidebar-menu wow fadeInRight animated">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Popular Searches</h4>
                                    </div>
                                </div>
                                <div class="panel-content">
                                    <p>WINE</p>
                                </div>
                                <div class="panel-content">
                                    <p>SPIRITS</p>
                                </div>
                                <div class="panel-content">
                                    <p>BEER</p>
                                </div>
                                <div class="panel-content">
                                    <p>OTHER</p>
                                </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                            <a href="{{route('front.wine_imports_usa')}}">Wine Importer<i class="fa fa-chevron-right"></i></a>
                            <div class="cat_nav_bg">
                            <div class="single-property-wrapper">
                                <div class="section pt-0">
                                    @foreach($wine_usa as $detail)
                                    <h2 class="wine_type">{{$detail->name}}</h2>
                                    @endforeach
                                    @foreach($company as $detail)
                                    <div>
                                       <h6 class="cat_clr"><label>Category:-</label>
                                       {{$detail->name}}</h6>
                                    </div>
                                    @endforeach
                                    @foreach($countries as $detail)
                                    <div>
                                       <h6 class="cat_clr"><label>Country:-</label>
                                       {{$detail->country}}</h6>
                                    </div>
                                    @endforeach
                                    <div>
                                        <h6 class="cat_clr"><label>Date:-</label>18-11-2019</h6>
                                    </div>
                                    @foreach($wine_usa as $data)
                                    <div>
                                       {{$data->description}}
                                    </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="sec_clr">More Company</h4><hr>
                                <a href="#" ><img src="{{url('img/product/product-home2.jpg')}}"></a>
                            </div>
                        </div>
                        </div>
                        <div class="col-md-3">
                            <div class="cat_nav_bg">
                                <div class="panel panel-default sidebar-menu wow fadeInRight animated">
                                    @foreach($wine_usa as $detail)
                                    <div class="panel-body recent-property-widget">
                                        <img src="{{url($detail->logo)}}" class="img_logo">
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
@endsection
<style type="text/css">
    div .event_padding {
        padding-left: 50px;
    }
</style>