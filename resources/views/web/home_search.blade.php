@extends('layouts.layout')
@section('title', 'World-beverages')
@section('content')        
<div class="page-head"> 
    <div class="container">
        <div class="row">
            <div class="page-head-content">
                <h1 class="page-title">List Layout Product</h1> 
                              
            </div>
        </div>
    </div>
</div>
<div class="properties-area recent-property" style="background-color: #FFF;">
    <div class="container">  
        <div class="row">
            @include('web.inc.search_bar')
            <div class="col-md-9  pr0 padding-top-40 properties-page">
                <div class="col-md-12 clear"> 
                    <div class="col-xs-10 page-subheader sorting pl0">
                        <ul class="sort-by-list">
                            <li class="active">
                                <a href="javascript:void(0);" class="order_by_date" data-orderby="property_date" data-order="ASC">
                                    Brand Date <i class="fa fa-sort-amount-asc"></i>                 
                                </a>
                            </li>
                            <li class="">
                                <a href="javascript:void(0);" class="order_by_price" data-orderby="property_price" data-order="DESC">
                                    Brand Price <i class="fa fa-sort-numeric-desc"></i>                      
                                </a>
                            </li>
                        </ul><!--/ .sort-by-list-->

                        <div class="items-per-page">
                            <label for="items_per_page"><b>Brand per page :</b></label>
                            <div class="sel">
                                <select id="items_per_page" name="per_page">
                                    <option value="3">3</option>
                                    <option value="6">6</option>
                                    <option value="9">9</option>
                                    <option selected="selected" value="12">12</option>
                                    <option value="15">15</option>
                                    <option value="30">30</option>
                                    <option value="45">45</option>
                                    <option value="60">60</option>
                                </select>
                            </div><!--/ .sel-->
                        </div><!--/ .items-per-page-->
                    </div>

                    <div class="col-xs-2 layout-switcher">
                        <a class="layout-list" href="javascript:void(0);"> <i class="fa fa-th-list"></i>  </a>
                        <a class="layout-grid active" href="javascript:void(0);"> <i class="fa fa-th"></i> </a>                          
                    </div><!--/ .layout-switcher-->
                </div>

                <div class="col-md-12 clear"> 
                    <div id="list-type" class="proerty-th">
                        @forelse($products as $product)
                        <div class="col-sm-6 col-md-4">
                            <div class="box-two proerty-item">
                                <div class="item-thumb">
                                    <a href="{{route('front.product_description',$product->id)}}" ><img src="{{url($product->img)}}" width="500" height="400"></a>
                                </div>
                                <div class="item-entry overflow">
                                    <h5><a href="{{route('front.product_description',$product->id)}}">@if(strlen($product->name)<12){{ $product->name }}@else{{ substr($product->name,0,12)."..." }}@endif</a></h5>
                                    <div class="dot-hr"></div>
                                    <span class="pull-left"><b> Product type :</b> {{$product->category_name}} </span>
<!--                                     <span class="proerty-price pull-right"> ${{$product->price_unit_usd}} </span> -->
                                    <p style="display: none;">{{$product->description}}</p>
                                </div>
                            </div>
                        </div>
                        @empty
                        <div class="col-sm-6 col-md-4 p0">
                            <label>0 results</label>
                        </div>
                        @endforelse
                    </div>
                </div>
                
                <div class="col-md-12"> 
                    <div class="pull-right">
                        <div class="pagination">
                            <nav>
                                @php
                                    $tempQuery=Request::getQueryString();
                                    $tempQuery = explode("&",$tempQuery);
                                    $newQueryString = "";
                                    foreach ($tempQuery as $queryString) {
                                        if(explode("=",$queryString)[0]=="page"){
                                        } else{
                                            $newQueryString .= "&".$queryString;
                                        }
                                    }
                                @endphp
                                @if ($products->lastPage() > 1)
                                    <ul class="">
                                        @if ($products->currentPage() != 1 && $products->lastPage() >= 5)
                                            <li class="page-item"><a class="page-link" href="{{ $products->url($products->url(1)).$newQueryString }}" ><i class="fa fa-angle-double-left"></i></a></li>
                                        @endif
                                        @if($products->currentPage() != 1)
                                            <li class="page-item">
                                                <a class="page-link" href="{{ $products->url($products->currentPage()-1).$newQueryString }}" >
                                                    <
                                                </a>
                                            </li>
                                        @endif
                                        @for($i = max($products->currentPage()-2, 1); $i <= min(max($products->currentPage()-2, 1)+4,$products->lastPage()); $i++)
                                                <li class="page-item {{ ($products->currentPage() == $i) ? 'active' : '' }}">
                                                    <a class="page-link" href="{{ $products->url($i).$newQueryString }}">{{ $i }}</a>
                                                </li>
                                        @endfor
                                        @if ($products->currentPage() != $products->lastPage())
                                            <li class="page-item">
                                                <a class="page-link" href="{{ $products->url($products->currentPage()+1).$newQueryString }}" >
                                                    >
                                                </a>
                                            </li>
                                        @endif
                                        @if ($products->currentPage() != $products->lastPage() && $products->lastPage() >= 5)
                                            <li class="page-item">
                                                <a class="page-link" href="{{ $products->url($products->lastPage()).$newQueryString }}" >
                                                    >>
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                @endif
                            </nav>
                        </div>
                    </div>                
                </div>
            </div>  
        </div>              
    </div>
</div>
@endsection
<style type="text/css">
    .home-search-img{
        height: 100px!important;
        width: 250px!important;
    }
    img{
        height: 200!important;
    }
</style>        