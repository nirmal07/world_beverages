@extends('layouts.layout')
@section('title', 'World-beverages')
@section('content')        
<div class="page-head"> 
    <div class="container">
        <div class="row">
            <div class="page-head-content">
                <h1 class="page-title">List Layout Digital Directory</h1>               
            </div>
        </div>
    </div>
</div>
<div class="properties-area recent-property" style="background-color: #FFF;">
    <div class="container">  
        <div class="row">
            @include('web.inc.directory_search')
            <div class="col-md-9  pr0 padding-top-40 properties-page">  
                <div class="col-md-12 clear"> 
                    <div id="list-type" class="proerty-th-list">
                        @forelse($directorys as $directory)
                        <div class="col-sm-6 col-md-4 p0">
                            <div class="box-two proerty-item">
                                <div class="item-thumb">
                                    <a href="#" ><img src="{{url($directory->logo)}}"></a>
                                </div>
                                <div class="item-entry overflow">
                                    <h5><a href="#"> {{$directory->name}} </a></h5>
                                    <div class="dot-hr"></div>
                                    <span class="pull-left"><b> Company type :</b> {{$directory->company_types}} </span><br>
                                    <span class="pull-left">{{$directory->countries_name}}</span>
                                    <p>
                                    {{$directory->description}}</p>
                                </div>
                            </div>
                        </div> 
                        @empty
                        <div class="col-sm-6 col-md-4 p0">
                            <label>0 results</label>
                        </div>
                        @endforelse
                    </div>
                </div>
                
                <div class="col-md-12"> 
                    <div class="pull-right">
                        <div class="pagination">
                            <nav>
                                @php
                                    $tempQuery=Request::getQueryString();
                                    $tempQuery = explode("&",$tempQuery);
                                    $newQueryString = "";
                                    foreach ($tempQuery as $queryString) {
                                        if(explode("=",$queryString)[0]=="page"){
                                        } else{
                                            $newQueryString .= "&".$queryString;
                                        }
                                    }
                                @endphp
                                @if ($directorys->lastPage() > 1)
                                    <ul class="">
                                        @if ($directorys->currentPage() != 1 && $directorys->lastPage() >= 5)
                                            <li class="page-item"><a class="page-link" href="{{ $directorys->url($directorys->url(1)).$newQueryString }}" ><i class="fa fa-angle-double-left"></i></a></li>
                                        @endif
                                        @if($directorys->currentPage() != 1)
                                            <li class="page-item">
                                                <a class="page-link" href="{{ $directorys->url($directorys->currentPage()-1).$newQueryString }}" >
                                                    <
                                                </a>
                                            </li>
                                        @endif
                                        @for($i = max($directorys->currentPage()-2, 1); $i <= min(max($directorys->currentPage()-2, 1)+4,$directorys->lastPage()); $i++)
                                                <li class="page-item {{ ($directorys->currentPage() == $i) ? 'active' : '' }}">
                                                    <a class="page-link" href="{{ $directorys->url($i).$newQueryString }}">{{ $i }}</a>
                                                </li>
                                        @endfor
                                        @if ($directorys->currentPage() != $directorys->lastPage())
                                            <li class="page-item">
                                                <a class="page-link" href="{{ $directorys->url($directorys->currentPage()+1).$newQueryString }}" >
                                                    >
                                                </a>
                                            </li>
                                        @endif
                                        @if ($directorys->currentPage() != $directorys->lastPage() && $directorys->lastPage() >= 5)
                                            <li class="page-item">
                                                <a class="page-link" href="{{ $directorys->url($directorys->lastPage()).$newQueryString }}" >
                                                    >>
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                @endif
                            </nav>
                        </div>
                    </div>                
                </div>
            </div>  
        </div>              
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="{{url('css/customer/digital_directory.css')}}">
@endpush
        