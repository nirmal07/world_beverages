@extends('layouts.layout')
@section('title', 'World-beverages')
@section('content')        
<!-- <div class="page-head"> 
    <div class="container">
        <div class="row">
            <div class="page-head-content">
                <h1 class="page-title">Wine Importers in USA</h1>               
            </div>
        </div>
    </div>
</div> -->
<header class="wine_head">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="title">Wine Importers in USA</h3>
            </div>
        </div>
    </div>
</header>
<div class="properties-area recent-property" style="background-color: #FFF;">
    <div class="container">   
        <div class="row">
            <div class="col-md-8 properties-page prep_sec">
                <div class="section clear sec_clr">
                    <h4>On this page you will find list of wine Importers in USA</h4>
                    <hr>
                    <!-- <div class="col-xs-10 page-subheader sorting pl0">
                        <ul class="sort-by-list">
                            <li class="active">
                                <a href="javascript:void(0);" class="order_by_date" data-orderby="property_date" data-order="ASC">
                                    Property Date <i class="fa fa-sort-amount-asc"></i>                 
                                </a>
                            </li>
                            <li class="">
                                <a href="javascript:void(0);" class="order_by_price" data-orderby="property_price" data-order="DESC">
                                    Property Price <i class="fa fa-sort-numeric-desc"></i>                      
                                </a>
                            </li>
                        </ul>
                        </ .sort-by-list-->
                        <!-- <div class="items-per-page">
                            <label for="items_per_page"><b>Property per page :</b></label>
                            <div class="sel">
                                <select id="items_per_page" name="per_page">
                                    <option value="3">3</option>
                                    <option value="6">6</option>
                                    <option value="9">9</option>
                                    <option selected="selected" value="12">12</option>
                                    <option value="15">15</option>
                                    <option value="30">30</option>
                                    <option value="45">45</option>
                                    <option value="60">60</option>
                                </select> -->
                            <!-- </div> --><!--/ .sel-->
                        <!-- </div> -->
                        <!--/ .items-per-page-->
                    <!-- </div> -->
<!-- 
                    <div class="col-xs-2 layout-switcher">
                        <a class="layout-list" href="javascript:void(0);"> <i class="fa fa-th-list"></i>  </a>
                        <a class="layout-grid active" href="javascript:void(0);"> <i class="fa fa-th"></i> </a>                          
                    </div> -->
                    <!--/ .layout-switcher-->
                </div>

                <div class="section clear"> 
                    <div id="" class="proerty-th-list">
                        @forelse($directorys_usa as $directory)
                        <div class="col-sm-6 col-md-4 p0">
                            <div class="box-two proerty-item">
                                <div class="item-thumb">
                                    <a href="{{route('front.wine_importer_details',$directory->id)}}" ><img src="{{url($directory->logo)}}" class="img_importer_logo"></a>
                                </div>
                                <div class="item-entry overflow">
                                    <h5><a href="#">{{$directory->name}} </a></h5>
                                    <div class="dot-hr"></div>
                                    <span class="pull-left cmp_type"><!-- <b> Company type :</b> --> {{$directory->company_types}} </span>
                                    <p class="cnt_nme">{{$directory->countries_name}}</p>
                                    <span class="cnt_nme">lorem ipsum,</span>{{$directory->description}}
                                </div>
                            </div>
                        </div>  
                        @empty
                        <div class="col-sm-6 col-md-4 p0">
                            <label>0 results</label>
                        </div>
                        @endforelse
                    </div>
                </div>
                <div class="section">
                    <div class="pull-right">
                        <div class="pagination">
                            <nav>
                                @php
                                    $tempQuery=Request::getQueryString();
                                    $tempQuery = explode("&",$tempQuery);
                                    $newQueryString = "";
                                    foreach ($tempQuery as $queryString) {
                                        if(explode("=",$queryString)[0]=="page"){
                                        } else{
                                            $newQueryString .= "&".$queryString;
                                        }
                                    }
                                @endphp
                                @if ($directorys_usa->lastPage() > 1)
                                    <ul class="">
                                        @if ($directorys_usa->currentPage() != 1 && $directorys_usa->lastPage() >= 5)
                                            <li class="page-item"><a class="page-link" href="{{ $directorys_usa->url($directorys_usa->url(1)).$newQueryString }}" ><i class="fa fa-angle-double-left"></i></a></li>
                                        @endif
                                        @if($directorys_usa->currentPage() != 1)
                                            <li class="page-item">
                                                <a class="page-link" href="{{ $directorys_usa->url($directorys_usa->currentPage()-1).$newQueryString }}" >
                                                    <
                                                </a>
                                            </li>
                                        @endif
                                        @for($i = max($directorys_usa->currentPage()-2, 1); $i <= min(max($directorys_usa->currentPage()-2, 1)+4,$directorys_usa->lastPage()); $i++)
                                                <li class="page-item {{ ($directorys_usa->currentPage() == $i) ? 'active' : '' }}">
                                                    <a class="page-link" href="{{ $directorys_usa->url($i).$newQueryString }}">{{ $i }}</a>
                                                </li>
                                        @endfor
                                        @if ($directorys_usa->currentPage() != $directorys_usa->lastPage())
                                            <li class="page-item">
                                                <a class="page-link" href="{{ $directorys_usa->url($directorys_usa->currentPage()+1).$newQueryString }}" >
                                                    >
                                                </a>
                                            </li>
                                        @endif
                                        @if ($directorys_usa->currentPage() != $directorys_usa->lastPage() && $directorys_usa->lastPage() >= 5)
                                            <li class="page-item">
                                                <a class="page-link" href="{{ $directorys_usa->url($directorys_usa->lastPage()).$newQueryString }}" >
                                                    >>
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                @endif
                            </nav>
                        </div>
                    </div>
                </div>
            </div> 
            @include('web.inc.wine_importers_usa_slide')
        </div>           
    </div>
</div>

@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="{{url('css/customer/digital_directory.css')}}">
@endpush
@push('script')        
<script type="text/javascript" src="{{url('js/custom/user/home.js')}}"></script>
@endpush