@extends('layouts.layout')
@section('content')
	<div class="content-area single-property" style="background-color: #FCFCFC;">&nbsp;
            <div class="container">   

                <div class="clearfix padding-top-40">
                    <div class="row">
                        <div class="col-md-3">
                           <div class="cat_nav_bg">
                                <div class="panel panel-default sidebar-menu wow fadeInRight animated">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Category Navigation</h4>
                                    </div>
                                </div>
                                <div class="panel-content">
                                    <p>WINE</p>
                                </div>
                                <div class="panel-content">
                                    <p>SPIRITS</p>
                                </div>
                                <div class="panel-content">
                                    <p>BEER</p>
                                </div>
                                <div class="panel-content">
                                    <p>OTHER</p>
                                </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                            <div class="cat_nav_bg">
                            <div class="single-property-wrapper">
                                <div class="section pt-0">
                                    <div class="panel panel-default sidebar-menu wow fadeInRight animated">
                                        @foreach($events as $detail)
                                        <div class="panel-heading">
                                           <h4 class="panel-title">Events Title</h4>
                                        </div>
                                        <div class="panel-content">
                                            <p>{{$detail->title}}</p>
                                        </div>
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Events Description</h4>
                                        </div>
                                        <div class="panel-content">
                                            <p>{{$detail->description}}</p>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="section additional-details">
                                    <div class="panel panel-default sidebar-menu wow fadeInRight animated">
                                        @foreach($events as $detail)
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Additional Details</h4>
                                        </div>
                                        <ul class="additional-details-list clearfix">
                                            <li>
                                                <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">PRIORITY</span>
                                                <span class="col-xs-6 col-sm-8 col-md-8 add-d-entry">{{$detail->priority}}</span>
                                            </li>

                                            <li>
                                                <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">EVENT OPENING HOUR</span>
                                                <span class="col-xs-6 col-sm-8 col-md-8 add-d-entry">{{$detail->event_opening_hour}}</span>
                                            </li>
                                            <li>
                                                <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">START DATE</span>
                                                <span class="col-xs-6 col-sm-8 col-md-8 add-d-entry">{{$detail->start_date}}</span>
                                            </li>
                                            <li>
                                                <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">END DATE</span>
                                                <span class="col-xs-6 col-sm-8 col-md-8 add-d-entry">{{$detail->end_date}}</span>
                                            </li>
                                        </ul>
                                        @endforeach
                                    </div>
                                </div> 
        
                            </div>

                        </div>
                        <div class="cat_nav_bg">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="single-property-wrapper">
                                        <div class="panel panel-default sidebar-menu wow fadeInRight animated">
                                            @foreach($events as $detail)
                                            <div class="panel-heading">
                                               <h4 class="panel-title">Events Map</h4>
                                            </div>
                                            <div  class="panel-content ">
                                                <iframe class="map_iframe" src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d2965.0824050173574!2d-93.63905729999999!3d41.998507000000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sWebFilings%2C+University+Boulevard%2C+Ames%2C+IA!5e0!3m2!1sen!2sus!4v1390839289319" frameborder="0" style="border:0;"></iframe>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cat_nav_bg">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="panel panel-default sidebar-menu wow fadeInRight animated">
                                            @foreach($events as $detail)
                                            <div class="panel-heading">
                                                <h4 class="panel-title">Organizer</h4>
                                            </div>
                                            <div class="panel-content">
                                                <p>Further Info Url :-{{$detail->further_info_url}}</p>
                                            </div>
                                            <div class="panel-content">
                                                <p>Registration Url:-{{$detail->registration_url}}</p>
                                            </div>
                                            <div class="panel-content">
                                                <p>Further Info Email:-{{$detail->further_info_email}}</p>
                                            </div>
                                            @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="col-md-3">
                            <div class="cat_nav_bg">
                                <div class="panel panel-default sidebar-menu wow fadeInRight animated">
                                    @foreach($events as $detail)
                                    <div class="panel-heading">
                                        <h3 class="panel-title">View Advertise </h3>
                                    </div>
                                    <div class="panel-body recent-property-widget">
                                        <img src="{{url($detail->image)}}">
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="cat_nav_bg loc_head">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="panel panel-default sidebar-menu wow fadeInRight animated">
                                            @foreach($events as $detail)
                                            <div class="panel-heading">
                                                <h4 class="panel-title">Dates</h4>
                                            </div>
                                            <div class="panel-content">
                                                <p>{{$detail->start_date}}</p>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cat_nav_bg sec_tag">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="panel panel-default sidebar-menu wow fadeInRight animated">
                                            @foreach($events as $detail)
                                            <div class="panel-heading">
                                                <h4 class="panel-title">Section Tags</h4>
                                            </div>
                                            <div class="panel-content">
                                                <p>{{$detail->section_tags}}</p>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
@endsection
<style type="text/css">
    div .event_padding {
        padding-left: 50px;
    }
</style>